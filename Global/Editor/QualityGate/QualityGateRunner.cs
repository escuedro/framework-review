#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Framework.QualityGates
{
	public class QualityGateRunner
	{
		private static List<IQualityGate> _qualityGates;
		private readonly HashSet<string> _allStrings = new HashSet<string>();

		private static readonly string[] AssembliesToCheck = new[] {"GameAssembly", "Assembly-CSharp-Editor", "GameEditorAssembly"};
		private static readonly Type QualityGateType = typeof(IQualityGate);

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
		static void Init()
		{
			QualityGateRoot.TestQuality = countMessages => new QualityGateRunner().Test(countMessages, true);
		}

		[UnityEditor.MenuItem("Quality/Test")]
		public static void TestStatic()
		{
			new ClientLogger();
			new QualityGateRunner().Test(150, false);
		}

		public static void RunFromDi()
		{
			new ClientLogger();
			_qualityGates = GetQualityGates(false);
			bool success = new QualityGateRunner().RunPreparedGates(100);
			if (!success)
			{
				throw new Exception("Check failed");
			}
		}

		public void Test(int countMessages, bool onPlayQualityGatesOnly)
		{
			_qualityGates = GetQualityGates(onPlayQualityGatesOnly);
			RunPreparedGates(countMessages);
		}

		private static List<IQualityGate> GetQualityGates(bool onPlayQualityGatesOnly)
		{
			List<IQualityGate> qualityGates = new List<IQualityGate>();
			Assembly[] assemblies = new Assembly[AssembliesToCheck.Length];
			for (int i = 0; i < AssembliesToCheck.Length; i++)
			{
				assemblies[i] = AppDomain.CurrentDomain.Load(AssembliesToCheck[i]);
			}
			foreach (Assembly assembly in assemblies)
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (!type.ContainsGenericParameters && QualityGateType.IsAssignableFrom(type) &&
							!type.IsAbstract)
					{
						if (type.IsDefined(typeof(AutoTestExceptedAttribute)))
						{
							continue;
						}
						IQualityGate gate = (QualityGate)Activator.CreateInstance(type);
						if (onPlayQualityGatesOnly && gate.StartOnPlay)
						{
							qualityGates.Add(gate);
						}
						else if (!onPlayQualityGatesOnly)
						{
							qualityGates.Add(gate);
						}
					}
				}
			}
			return qualityGates;
		}

		private bool RunPreparedGates(int countMessages)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			List<Error> exceptions = RunTest();
			if (exceptions.Count > 0)
			{
				stopwatch.Stop();
				Log.Error(
						$"Game is NOT ready to deploy. Exception count = {exceptions.Count.ToString()}. Time = {stopwatch.ElapsedMilliseconds.ToString()}ms");
				int next = 0;
				foreach (Error error in exceptions)
				{
					if (next++ < countMessages)
					{
						Log.Exception(error.Exception, error.Context);
					}
				}
				return false;
			}
			else
			{
				stopwatch.Stop();
				Log.Info(
						$"<color=green>Game quality checks is passed</color> {stopwatch.ElapsedMilliseconds.ToString()}ms");
			}
			return true;
		}

		private List<Error> RunTest()
		{
			LogQualityGatesLaunched();
			List<Error> errors = new List<Error>();
			try
			{
				//TODO LoadStatic
			}
			catch (Exception exception)
			{
				errors.Add(new Error(exception));
				return errors;
			}
			foreach (IQualityGate qualityGate in _qualityGates)
			{
				try
				{
					errors.AddRange(qualityGate.RunTest());
				}
				catch (Exception exception)
				{
					errors.Add(new Error(exception));
				}
			}

			return errors;
		}

		private void LogQualityGatesLaunched()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("Quality gates were launched: ");
			for (int i = 0; i < _qualityGates.Count; i++)
			{
				stringBuilder.Append($"{_qualityGates[i].GetType().Name}");
				if (i != _qualityGates.Count - 1)
				{
					stringBuilder.Append("\n");
				}
			}
			Log.Info(stringBuilder.ToString());
		}
	}
}
#endif