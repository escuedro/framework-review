﻿#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Object = System.Object;

namespace Framework.QualityGates
{
	public class ComponentScanner
	{
		private static readonly string[] TestAssemblies =
		{
				"GameAssembly",
				"Assembly-CSharp"
		};

		private readonly Type[] stopInspectTypes =
		{
				typeof(MonoBehaviour),
				typeof(Component)
		};
		private static readonly Type SerializableType = typeof(SerializeField);
		private static readonly Type ObjectType = typeof(Object);

		public IEnumerable<FieldInfo> GetFilteredFields(Component component, params Type[] removeFieldsWithAttributes)
		{
			IEnumerable<FieldInfo> fields = GetComponentSerializedFields(component);
			foreach (var field in fields)
			{
				bool correct = true;
				foreach (var attribute in removeFieldsWithAttributes)
				{
					if (field.IsDefined(attribute))
					{
						correct = false;
						break;
					}
				}
				if(correct)
				{
					yield return field;
				}
			}
		}

		public IEnumerable<FieldInfo> GetComponentSerializedFields(Component component)
		{
			Type componentType = component.GetType();

			List<FieldInfo> result = new List<FieldInfo>();
			if (IsProjectType(componentType))
			{
				InspectFields(componentType, result);
			}
			if (result == null)
			{
				throw new Exception("Component does not have fields!");
			}
			return result;
		}

		private void InspectFields(Type type, List<FieldInfo> fieldInfoResult)
		{
			foreach (Type stopInspectType in stopInspectTypes)
			{
				if (stopInspectType == type)
				{
					return;
				}
			}
			if (type.BaseType != null)
			{
				InspectFields(type.BaseType, fieldInfoResult);
			}

			foreach (FieldInfo serializableField in GetSerializableFields(type))
			{
				fieldInfoResult.Add(serializableField);
			}
		}

		public static IEnumerable<FieldInfo> GetSerializableFields(Type type)
		{
			FieldInfo[] fieldInfos =
					type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			foreach (FieldInfo fieldInfo in fieldInfos)
			{
				if (fieldInfo.IsPublic || fieldInfo.IsDefined(SerializableType))
				{
					if (fieldInfo.FieldType.IsClass)
					{
						if (ObjectType.IsAssignableFrom(fieldInfo.FieldType))
						{
							yield return fieldInfo;
						}
					}
				}
			}
		}

		public static bool IsProjectType(Type componentType)
		{
			foreach (string allowedAssembly in TestAssemblies)
			{
				if (componentType.Assembly.FullName.StartsWith(allowedAssembly))
				{
					return true;
				}
			}
			return false;
		}
	}
}
#endif