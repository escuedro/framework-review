#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.QualityGates
{
	public class NotNullRefsInSceneQualityGate : SceneComponentQualityGate<Component>
	{
		public override bool StartOnPlay => false;

		private static readonly Type CanBeNullType = typeof(CanBeNullAttribute);
		private static readonly Type NotUsedInBuildType = typeof(NotUsedInBuildAttribute);
		private static readonly Type InjectType = typeof(InjectAttribute);

		protected override void CheckAsset(Component component)
		{
			ComponentScanner scanner = new ComponentScanner();
			var fields = scanner.GetFilteredFields(component, CanBeNullType, NotUsedInBuildType, InjectType);


			foreach (var serializableField in fields)
			{
				object value = serializableField.GetValue(component);
				if (value == null)
				{
					Collect(new Exception(
							$"{component.name}: {serializableField.FieldType.Name} is null at {SceneManager.GetActiveScene().name}/{GetObjectScenePath(component)}"));
				}
			}
		}

		private static string GetObjectScenePath(Component component)
		{
			Stack<string> hierarchy = new Stack<string>();
			Transform parent = component.transform;
			while (parent != null)
			{
				hierarchy.Push(parent.name);
				parent = parent.parent;
			}
			StringBuilder sb = new StringBuilder();
			while (hierarchy.Count > 0)
			{
				sb.Append(hierarchy.Pop());
				sb.Append("/");
			}
			return sb.ToString();
		}
	}
}
#endif