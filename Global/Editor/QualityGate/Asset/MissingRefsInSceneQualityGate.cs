#if !NOT_UNITY3D
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.QualityGates
{
	public class MissingRefsInSceneQualityGate : QualityGate
	{
		public override bool StartOnPlay => false;
		protected override void Test()
		{
			//all assets check
			//var allAssets = AssetDatabase.GetAllAssetPaths();
			//var objects = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

			GameObject[] objects = GetSceneObjects();
			FindMissingReferences(SceneManager.GetActiveScene().path, objects);
		}

		private void FindMissingReferences(string context, GameObject[] objects)
		{
			foreach (GameObject go in objects)
			{
				Component[] components = go.GetComponents<Component>();

				foreach (var component in components)
				{
					if (!component)
					{
						Debug.LogError("Missing Component in GO: " + FullPath(go), go);
						continue;
					}

					SerializedObject so = new SerializedObject(component);
					SerializedProperty sp = so.GetIterator();

					while (sp.NextVisible(true))
					{
						if (sp.propertyType == SerializedPropertyType.ObjectReference)
						{
							if (sp.objectReferenceValue == null
									&& sp.objectReferenceInstanceIDValue != 0)
							{
								ShowError(context, go, component.GetType().Name,
										ObjectNames.NicifyVariableName(sp.name));
							}
						}
					}
				}
			}
		}

		private void ShowError(string context, GameObject go, string c, string property)
		{
			Collect(new Exception($"Missing Ref in: [{context}]{FullPath(go)}. Component: {c}, Property: {property}"));
		}

		private static GameObject[] GetSceneObjects()
		{
			return Resources.FindObjectsOfTypeAll<GameObject>().Where(go =>
					string.IsNullOrEmpty(AssetDatabase.GetAssetPath(go))
					&& go.hideFlags == HideFlags.None).ToArray();
		}

		private static string FullPath(GameObject go)
		{
			var parent = go.transform.parent;
			return parent == null
					? go.name
					: FullPath(parent.gameObject) + "/" + go.name;
		}
	}
}
#endif