﻿#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework.QualityGates
{
	public class NotNullRefsInResourcesQualityGate : AssetQualityGate<GameObject>
	{
		public override bool StartOnPlay => false;
		protected override string GetAssetFilter() => "t:prefab";

		private static readonly Type CanBeNullType = typeof(CanBeNullAttribute);
		private static readonly Type NotUsedInBuildType = typeof(NotUsedInBuildAttribute);
		private static readonly Type InjectType = typeof(InjectAttribute);

		protected override void CheckAsset(GameObject asset, string assetPath)
		{
			Component[] components = asset.GetComponents<Component>();

			ComponentScanner scanner = new ComponentScanner();
			foreach (var component in components)
			{
				var fields = scanner.GetFilteredFields(component, CanBeNullType, NotUsedInBuildType, InjectType);

				foreach (var serializableField in fields)
				{
					object value = serializableField.GetValue(component);
					if (value == null)
					{
						Collect(new Exception(
								$"{component.name}: {serializableField.FieldType.Name} : {serializableField.Name} is null at {assetPath}"));
					}
				}
			}
		}
	}
}
#endif