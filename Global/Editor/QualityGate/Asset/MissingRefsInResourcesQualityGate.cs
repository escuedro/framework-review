#if !NOT_UNITY3D
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.QualityGates
{
	public class MissingRefsInResourcesQualityGate : AssetQualityGate<GameObject>
	{
		public override bool StartOnPlay => false;
		protected override string GetAssetFilter() => "t:prefab";

		protected override void CheckAsset(GameObject asset, string assetPath)
		{
			Component[] components = asset.GetComponents<Component>();

			foreach (var component in components)
			{
				if (!component)
				{
					Collect(new Exception(
							$"{asset.name}: component is missing at {assetPath}"));
					continue;
				}

				SerializedObject so = new SerializedObject(component);
				SerializedProperty sp = so.GetIterator();

				while (sp.NextVisible(true))
				{
					if (sp.propertyType == SerializedPropertyType.ObjectReference)
					{
						if (sp.objectReferenceValue == null
								&& sp.objectReferenceInstanceIDValue != 0)
						{
							Collect(new Exception(
									$"{component.name}: {sp.type} is missing at {assetPath}"));
						}
					}
				}
			}
		}
	}
}
#endif