﻿#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Framework.QualityGates
{
	public class TMPCheckQualityGate : AssetQualityGate<GameObject>
	{
		public override bool StartOnPlay => false;

		protected override string GetAssetFilter() => "t:prefab";

		protected override void CheckAsset(GameObject asset, string assetPath)
		{
			List<TextMeshProUGUI> textMeshPros = new List<TextMeshProUGUI>();
			textMeshPros.AddRange(FindTmpInGameObjectAndChildren(asset));
			foreach (var tmp in textMeshPros)
			{
				ShowError(assetPath, tmp.transform);
			}
		}

		private static IEnumerable<TextMeshProUGUI> FindTmpInGameObjectAndChildren(GameObject go)
		{
			List<TextMeshProUGUI> tmps = new List<TextMeshProUGUI>();
			TextMeshProUGUI tmp = go.GetComponent<TextMeshProUGUI>();
			if (tmp != null && tmp.GetType() != typeof(UIText))
			{
				tmps.Add(tmp);
			}

			for (int i = 0; i < go.transform.childCount; i++)
			{
				tmps.AddRange(FindTmpInGameObjectAndChildren(go.transform.GetChild(i).gameObject));
			}
			return tmps;
		}

		private void ShowError(string assetPath, Transform tmpContainer)
		{
			Collect(new Exception(
					$"Found TextMeshProUGUI in prefab! Prefab [{assetPath}]. Child name [{tmpContainer.name}]. You should use UIText instead."));
		}
	}
}
#endif