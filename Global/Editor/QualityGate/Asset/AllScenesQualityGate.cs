#if !NOT_UNITY3D
using System;
using UnityEngine.SceneManagement;

namespace Framework.QualityGates
{
	public class AllScenesQualityGate : QualityGate
	{
		public override bool StartOnPlay => false;

		protected override void Test()
		{
#if UNITY_EDITOR

			int startSceneIndex = SceneManager.GetActiveScene().buildIndex;
			string startScenePath = SceneUtility.GetScenePathByBuildIndex(startSceneIndex);
			try
			{
				for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
				{
					string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
					UnityEditor.SceneManagement.EditorSceneManager.OpenScene(scenePath,
							UnityEditor.SceneManagement.OpenSceneMode.Single);
					NotNullRefsInSceneQualityGate notNullRefsInSceneQualityGate = new NotNullRefsInSceneQualityGate();
					Collect(notNullRefsInSceneQualityGate.RunTest());
					MissingRefsInSceneQualityGate missingRefsInSceneQualityGate = new MissingRefsInSceneQualityGate();
					Collect(missingRefsInSceneQualityGate.RunTest());
				}
			}
			catch (Exception exception)
			{
				Collect(exception);
			}
			finally
			{
				if (!string.IsNullOrEmpty(startScenePath))
				{
					UnityEditor.SceneManagement.EditorSceneManager.OpenScene(startScenePath,
							UnityEditor.SceneManagement.OpenSceneMode.Single);
				}
			}
#endif
		}
	}
}
#endif