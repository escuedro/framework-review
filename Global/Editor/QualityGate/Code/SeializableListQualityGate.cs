#if !NOT_UNITY3D
using System;
using System.Reflection;
using UnityEngine;

namespace Framework.QualityGates
{
	public class SerializableListQualityGate : QualityGate
	{
		private static readonly Type ComponentType = typeof(Component);
		private static string ListName = "List`1";

		public override bool StartOnPlay => true;

		protected override void Test()
		{
			foreach (var assemblyName in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
			{
				Assembly assembly = Assembly.Load(assemblyName);
				foreach (var type in assembly.GetTypes())
				{
					if (ComponentScanner.IsProjectType(type) && ComponentType.IsAssignableFrom(type))
					{
						foreach (FieldInfo fieldInfo in ComponentScanner.GetSerializableFields(type))
						{
							if (fieldInfo.FieldType.Name == ListName)
							{
								Collect(new Exception(
										$"{type.FullName} has serializable list {fieldInfo.Name}, please replace for array"));
							}
						}
					}
				}
			}
		}
	}
}
#endif