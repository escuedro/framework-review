﻿#if !NOT_UNITY3D

using GenerateDI;

namespace Framework.CompileListener
{
	public class DiCodegenCompileTimeErrorsListener : CompileTimeErrorsListener
	{
		protected override string FileName => "GeneratedInjectors.cs";
		protected override string EditorPrefsKey => "DiCodegenErrors";

		protected override void OnErrorReceived()
		{
			CodeGeneratorClient.GenerateDi();
		}
	}
}
#endif