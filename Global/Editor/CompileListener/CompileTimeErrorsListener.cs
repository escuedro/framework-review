﻿#if !NOT_UNITY3D

using UnityEditor;
using UnityEngine;

namespace Framework.CompileListener
{
	public abstract class CompileTimeErrorsListener
	{
		protected abstract string FileName { get; }
		protected abstract string EditorPrefsKey { get; }

		public void LogMessageReceived(string condition, string stackTrace, LogType type)
		{
			if (condition.Contains(FileName) && type == LogType.Error)
			{
				if (!EditorPrefs.HasKey(EditorPrefsKey))
				{
					EditorPrefs.SetString(EditorPrefsKey, EditorPrefs.GetString(EditorPrefsKey) + condition);
				}
				else
				{
					string value = EditorPrefs.GetString(EditorPrefsKey);

					if (!value.Contains(condition))
					{
						EditorPrefs.SetString(EditorPrefsKey, value + condition);
						return;
					}
				}
				Log.Info($"<color=red> Error </color> was occured {condition} with stacktrace {stackTrace}");
				OnErrorReceived();
				EditorPrefs.SetString(EditorPrefsKey, "");
			}
		}

		protected abstract void OnErrorReceived();
	}
}

#endif