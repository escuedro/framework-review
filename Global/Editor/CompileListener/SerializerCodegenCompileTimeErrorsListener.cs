﻿#if !NOT_UNITY3D

using GenerateDI;

namespace Framework.CompileListener
{
	public class SerializerCodegenCompileTimeErrorsListener : CompileTimeErrorsListener
	{
		protected override string FileName => "GeneratedSerializer.cs";
		protected override string EditorPrefsKey => "SerializerCodegenErrors";

		protected override void OnErrorReceived()
		{
			CodeGeneratorClient.GenerateProtocol();
		}
	}
}

#endif