﻿#if !NOT_UNITY3D
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Framework.CompileListener
{
	[InitializeOnLoad]
	public class CompileListener
	{
		private static List<CompileTimeErrorsListener> _listeners = new List<CompileTimeErrorsListener>();

		static CompileListener()
		{
			Application.logMessageReceived += LogMessageReceived;
			_listeners.Add(new DiCodegenCompileTimeErrorsListener());
			_listeners.Add(new SerializerCodegenCompileTimeErrorsListener());
		}

		private static void LogMessageReceived(string condition, string stacktrace, LogType type)
		{
			foreach (CompileTimeErrorsListener listener in _listeners)
			{
				listener.LogMessageReceived(condition, stacktrace, type);
			}
		}
	}
}
#endif