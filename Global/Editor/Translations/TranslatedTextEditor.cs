﻿#if !NOT_UNITY3D

using System;
using Framework.Client.Translations;
using UnityEditor;
using UnityEngine;

namespace Framework.Global.Editor.Translations
{
    [CustomEditor(typeof(TranslatedText))]
    public class TranslatedTextEditor : UnityEditor.Editor
    {
        private TranslationKey _currentTranslationKey = new TranslationKey("");
        private TranslationKey[] _variants = Array.Empty<TranslationKey>();

        private bool _changed;

        private SerializedProperty _textMeshProProperty;
        private SerializedProperty _translationKeyProperty;

        private GUIStyle _labelStyle;
        private GUIStyle _errorLabelStyle;

        private TranslatedText _target;

        private void OnEnable()
        {
            _translationKeyProperty = serializedObject.FindProperty("_translationKey");
            _variants = TranslationKeysPicker.GetVariants(_currentTranslationKey, 5);
            _labelStyle = new GUIStyle()
            {
                normal = new GUIStyleState() {textColor = Color.white},
                alignment = TextAnchor.MiddleCenter,
                fontSize = 15,
                fontStyle = FontStyle.Bold
            };
            _errorLabelStyle = new GUIStyle()
            {
                    normal = new GUIStyleState() {textColor = Color.red},
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 12,
                    fontStyle = FontStyle.Bold
            };
            _target = (TranslatedText) target;
        }

        public override void OnInspectorGUI()
        {
            if (!Translation.WasInitialized)
            {
                EditorGUILayout.LabelField("Please click Translations/Generate Translation", _errorLabelStyle);
                return;
            }
            serializedObject.Update();
            
            EditorGUILayout.PropertyField(_translationKeyProperty);
            
            string translationKeyValue = _translationKeyProperty.stringValue;
            _changed = _currentTranslationKey.Value != translationKeyValue;
            _currentTranslationKey = new TranslationKey(translationKeyValue);
            
            if (_changed)
            {
                _variants = TranslationKeysPicker.GetVariants(_currentTranslationKey, 5);
                _target.SetTranslationKey(_currentTranslationKey);
            }
            _target = (TranslatedText) target;
            bool isEmptyVariant = string.IsNullOrEmpty(_currentTranslationKey.Value);
            if (_target.NeedToFindVariant && !isEmptyVariant)
            {
                DrawVariants(OnButtonClick);
            }
            else if (_target.NeedToFindVariant && isEmptyVariant)
            {
                DrawNeedVariantMessage();
            }
            else
            {
                DrawTranslation();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawNeedVariantMessage()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Need translation key", _labelStyle);
        }

        private void DrawTranslation()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(_target.KeyTranslation, _labelStyle);
        }

        private void OnButtonClick(TranslationKey variant)
        {
            _translationKeyProperty.stringValue = variant.Value;
            _target.SetTranslationKey(variant);
            GUI.FocusControl(null);
            Repaint();
        }

        private void DrawVariants(Action<TranslationKey> onButtonClick)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Variants: ", _labelStyle);
            EditorGUILayout.Space();
            
            foreach (TranslationKey variant in _variants)
            {
                if (GUILayout.Button(variant.Value))
                {
                    onButtonClick?.Invoke(variant);
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}

#endif
