﻿#if NOT_UNITY3D
using System;
using Framework;
using Framework.Generated;
using Framework.Injection;
using Framework.Server;
using Framework.Server.Telemetry;

namespace Global
{
	class Program
	{
		static void Main(string[] args)
		{
			Container container = CreateContainer(args);
			try
			{
				Startup startup = new Startup();
				startup.RunServer(container);
			}
			catch (Exception exception)
			{
				Log.Fatal(exception);
				container.Resolve<ServerLogger>().WaitForSendLogs();
				Log.Info("Fatal shutdown");
			}
			container.Resolve<DisposeShutdown>().Shutdown();
		}

		private static Container CreateContainer(string[] args)
		{
			Container container = new Container();
			ProgramParameters programParameters = new ProgramParameters(args);
			GeneratedBinder generatedBinder = new GeneratedBinder();
			generatedBinder.Bind(container);
			container.BindInstance((BaseBinder)generatedBinder);
			container.BindInstance((IBinder)container);
			container.BindInstance(programParameters);

			IConfigBinder configBinder = container.Resolve<IConfigBinder>();
			configBinder.BindConfig("--server_config", ServerConfig.Default);
			container.Resolve<ServerLogger>();
			container.Resolve<TelemetryCollector>();
			return container;
		}
	}
}
#endif