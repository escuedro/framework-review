using Framework.Serialization;

namespace Framework.Generated
{
	[Bind(BindTo = typeof(BaseSerializer))]
	public partial class GeneratedSerializer : BaseSerializer
	{
	}
}