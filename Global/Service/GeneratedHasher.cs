#if NOT_UNITY3D
using Framework.Hasher;

namespace Framework.Generated
{
	[Bind(BindTo = typeof(BaseHasher))]
	public partial class GeneratedHasher: BaseHasher
	{
	}
}
#endif