﻿#if UNITY_EDITOR

using Framework.Client.Translations;
using Framework.Model.Localization;
using Game.Translations;
using GenerateDI;
using UnityEditor;

namespace Framework.Generated
{
	public class TranslationGeneratorGlobal
	{
		[MenuItem("Translations/Generate Translations")]
		public static void Generate()
		{
			ClientLogger clientLogger = new ClientLogger();
			GeneratedSerializer generatedSerializer = new GeneratedSerializer();
			TranslationGenerator translationGenerator = new TranslationGenerator(generatedSerializer);
			translationGenerator.Generate();
			TranslationDeserializer translationDeserializer = new TranslationDeserializer(generatedSerializer);
			Translation.SetTranslationFile(Language.English, translationDeserializer.Deserialize(Language.English));
			Translation.SetCurrentLanguage(Language.English);
		}

		[MenuItem("Codegen/Translation Keys")]
		public static void GenerateTranslationKeys()
		{
			Generate();
			CodeGeneratorClient.GenerateTranslationKeys();
		}
	}
}

#endif