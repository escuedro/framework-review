﻿using System;

namespace Framework.Collections
{
	public static class ArrayExtension
	{
		[Pure]
		public static T GetRandom<T>(this T[] array, Random random)
		{
			return array[random.Next(0, array.Length)];
		}

		[Pure]
		public static bool ContainsValue(this long[] array, long value)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == value)
				{
					return true;
				}
			}
			return false;
		}
	}
}