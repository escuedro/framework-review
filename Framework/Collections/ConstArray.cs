namespace Framework.Collections
{
#if NOT_UNITY3D
	[System.Text.Json.Serialization.JsonConverter(typeof(ConstArrayConverter<>))]
#endif
	public readonly struct ConstArray<T>
	{
		internal readonly T[] _items;
#if NOT_UNITY3D
		[System.Text.Json.Serialization.JsonIgnore]
#endif
		public int Count
		{
			get
			{
				if (_items == null)
				{
					return 0;
				}
				return _items.Length;
			}
		}

		public T this[int index] => _items[index];

		public ConstArray(T[] items)
		{
			_items = items;
		}

		public Enumerator GetEnumerator()
		{
			return new Enumerator(_items);
		}

		public struct Enumerator
		{
			private readonly T[] _items;

			private int _index;

			private T _current;

			public T Current => _current;

			internal Enumerator(T[] items)
			{
				_items = items;
				_index = 0;
				_current = default;
			}

			public void Dispose()
			{
			}

			public bool MoveNext()
			{
				if (_items == null)
				{
					_index = 1;
					_current = default;
					return false;
				}
				if (_index >= _items.Length)
				{
					_index = _items.Length + 1;
					_current = default;
					return false;
				}
				_current = _items[_index];
				_index++;
				return true;
			}
		}
	}
}