using System;

namespace Framework.Collections
{
	public static class ConstArrayExtension
	{
		[Pure]
		public static T[] ToArray<T>(this ConstArray<T> array)
		{
			if (array._items == null)
			{
				return Array.Empty<T>();
			}
			return array._items;
		}
	}
}