#if NOT_UNITY3D
using System;
using System.Diagnostics;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Framework.Collections
{
	public class ConstArrayConverterFactory : JsonConverterFactory
	{
		public override bool CanConvert(Type typeToConvert)
			=> typeToConvert.IsGenericType
					&& typeToConvert.GetGenericTypeDefinition() == typeof(ConstArray<>);

		public override JsonConverter CreateConverter(
				Type typeToConvert, JsonSerializerOptions options)
		{
			Debug.Assert(typeToConvert.IsGenericType &&
					typeToConvert.GetGenericTypeDefinition() == typeof(ConstArray<>));

			Type elementType = typeToConvert.GetGenericArguments()[0];

			JsonConverter converter = (JsonConverter)Activator.CreateInstance(
					typeof(ConstArrayConverter<>)
							.MakeGenericType(new[] { elementType }),
					BindingFlags.Instance | BindingFlags.Public,
					binder: null,
					args: null,
					culture: null)!;

			return converter;
		}
	}

	public class ConstArrayConverter<T> : JsonConverter<ConstArray<T>>
	{
		public override ConstArray<T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			return new ConstArray<T>(JsonSerializer.Deserialize<T[]>(ref reader, options));
		}

		public override void Write(Utf8JsonWriter writer, ConstArray<T> value, JsonSerializerOptions options)
		{
			JsonSerializer.Serialize(writer, value.ToArray(), options);
		}
	}
}
#endif