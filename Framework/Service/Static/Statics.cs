using System;
using System.Collections.Generic;

namespace Framework.Model
{
	public class Statics
	{
		private static Statics _instance;

		private readonly Dictionary<Type, Dictionary<int, StaticObject>> _staticDictionary =
				new Dictionary<Type, Dictionary<int, StaticObject>>();
		private readonly Dictionary<Type, object> _staticArraysDictionary = new Dictionary<Type, object>();
		private readonly Dictionary<Type, object> _staticSingles = new Dictionary<Type, object>();

		public static Statics Init()
		{
			_instance = new Statics();
			return _instance;
		}

		[Pure]
		public static T Get<T>(int id) where T : StaticObject
		{
			return (T)Get(typeof(T), id);
		}

		[Pure]
		public static T GetSingle<T>() where T : class
		{
			return (T)_instance._staticSingles[typeof(T)];
		}

		[Pure]
		public static T[] GetAll<T>() where T : StaticObject
		{
			return (T[])_instance._staticArraysDictionary[typeof(T)];
		}

		public void RegisterStatic<T>(T[] objectArray) where T : StaticObject
		{
			Type staticType = typeof(T);
			// ReSharper disable once CoVariantArrayConversion
			RegisterStaticInternal(objectArray, staticType);
		}

		public void RegisterSingle<T>(T staticObject) where T : class
		{
			_staticSingles.Add(typeof(T), staticObject);
		}

		private void RegisterStaticInternal(StaticObject[] objectArray, Type staticType)
		{
			if (objectArray == null || objectArray.Length == 0)
			{
				Log.Warn($"Empty static {staticType.Name}");
				return;
			}
			Dictionary<int, StaticObject> staticObjects = new Dictionary<int, StaticObject>(objectArray.Length);
			foreach (StaticObject staticObject in objectArray)
			{
#if UNITY_EDITOR
				if (staticObject.Id == 0)
				{
					throw new Exception($"load static fail {staticType.Name} id is 0");
				}

#endif
				staticObjects.Add(staticObject.Id, staticObject);
			}
			_staticDictionary.Add(staticType, staticObjects);
			_staticArraysDictionary.Add(staticType, objectArray);
		}

		private static StaticObject Get(Type type, int id)
		{
			if (_instance._staticDictionary.TryGetValue(type, out Dictionary<int, StaticObject> dictionary))
			{
				if (dictionary.TryGetValue(id, out StaticObject staticObject))
				{
					return staticObject;
				}
				throw new Exception($"{type.Name} {id.ToString()} not registered in static");
			}
			throw new Exception($"{type.Name} not registered in static");
		}
	}
}