using System;
using System.Collections.Generic;

namespace Framework.Pools
{
	//not thread safe
	public static class BufferPool
	{
		private class ArrayComparrerBySize : IComparer<byte[]>
		{
			public int Compare(byte[] x, byte[] y)
			{
				if (ReferenceEquals(x, y))
				{
					return 0;
				}
				if (ReferenceEquals(null, y))
				{
					return 1;
				}
				if (ReferenceEquals(null, x))
				{
					return -1;
				}
				return x.Length.CompareTo(y.Length);
			}
		}

		private static readonly List<byte[]> Arrays = new List<byte[]>();
		private static readonly ArrayComparrerBySize ComparrerBySize = new ArrayComparrerBySize();

		public static void Clear()
		{
			Arrays.Clear();
		}

		[MustUseReturnValue]
		public static byte[] Pull(int size)
		{
			int index = GetIndex(Arrays, size);
			if (index >= 0)
			{
				byte[] array = Arrays[index];
				Arrays.RemoveAt(index);
				return array;
			}
			return new byte[size];
		}

		public static void Release(byte[] array)
		{
			if (Arrays.Count == 0)
			{
				Arrays.Add(array);
				return;
			}
			if (ComparrerBySize.Compare(Arrays[Arrays.Count - 1], array) <= 0)
			{
				Arrays.Add(array);
				return;
			}
			if (ComparrerBySize.Compare(Arrays[0], array) >= 0)
			{
				Arrays.Insert(0, array);
				return;
			}
			int index = Arrays.BinarySearch(array, ComparrerBySize);
			if (index < 0)
				index = ~index;
			Arrays.Insert(index, array);
		}

		private static int GetIndex(List<byte[]> array, int targetLength)
		{
			int pos = 0;
			int count = array.Count;
			int limit = Math.Min(count, 1);
			while (limit < count && array[limit].Length < targetLength)
			{
				pos = limit + 1;
				limit = Math.Min(count, limit * 2 + 1);
			}
			while (pos < limit)
			{
				int testpos = pos + ((limit - pos) >> 1);

				if (array[testpos].Length < targetLength)
					pos = testpos + 1;
				else
					limit = testpos;
			}
			return pos < count && array[pos].Length == targetLength ? pos : -1;
		}
	}
}