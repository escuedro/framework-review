﻿#if NOT_UNITY3D

using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Framework.FixedMath
{
	public class Fix64JsonConverter : JsonConverter<Fix64>
	{
		public override Fix64 Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			return Fix64.CreateByRawValue(JsonSerializer.Deserialize<long>(ref reader, options));
		}

		public override void Write(Utf8JsonWriter writer, Fix64 value, JsonSerializerOptions options)
		{
			JsonSerializer.Serialize(writer, value.RawValue, options);
		}
	}
}

#endif