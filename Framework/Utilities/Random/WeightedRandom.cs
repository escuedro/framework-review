﻿using System;
using System.Collections.Generic;
using SAS.Model.Time;

namespace Framework.RandomUtils
{
	public class WeightedRandom
	{
		private Random _random;

		public WeightedRandom()
		{
			_random = new Random((int)TimePoint.Now.Seconds);
		}

		public TWeighted GetRandomWeightedElement<TWeighted>(TWeighted[] elements) where TWeighted : IWeightedElement
		{
			int totalWeight = 0;
			foreach (TWeighted weightedElement in elements)
			{
				totalWeight += weightedElement.Weight;
			}
			int randomNumber = _random.Next(0, totalWeight);
			foreach (TWeighted weightedElement in elements)
			{
				randomNumber -= weightedElement.Weight;
				if (randomNumber < 0)
				{
					return weightedElement;
				}
			}
			return default;
		}
	}
}