﻿using System;

namespace Framework.RandomUtils
{
	public static class RandomExtensions
	{
		[Pure]
		public static bool TrySuccess(this Random random, uint chance)
		{
			return random.Next(0, 100001) <= chance;
		}
	}
}