﻿namespace Framework.RandomUtils
{
	public interface IWeightedElement
	{
		public int Weight { get; }
	}
}