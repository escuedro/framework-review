namespace Framework
{
	public static class HttpErrorCodes
	{
		public const int Exception = 400;
		public const int SessionExpired = 460;
		public const int RefreshTokenNotAllowed = 403;
	}
}