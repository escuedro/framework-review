#if NOT_UNITY3D
using System;
using System.Collections.Generic;

namespace Framework
{
	[Bind]
	public class TypeCollection
	{
		private readonly List<Type> _types = new List<Type>();

		public void Add<T>()
		{
			_types.Add(typeof(T));
		}

		public void CopyContainer(IResolver from, IBinder to)
		{
			foreach (Type type in _types)
			{
				object instance = from.Resolve(type);
				to.BindInstance(type, instance);
			}
		}
	}
}
#endif