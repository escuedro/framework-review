using System;
using Framework.Futures.Internal;

namespace Framework.Futures
{
	public sealed class Future : AbstractFuture
	{
		private IPromise _promise;

		internal Future(IPromise promise)
		{
			Init(promise);
		}

		internal Future()
		{
			Status = FutureStatus.Resolved;
		}

		internal Future(Exception reason) : base(reason)
		{
		}

		internal void Init(IPromise promise)
		{
			if (_promise != null)
			{
				throw new Exception($"{GetType().Name} is initialized. Try reset before reuse");
			}
			base.Init();
			_promise = promise;
			_promise.Fulfill(this);
		}

		protected override void HandleClear()
		{
			base.HandleClear();
			if (_promise != null)
			{
				if (_promise.IsPending)
				{
					_promise.Revoke(this);
				}
				_promise = null;
			}
		}
	}

	public sealed class Future<TResult> : AbstractFuture<TResult>
	{
		private IPromise<TResult> _promise;

		internal Future(IPromise<TResult> promise)
		{
			Init(promise);
		}

		internal Future(TResult result) : base(result)
		{
		}

		internal Future(Exception reason) : base(reason)
		{
		}

		internal void Init(IPromise<TResult> promise)
		{
			if (_promise != null)
			{
				throw new Exception($"{GetType().Name} is initialized. Try reset before reuse");
			}
			base.Init();
			_promise = promise;
			_promise.Fulfill(this);
		}

		protected override void HandleClear()
		{
			base.HandleClear();
			if (_promise != null)
			{
				if (_promise.IsPending)
				{
					_promise.Revoke(this);
				}
				_promise = null;
			}
		}
	}
}