namespace Framework.Futures
{
	public interface IFutureHandler
	{
		void HandleFuture(IFuture future);
	}
}