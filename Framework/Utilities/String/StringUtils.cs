﻿using System.Text;

namespace Framework.Utils
{
	public class StringUtils
	{
		private static readonly StringBuilder _sb = new StringBuilder();

		public static string DurationFormat(int secondsLeft)
		{
			int hours = secondsLeft / 3600;
			int minutes = secondsLeft / 60 - hours * 60;
			int seconds = secondsLeft % 60;
			_sb.Clear();
			if (hours < 10)
			{
				_sb.Append('0');
			}
			_sb.Append(hours);
			_sb.Append(':');
			if (minutes < 10)
			{
				_sb.Append('0');
			}
			_sb.Append(minutes);
			_sb.Append(':');
			if (seconds < 10)
			{
				_sb.Append('0');
			}
			_sb.Append(seconds);
			return _sb.ToString();
		}

		public static string MinutesSecondsFormat(int secondsLeft)
		{
			int minutes = secondsLeft / 60;
			int seconds = secondsLeft % 60;
			_sb.Clear();
			if (minutes < 10)
			{
				_sb.Append('0');
			}
			_sb.Append(minutes);
			_sb.Append(':');
			if (seconds < 10)
			{
				_sb.Append('0');
			}
			_sb.Append(seconds);
			return _sb.ToString();
		}
	}
}