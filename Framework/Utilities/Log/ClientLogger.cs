#if !NOT_UNITY3D
using System;
using System.Diagnostics;

namespace Framework
{
	[Bind]
	public class ClientLogger
	{
		public ClientLogger()
		{
			Log.OnLog = LogLocal;
			Log.OnLogException = ExceptionLocal;
		}

		[DebuggerHidden]
		private void LogLocal(Log.Level level, string message)
		{
			switch (level)
			{
				case Log.Level.Trace:
					UnityEngine.Debug.Log(message);
					break;
				case Log.Level.Debug:
					UnityEngine.Debug.Log(message);
					break;
				case Log.Level.Info:
					UnityEngine.Debug.Log(message);
					break;
				case Log.Level.Warn:
					UnityEngine.Debug.LogWarning(message);
					break;
				case Log.Level.Error:
					UnityEngine.Debug.LogError(message);
					break;
				case Log.Level.Fatal:
					UnityEngine.Debug.LogError(message);
					break;
			}
		}

		[DebuggerHidden]
		private void ExceptionLocal(Log.Level level, Exception exception, object context)
		{
			UnityEngine.Debug.LogException(exception, context as UnityEngine.Object);
		}
	}
}
#endif