using System;
using System.Diagnostics;

namespace Framework
{
	public class Log
	{
		public enum Level
		{
			Trace = 0,
			Debug = 1,
			Info = 2,
			Warn = 3,
			Error = 4,
			Fatal = 5
		}

		internal static Action<Level, string> OnLog = (level, message) => { };
		internal static Action<Level, Exception, object> OnLogException = (level, exception, context) => { };

		[DebuggerHidden]
		public static void Fatal(Exception exception)
		{
			OnLogException(Level.Fatal, exception, null);
		}

		[DebuggerHidden]
		public static void Exception(Exception exception)
		{
			OnLogException(Level.Error, exception, null);
		}

		[DebuggerHidden]
		public static void Exception(Exception exception, object context)
		{
			OnLogException(Level.Error, exception, context);
		}

		[DebuggerHidden]
		public static void Info(string message)
		{
			OnLog(Level.Info, message);
		}

		[DebuggerHidden]
		public static void Trace(string message)
		{
			OnLog(Level.Trace, message);
		}

		[DebuggerHidden]
		public static void Warn(string message)
		{
			OnLog(Level.Warn, message);
		}

		[DebuggerHidden]
		public static void Error(string message)
		{
			OnLog(Level.Error, message);
		}
	}
}