namespace Framework
{
	internal static class LogExtension
	{
		public static string GetName(this Log.Level level)
		{
			switch (level)
			{
				case Log.Level.Trace:
					return "Trace";
				case Log.Level.Info:
					return "Info";
				case Log.Level.Warn:
					return "Warn";
				case Log.Level.Error:
					return "Exception";
				case Log.Level.Fatal:
					return "Fatal";
				default:
					return "Unknown";
			}
		}
	}
}