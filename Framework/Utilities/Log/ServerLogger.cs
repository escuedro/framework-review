#if NOT_UNITY3D
using System;
using System.Diagnostics;
using System.Threading;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Grafana.Loki;

namespace Framework
{
	[Bind]
	public class ServerLogger
	{
		private ILogger _loki;
		private Log.Level _consoleLevel;
		private Log.Level _lokiLevel;

		[Inject]
		public ServerLogger(ServerConfig serverConfig)
		{
			_consoleLevel = serverConfig.ConsoleLog;
			_lokiLevel = serverConfig.LokiLog;
			Log.OnLog = LogLocal;
			Log.OnLogException = ExceptionLocal;

			if (!string.IsNullOrEmpty(serverConfig.LokiUrl))
			{
				AddLokiTarget(serverConfig.LokiUrl, serverConfig.GameName);
			}
			else
			{
				Log.Info("Only local log available");
			}
		}

		public void WaitForSendLogs()
		{
			if (_loki != null)
			{
				Thread.Sleep(10000);
			}
		}

		private void AddLokiTarget(string lokiAddress, string gameName)
		{
			_loki = new LoggerConfiguration().WriteTo.GrafanaLoki(lokiAddress,
					labels: new[]
					{
							new LokiLabel { Key = "game_name", Value = gameName },
							new LokiLabel { Key = "job", Value = "game_server" }
					}
			).CreateLogger();
			Log.OnLog += LogLoki;
			Log.OnLogException += ExceptionLoki;

			Log.Info($"Add log target Loki: {lokiAddress}");
		}

		[DebuggerHidden]
		private void LogLoki(Log.Level level, string message)
		{
			if (level > _lokiLevel)
			{
				try
				{
					_loki.Write((LogEventLevel)(int)level, message);
				}
				catch (Exception lokiException)
				{
					ExceptionLocal(Log.Level.Error, lokiException, null);
				}
			}
		}

		[DebuggerHidden]
		private void ExceptionLoki(Log.Level level, Exception exception, object context)
		{
			if (level > _lokiLevel)
			{
				try
				{
					_loki.Write((LogEventLevel)(int)level, exception, "exception");
				}
				catch (Exception lokiException)
				{
					ExceptionLocal(Log.Level.Error, lokiException, null);
				}
			}
		}

		[DebuggerHidden]
		private void LogLocal(Log.Level level, string message)
		{
			if (level >= _consoleLevel)
			{
				Console.WriteLine($"{level.GetName()}: {message}");
			}
		}

		[DebuggerHidden]
		private void ExceptionLocal(Log.Level level, Exception exception, object context)
		{
			if (level >= _consoleLevel)
			{
				Console.WriteLine($"{level.GetName()}: {exception}");
			}
		}
	}
}
#endif