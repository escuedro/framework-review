using System;

namespace Framework.Reactive
{
	public class EventListener<T> : Listener
	{
		public readonly Action<T> Action;
		private readonly Event<T> _event;

		public EventListener(Event<T> field, Action<T> action)
		{
			Action = action;
			_event = field;
			_event.Subscribe(this);
		}

		public override void Dispose()
		{
			_event.Unsubscribe(this);
		}

		public override bool IsListenTo(object target)
		{
			return _event == target;
		}
	}
}