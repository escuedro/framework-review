﻿using System;
using System.Collections.Generic;
using Framework.Pools;

namespace Framework.Reactive
{
	public class Field<T>
	{
		//TODO replace comparer in client to avoid T[] il2cpp code
		private static readonly EqualityComparer<T> Comparer = EqualityComparer<T>.Default;
		private T _value;
		private List<object> _listeners;

		public T PreviousValue { get; private set; }
		public readonly T StartValue;

		public T Value
		{
			get => _value;
			set
			{
				if (!Comparer.Equals(value, _value))
				{
					PreviousValue = _value;
					_value = value;
					DispatchUpdate();
				}
			}
		}

		protected void DispatchUpdate()
		{
			if (_listeners != null)
			{
				var iterateListeners = ListPool<object>.Pull(_listeners.Count);
				iterateListeners.AddRange(_listeners);
				foreach (FieldListener<T> listener in iterateListeners)
				{
					try
					{
						listener.Action(_value);
					}
					catch (Exception exception)
					{
						Log.Exception(exception);
					}
				}
				ListPool<object>.Release(iterateListeners);
			}
		}

		public Field(T value)
		{
			_value = value;
			StartValue = value;
			PreviousValue = value;
		}

		public Field()
		{
		}

		internal void Subscribe(FieldListener<T> listener)
		{
			if (_listeners == null)
			{
				_listeners = new List<object>();
			}
			_listeners.Add(listener);
		}

		internal void Unsubscribe(FieldListener<T> listener)
		{
			_listeners.Remove(listener);
		}

		public override string ToString()
		{
			if (_value == null)
			{
				return "Null";
			}

			return _value.ToString();
		}

		public static implicit operator T(Field<T> field)
		{
			if (field == null)
			{
				return default;
			}
			return field._value;
		}
	}
}