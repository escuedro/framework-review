﻿using System;
using System.Collections.Generic;
using Framework.Pools;

namespace Framework.Reactive
{
	public class ListField
	{
		protected List<object> Listeners;

		public void Subscribe(object listener)
		{
			if (Listeners == null)
			{
				Listeners = new List<object>();
			}
			Listeners.Add(listener);
		}

		public void Unsubscribe(object listener)
		{
			Listeners.Remove(listener);
		}
	}

	public class ListField<T> : ListField
	{
		public readonly Event<T> OnAdd = new Event<T>();
		public readonly Event<T> OnRemove = new Event<T>();
		
		public int Count => _list.Count;
		
		private readonly List<T> _list;
		
		public ListField(List<T> list)
		{
			_list = list;
		}

		public ListField()
		{
			_list = new List<T>();
		}

		public Enumerator GetEnumerator()
		{
			return new Enumerator(_list);
		}

		public void Add(T value)
		{
			_list.Add(value);
			OnAdd.Dispatch(value);
			DispatchEvent();
		}

		public void AddRange(IEnumerable<T> values)
		{
			_list.AddRange(values);
			foreach (var value in values)
			{
				OnAdd.Dispatch(value);
			}
			DispatchEvent();
		}

		public void Clear()
		{
			for (int i = _list.Count - 1; i >= 0; i--)
			{
				var value = _list[i];
				_list.RemoveAt(i);
				OnRemove.Dispatch(value);
			}
			DispatchEvent();
		}

		public void Remove(T value)
		{
			_list.Remove(value);
			OnRemove.Dispatch(value);
			DispatchEvent();
		}

		public void RemoveAt(int index)
		{
			var value = _list[index];
			_list.RemoveAt(index);
			OnRemove.Dispatch(value);
			DispatchEvent();
		}

		public void RemoveLast()
		{
			var index = _list.Count - 1;
			var value = _list[index];
			_list.RemoveAt(index);
			OnRemove.Dispatch(value);
			DispatchEvent();
		}

		public void RemoveFirst()
		{
			var value = _list[0];
			_list.RemoveAt(0);
			OnRemove.Dispatch(value);
			DispatchEvent();
		}

		private void DispatchEvent()
		{
			if (Listeners != null)
			{
				var iterateListeners = ListPool<object>.Pull(Listeners.Count);
				iterateListeners.AddRange(Listeners);
				foreach (ListFieldListener<T> listener in iterateListeners)
				{
					try
					{
						listener.Action(this);
					}
					catch (Exception exception)
					{
						Log.Exception(exception);
					}
				}
				ListPool<object>.Release(iterateListeners);
			}
		}

		public struct Enumerator
		{
			private int _position;
			private readonly List<T> _list;

			public bool MoveNext()
			{
				_position++;
				return _position < _list.Count;
			}

			public Enumerator(List<T> list) : this()
			{
				_position = -1;
				_list = list;
			}

			public T Current => _list[_position];
		}

		public T this[int i] => _list[i];
	}
}