using System;
using System.Collections.Generic;

namespace Framework.Reactive
{
	public abstract class ModelListener
	{
		private List<Listener> _listeners;

		protected IFieldListener Subscribe<T>(Field<T> field, Action<T> action)
		{
			if (_listeners == null)
			{
				_listeners = new List<Listener>();
			}

			FieldListener<T> fieldListener = new FieldListener<T>(field, action);
			_listeners.Add(fieldListener);
			return fieldListener;
		}

		protected IFieldListener Subscribe<T>(ListField<T> listField, Action<ListField<T>> action)
		{
			if (_listeners == null)
			{
				_listeners = new List<Listener>();
			}

			ListFieldListener<T> fieldListener = new ListFieldListener<T>(listField, action);
			_listeners.Add(fieldListener);
			return fieldListener;
		}

		protected void Subscribe<T>(Event<T> eventField, Action<T> action)
		{
			if (_listeners == null)
			{
				_listeners = new List<Listener>();
			}

			_listeners.Add(new EventListener<T>(eventField, action));
		}

		protected void Dispose()
		{
			if (_listeners != null)
			{
				foreach (Listener fieldListener in _listeners)
				{
					fieldListener.Dispose();
				}
				_listeners.Clear();
			}
		}
	}
}