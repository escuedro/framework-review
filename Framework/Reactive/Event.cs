using System.Collections.Generic;
using Framework.Pools;

namespace Framework.Reactive
{
	public class Event<T>
	{
		private List<object> _listeners;

		public void Dispatch(T value)
		{
			if (_listeners != null)
			{
				List<object> iterateListeners = ListPool<object>.Pull(_listeners.Count);
				iterateListeners.AddRange(_listeners);
				foreach (EventListener<T> listener in iterateListeners)
				{
					listener.Action(value);
				}
				ListPool<object>.Release(iterateListeners);
			}
		}

		internal void Subscribe(EventListener<T> listener)
		{
			if (_listeners == null)
			{
				_listeners = new List<object>();
			}
			_listeners.Add(listener);
		}

		internal void Unsubscribe(EventListener<T> listener)
		{
			_listeners.Remove(listener);
		}
	}
}