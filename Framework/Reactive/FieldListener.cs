﻿using System;

namespace Framework.Reactive
{
	public abstract class Listener
	{
		public abstract void Dispose();
		public abstract bool IsListenTo(object target);
	}

	public interface IFieldListener
	{
		void Update();
	}

	public class FieldListener<T> : Listener, IFieldListener
	{
		public readonly Action<T> Action;
		private readonly Field<T> _field;

		public FieldListener(Field<T> field, Action<T> action)
		{
			Action = action;
			_field = field;
			_field.Subscribe(this);
		}

		public override void Dispose()
		{
			_field.Unsubscribe(this);
		}

		public override bool IsListenTo(object target)
		{
			return _field == target;
		}

		public void Update()
		{
			Action(_field.Value);
		}
	}

	public class ListFieldListener<T> : Listener, IFieldListener
	{
		public readonly Action<ListField<T>> Action;
		private readonly ListField<T> _listField;

		public ListFieldListener(ListField<T> listField, Action<ListField<T>> action)
		{
			Action = action;
			_listField = listField;
			_listField.Subscribe(this);
		}

		public override void Dispose()
		{
			_listField.Unsubscribe(this);
		}

		public override bool IsListenTo(object target)
		{
			return _listField == target;
		}

		public void Update()
		{
			Action(_listField);
		}
	}
}