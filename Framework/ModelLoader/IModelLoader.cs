namespace Framework
{
	public interface IModelLoader<TRootModel>
	{
		void Load(IContainer container, TRootModel rootModel);
		TRootModel Save(IContainer container);
	}
}