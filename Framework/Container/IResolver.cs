﻿using System;

namespace Framework
{
	public interface IResolver
	{
		T Resolve<T>();
		object Resolve(Type type);
	}
}