using System;

namespace Framework
{
	public interface IContainer : IInjector, IResolver, IBinder
	{
		T Instantiate<T>();
		object Instantiate(Type type);
	}
}