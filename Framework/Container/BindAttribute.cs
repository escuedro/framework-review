using System;

namespace Framework
{
	[MeansImplicitUse]
	[AttributeUsage(AttributeTargets.Class)]
	public class BindAttribute : Attribute
	{
		[CanBeNull]
		public Type BindTo { get; set; }
		public bool AsTransient { get; set; }
	}
}