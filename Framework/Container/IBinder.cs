﻿using System;

namespace Framework
{
	public interface IBinder
	{
		public void BindInstance<T>(T instance);
		public void BindInstance(Type type, object instance);
	}
}