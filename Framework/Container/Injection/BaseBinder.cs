using System;

namespace Framework.Injection
{
	public class BaseBinder
	{
		public virtual void Bind(Container container)
		{
			throw new Exception("Please run DI codegenerator");
		}
	}
}