namespace Framework.Injection
{
	internal interface IInternalInjector
	{
		object Instantiate();
		void Inject(object instance);
	}
}