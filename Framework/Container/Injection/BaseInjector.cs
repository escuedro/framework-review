using System;

namespace Framework.Injection
{
	public abstract class BaseInjector : IInternalInjector
	{
		// ReSharper disable once MemberCanBeProtected.Global
		// ReSharper disable once InconsistentNaming
		// ReSharper disable once UnassignedField.Global
		public IContainer _container;

		public virtual object Instantiate()
		{
			throw new Exception($"Cannot instantiate {GetType().Name} without public constructor with [Inject]");
		}

		public virtual void Inject(object instance)
		{
		}
	}
}