using System;

namespace Framework.Injection
{
	public class UsageException: Exception
	{
		public UsageException(string message) : base(message)
		{
		}
	}
}