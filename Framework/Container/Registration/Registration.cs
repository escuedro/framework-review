namespace Framework.Injection
{
	public class Registration
	{
		public readonly bool IsTransient;

		public Registration(bool isTransient)
		{
			IsTransient = isTransient;
		}
	}
}