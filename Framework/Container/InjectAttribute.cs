using System;

namespace Framework
{
	[MeansImplicitUse]
	[AttributeUsage(
			AttributeTargets.Constructor |
			AttributeTargets.Method |
			AttributeTargets.Property |
			AttributeTargets.Field |
			AttributeTargets.Parameter)]
	public class InjectAttribute : Attribute
	{
		[CanBeNull]
		public object Id { get; set; } //  TODO realize ondemand
		public bool Optional { get; set; } //  TODO realize ondemand
	}
}