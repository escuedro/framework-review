﻿namespace Framework
{
	public interface IInjector
	{
		void Inject(object target);
	}
}