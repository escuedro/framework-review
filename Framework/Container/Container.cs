using System;
using System.Collections.Generic;
using Framework.Injection;

namespace Framework
{
	public class Container : IContainer
	{
		private readonly Dictionary<Type, object> _instances = new Dictionary<Type, object>();
		private readonly Dictionary<Type, Registration> _registrations = new Dictionary<Type, Registration>();
		private readonly Dictionary<Type, IInternalInjector> _injectors = new Dictionary<Type, IInternalInjector>();
#if !NOT_UNITY3D
		private readonly Type _stopType = typeof(UnityEngine.MonoBehaviour);
#endif
		public T Resolve<T>()
		{
			return (T)Resolve(typeof(T));
		}

		public T Instantiate<T>()
		{
			return (T)Instantiate(typeof(T));
		}

		public object Instantiate(Type type)
		{
			if (_injectors.TryGetValue(type, out IInternalInjector injector))
			{
				object instance = injector.Instantiate();
				InjectRecursively(instance);
				return instance;
			}

			throw new UsageException($"Fail instantiate {type.FullName}. Try to rebuild generated code.");
		}

		public void Inject(object target)
		{
			InjectRecursively(target);
		}

		private void InjectRecursively(object target)
		{
			Type type = target.GetType();
			while (type != null)
			{
				if (_injectors.TryGetValue(type, out IInternalInjector injector))
				{
					injector.Inject(target);
				}
				type = type.BaseType;
#if !NOT_UNITY3D
				if (type == _stopType)
				{
					return;
				}
#endif
			}
		}

		public object Resolve(Type type)
		{
			if (_instances.TryGetValue(type, out object instance))
			{
				return instance;
			}
			if (_registrations.TryGetValue(type, out Registration registration) &&
					_injectors.TryGetValue(type, out IInternalInjector injector))
			{
				instance = injector.Instantiate();
				InjectRecursively(instance);
				if (!registration.IsTransient)
				{
					_instances.Add(type, instance);
				}
				return instance;
			}

			throw new UsageException($"Unknown type {type.FullName} to inject. Try to rebuild generated code.");
		}

		public void AddInjector(Type type, BaseInjector injector)
		{
			injector._container = this;
			_injectors.Add(type, injector);
		}

		public void AddRegistration(Type type, bool isTransient)
		{
			_registrations.Add(type, new Registration(isTransient));
		}

		public void BindInstance<T>(T instance)
		{
			BindInstance(typeof(T), instance);
		}

		public void Clear()
		{
			_instances.Clear();
		}

		public void BindInstance(Type type, object instance)
		{
			_instances.Add(type, instance);
		}
	}
}