﻿#if !NOT_UNITY3D
using System;
using Framework.Model;

namespace Framework.QualityGates
{
	[AutoTestExceptedAttribute]
	public abstract class SingleStaticQualityGate<T> : QualityGate where T : class
	{
		protected abstract void CheckAsset(T model);
		private T GetStatic() => Statics.GetSingle<T>();

		protected override void Test()
		{
			T model = GetStatic();
			try
			{
				CheckAsset(model);
			}
			catch (Exception exception)
			{
				Collect(new Error(new Exception($"asset {model} has error {exception.Message}"), model));
			}
		}
	}
}
#endif