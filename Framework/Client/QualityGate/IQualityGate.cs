using System.Collections.Generic;

namespace Framework.QualityGates
{
	public interface IQualityGate
	{
		public bool StartOnPlay { get; }
		List<Error> RunTest();
	}
}