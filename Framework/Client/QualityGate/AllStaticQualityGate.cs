﻿#if !NOT_UNITY3D
using System;
using Framework.Model;

namespace Framework.QualityGates
{
	[AutoTestExceptedAttribute]
	public abstract class AllStaticQualityGate<T> : QualityGate where T : StaticObject
	{
		protected abstract void CheckAsset(T model);
		private T[] GetStatic() => Statics.GetAll<T>();

		protected override void Test()
		{
			T[] statics = GetStatic();
			foreach (var model in statics)
			{
				try
				{
					CheckAsset(model);
				}
				catch (Exception exception)
				{
					Collect(new Error(new Exception($"{model} {model.Id} has error {exception.Message}"), model));
				}
			}
		}
	}
}
#endif