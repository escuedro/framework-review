﻿#if !NOT_UNITY3D
using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.QualityGates
{
	[AutoTestExceptedAttribute]
	public abstract class SceneComponentQualityGate<T> : QualityGate where T : Component
	{
		protected abstract void CheckAsset(T asset);

		protected override void Test()
		{
			T[] components = Object.FindObjectsOfType<T>(true);
			foreach (T component in components)
			{
				if (component == null)
				{
					Collect(new Exception($"component '{component.name}' is null"));
				}
				else
				{
					try
					{
						CheckAsset(component);
					}
					catch (Exception exception)
					{
						Collect(new Error(new Exception($"asset has error {exception.Message}"), component));
					}
				}
			}
		}
	}
}
#endif