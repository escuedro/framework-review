#if !NOT_UNITY3D
using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.QualityGates
{
	public enum PostPrefabAction
	{
		None,
		Save
	}

	public abstract class PrefabQualityGate<T> : QualityGate where T : Object
	{
		protected abstract string GetAssetFilter();

		public virtual bool IncludePath(string path)
		{
			return true;
		}

		protected abstract PostPrefabAction CheckAsset(T asset);

		protected override void Test()
		{
			string[] assetsGuids = AssetDatabase.FindAssets(GetAssetFilter());

			foreach (string assetsGuid in assetsGuids)
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(assetsGuid);
				if (!IncludePath(assetPath))
				{
					continue;
				}
				GameObject prefab = PrefabUtility.LoadPrefabContents(assetPath);
				T asset = prefab.GetComponent<T>();
				if (asset == null)
				{
					Collect(new Exception($"asset at path '{assetPath}' is null"));
				}
				else
				{
					try
					{
						if (CheckAsset(asset) == PostPrefabAction.Save)
						{
							PrefabUtility.SaveAsPrefabAsset(prefab, assetPath);
						}
					}
					catch (Exception exception)
					{
						Collect(new Error(new Exception($"asset {assetPath} has error {exception.Message}"), asset));
					}
				}
			}
		}
	}
}
#endif