using System;
using System.Collections.Generic;

namespace Framework.QualityGates
{
	public abstract class QualityGate : IQualityGate
	{
		private readonly List<Error> _errors = new List<Error>();
		private readonly List<string> _warnings = new List<string>();

		public abstract bool StartOnPlay { get; }

		public List<Error> RunTest()
		{
			try
			{
				Test();
			}
			catch (Exception exception)
			{
				_errors.Add(new Error(exception));
			}
			return _errors;
		}

		protected abstract void Test();

		protected void Collect(Exception exception)
		{
			_errors.Add(new Error(exception));
		}

		protected void Collect(Error error)
		{
			_errors.Add(error);
		}

		protected void Warning(string text)
		{
			_warnings.Add(text);
		}

		protected void Collect(IEnumerable<Error> exceptions)
		{
			foreach (Error exception in exceptions)
			{
				_errors.Add(exception);
			}
		}

		protected void Collect(IEnumerable<Exception> exceptions)
		{
			foreach (Exception exception in exceptions)
			{
				_errors.Add(new Error(exception));
			}
		}
	}
}