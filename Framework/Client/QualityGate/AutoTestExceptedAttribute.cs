﻿using System;

namespace Framework.QualityGates
{
	[MeansImplicitUse]
	[AttributeUsage(AttributeTargets.Class)]
	public class AutoTestExceptedAttribute : Attribute
	{
	}
}