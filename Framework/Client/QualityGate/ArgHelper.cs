using System.IO;

namespace Framework.QualityGates
{
	public static class ArgHelper
	{
		[Pure]
		public static string Get(
				string sourceFilePath,
				int sourceLineNumber)
		{
			string[] fileLines = File.ReadAllLines(sourceFilePath);
			string line = fileLines[sourceLineNumber - 1];
			int start = line.IndexOf('(') + 1;
			int end = line.LastIndexOf(')');
			int firstComma = line.IndexOf(',', start);
			if (firstComma > start && firstComma < end)
			{
				end = firstComma;
			}
			return line.Substring(start, end - start);
		}
	}
}