using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Framework.Model;

namespace Framework.QualityGates
{
	public static class Asserts
	{
		public static void NotEmpty(string value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (string.IsNullOrWhiteSpace(value))
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}

		public static void FileExists(string path,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (!File.Exists(path))
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}

		public static void NotNull<T>(T value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0) where T : class
		{
#if !NOT_UNITY3D
			if (typeof(T) == typeof(UnityEngine.Transform))
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' cannot be checked for null. Use NotNullTransform instead.");
			}
#endif
			if (value == null)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}

		public static void NotEmpty<T>(T[] array,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (array == null || array.Length == 0)
			{
				throw new Exception($"array '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}

		public static void NotEmpty<T>(List<T> array,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (array == null || array.Count == 0)
			{
				throw new Exception($"array '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}

		public static void Equals(int value1,
				int value2,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value1 != value2)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value1.ToString()}' is non equals value '{value2.ToString()}'");
			}
		}

		public static void Equals(uint value1,
				uint value2,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value1 != value2)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value1.ToString()}' is non equals value '{value2.ToString()}'");
			}
		}

		public static void NotEquals(int value1,
				int value2,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value1 == value2)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value1.ToString()}' is equals value '{value2.ToString()}'");
			}
		}

		public static void NotZero(float value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (Math.Abs(value) < float.Epsilon)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is zero");
			}
		}

		public static void NotZero(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value == 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is zero");
			}
		}

		public static void Positive(long value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value < 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is negative");
			}
		}

		public static void Positive(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value < 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is negative");
			}
		}

		public static void Negative(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value >= 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is positive");
			}
		}

		public static void BiggerThanZero(uint value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value <= 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is zero");
			}
		}

		public static void BiggerThanZero(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value <= 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is negative or zero");
			}
		}

		public static void LessThanZero(uint value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value >= 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is negative");
			}
		}

		public static void LessThanZero(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value >= 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is negative");
			}
		}

		public static void StaticId<TStatic>(int value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0) where TStatic : StaticObject
		{
			if (value == 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
			Statics.Get<TStatic>(value);
		}

		public static void StaticArray<TStatic>(int[] array,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0) where TStatic : StaticObject
		{
			if (array == null || array.Length == 0)
			{
				throw new Exception($"array '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}

			foreach (int id in array)
			{
				if (id == 0)
				{
					throw new Exception($"array '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' have 0 id");
				}
				Statics.Get<TStatic>(id);
			}
		}

		public static void Enum<TEnum>(TEnum value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			Type enumType = typeof(TEnum);
			if (!System.Enum.IsDefined(enumType, value))
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' type '{enumType.Name}' is unknown enum value {value}");
			}
		}

		public static void InRange(int value,
				int min,
				int maxExc,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value < min || value >= maxExc)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value.ToString()}' out of range '[{min.ToString()},{maxExc.ToString()} )'");
			}
		}

		public static void InRange(uint value,
				uint min,
				uint maxExc,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value < min || value >= maxExc)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value.ToString()}' out of range '[{min.ToString()},{maxExc.ToString()} )'");
			}
		}

		public static void ValueRange(ValueRange valueRange,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (valueRange.Min > valueRange.Max)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' valueRange min larger than max '[{valueRange.Min.ToString()},{valueRange.Max.ToString()} )'");
			}
			NotZero(valueRange.Min);
			NotZero(valueRange.Max);
		}

		public static void ValueRangeBorders(ValueRange valueRange,
				int min,
				int max,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (valueRange.Min < min || valueRange.Max > max)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' valueRange not in borders '[{min},{max}]'");
			}
			NotZero(valueRange.Min);
			NotZero(valueRange.Max);
		}

		public static void True(bool expression,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (!expression)
			{
				throw new Exception(
						$"expression '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is not true");
			}
		}

#if !NOT_UNITY3D
		public static TComponent Resource<TComponent>(string value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0) where TComponent : UnityEngine.Object
		{
			Type componentType = typeof(TComponent);
			Object component = UnityEngine.Resources.Load(value, componentType);
			if (component == null)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' asset type '{componentType.Name}' not found {value}");
			}
			return (TComponent)component;
		}

		public static void NotNullTransform(UnityEngine.Transform value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value == null)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is empty");
			}
		}
#endif
		public static void Translation(string translationKey,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (string.IsNullOrEmpty(translationKey)) //TODO check in translations repository
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' unknown translation key {translationKey}");
			}
		}
	}
}