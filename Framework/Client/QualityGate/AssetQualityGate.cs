#if !NOT_UNITY3D
using System;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Framework.QualityGates
{
	public abstract class AssetQualityGate<T> : QualityGate where T : Object
	{
		protected abstract string GetAssetFilter();
		protected virtual string[] GetAssetFolders() => null;
		protected abstract void CheckAsset(T asset, string assetPath);

		protected override void Test()
		{
			string[] assetsGuids = AssetDatabase.FindAssets(GetAssetFilter(), GetAssetFolders());

			foreach (string assetsGuid in assetsGuids)
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(assetsGuid);
				T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
				if (asset == null)
				{
					Collect(new Exception($"asset at path '{assetPath}' is null"));
				}
				else
				{
					try
					{
						CheckAsset(asset, assetPath);
					}
					catch (Exception exception)
					{
						Collect(new Error(new Exception($"asset {assetPath} has error {exception.Message}"), asset));
					}
				}
			}
		}
	}
}
#endif