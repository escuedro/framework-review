using System;

namespace Framework.QualityGates
{
	public class Error
	{
		public readonly Exception Exception;
		public readonly object Context;

		public Error(Exception exception, object context = null)
		{
			Exception = exception;
			Context = context;
		}
	}
}