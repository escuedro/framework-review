#if !NOT_UNITY3D

using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.Extension
{
	public static class GameObjectExtension
	{
		public static T Create<T>(this T prefab, Transform parent) where T : Object
		{
			return Object.Instantiate(prefab, parent, false);
		}

		public static void DestroyChildren(this Transform transform)
		{
			int childCount = transform.childCount;
			for (int i = childCount - 1; i >= 0; i--)
			{
				Object.Destroy(transform.GetChild(i).gameObject);
			}
		}

		public static Transform FindMainParent(this Transform transform)
		{
			Transform parent = transform.parent;
			if (parent != null)
			{
				return parent.FindMainParent();
			}
			return transform;
		}

		public static IEnumerable<T> FindTypeInGameObjectAndChildren<T>(this GameObject go)
				where T : Component
		{
			return go.FindTypeInGameObjectAndChildren<T>(null);
		}
		public static IEnumerable<T> FindTypeInGameObjectAndChildren<T>(this GameObject go, Type[] exceptions) where T : Component
		{
			List<T> sortedComponents = new List<T>();
			T[] components = go.GetComponents<T>();
			foreach (var type in components)
			{
				if (type != null && !ExceptedType(type.GetType(), exceptions))
				{
					sortedComponents.Add(type);
				}
			}

			for (int i = 0; i < go.transform.childCount; i++)
			{
				sortedComponents.AddRange(go.transform.GetChild(i).gameObject.FindTypeInGameObjectAndChildren<T>(exceptions));
			}
			return sortedComponents;
		}

		private static bool ExceptedType(Type type, Type[] exceptedTypes)
		{
			if(exceptedTypes!=null)
			{
				foreach (Type exceptedType in exceptedTypes)
				{
					if (type == exceptedType) return true;
				}
			}
			return false;
		}
	}
}

#endif