﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;

namespace Framework.Client.Translations
{
    public static class TranslationKeysPicker
    {
        public static TranslationKey[] GetVariants(TranslationKey sourceWord, int variantsAmount)
        {
            TranslationKey[] allVariants = Translation.GetTranslationKeys();
            List<TranslationKey> correctVariants = new List<TranslationKey>();
            foreach (TranslationKey translationKey in allVariants)
            {
                bool contains = translationKey.Value.IndexOf(sourceWord.Value, StringComparison.OrdinalIgnoreCase) >= 0;
                if (contains)
                {
                    correctVariants.Add(translationKey);
                }
            }
            
            int correctVariantsAmount = correctVariants.Count < variantsAmount ? correctVariants.Count : variantsAmount;
            TranslationKey[] neededVariants = new TranslationKey[correctVariantsAmount];
            for (int i = 0; i < correctVariantsAmount; i++)
            {
                neededVariants[i] = correctVariants[i];
            }

            return neededVariants;
        }
    }
}

#endif