﻿using Framework.Model.Localization;

#if !NOT_UNITY3D

namespace Framework.Client.Translations
{
    public class CsvFileLoaderSettings
    {
        public readonly string Path;
        public readonly Language Language;

        public CsvFileLoaderSettings(string path, Language language)
        {
            Path = path;
            Language = language;
        }
    }
}

#endif