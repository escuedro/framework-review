﻿using Framework.Model.Localization;

#if !NOT_UNITY3D

namespace Framework.Client.Translations
{
    public interface ICsvFileLoader
    {
        public byte[] Load();
        public Language Language { get; }
    }
}

#endif