﻿using Framework.Hasher;
using Framework.Serialization;

namespace Framework.Client.Translations
{
	[GenerateSerialize]
	[GenerateHash]
	public struct KeyTranslationPair
	{
		public TranslationKey TranslationKey;
		public string Translation;
	}
}