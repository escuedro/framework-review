﻿#if !NOT_UNITY3D

namespace Framework.Client.Translations
{
    public class CsvFileReaderSettings
    {
        public readonly char SeparationChar;
        public readonly int LinesToSkip;

        public CsvFileReaderSettings(char separationChar, int linesToSkip)
        {
            SeparationChar = separationChar;
            LinesToSkip = linesToSkip;
        }
    }
}

#endif