﻿#if !NOT_UNITY3D

namespace Framework.Client.Translations
{
    public interface ICsvFileReader
    {
        public KeyTranslationPair[] Read(byte[] data);
    }
}

#endif