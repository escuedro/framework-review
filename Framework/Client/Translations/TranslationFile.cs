﻿#if !NOT_UNITY3D

using System.Collections.Generic;
using Framework.Collections;

namespace Framework.Client.Translations
{
    [System.Serializable]
    public class TranslationFile
    {
        private readonly Dictionary<TranslationKey, string> _translations = new Dictionary<TranslationKey, string>();

        public TranslationFile(ConstArray<KeyTranslationPair> keyTranslationPairs)
        {
            foreach (KeyTranslationPair translationPair in keyTranslationPairs)
            {
                _translations.Add(translationPair.TranslationKey, translationPair.Translation);
            }
        }

        public bool TryGetTranslation(TranslationKey translationKey, out string result)
        {
            return _translations.TryGetValue(translationKey, out result);
        }

        public TranslationKey[] GetTranslationKeys()
        {
            List<TranslationKey> keys = new List<TranslationKey>(_translations.Keys.Count);
            foreach (TranslationKey translationsKey in _translations.Keys)
            {
                keys.Add(translationsKey);
            }

            return keys.ToArray();
        }
    }
}

#endif