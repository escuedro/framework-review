﻿using Framework.Model.Localization;

namespace Framework.Client.Translations
{
	[System.Serializable]
	public struct LanguagePathPair
	{
		public Language Language;
		public string TranslationFilePath;
	}
}