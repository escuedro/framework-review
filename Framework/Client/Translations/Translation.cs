﻿#if !NOT_UNITY3D

using System.Collections.Generic;
using Framework.Model.Localization;
using UnityEngine;

namespace Framework.Client.Translations
{
	public static class Translation
	{
		private static TranslationFile _currentTranslationFile;

		private static Dictionary<Language, TranslationFile>
				_translations = new Dictionary<Language, TranslationFile>();
		private static HashSet<TranslationKey> _translationKeys = new HashSet<TranslationKey>();

		public static bool WasInitialized { private set; get; }

		internal static bool TryTranslate(TranslationKey translationKey, out string result)
		{
			if (_currentTranslationFile == null)
			{
				result = "";
				return false;
			}
			return _currentTranslationFile.TryGetTranslation(translationKey, out result);
		}

		public static TranslationKey[] GetTranslationKeys()
		{
			List<TranslationKey> translationKeys = new List<TranslationKey>(_translationKeys.Count);
			foreach (TranslationKey translationKey in _translationKeys)
			{
				translationKeys.Add(translationKey);
			}

			return translationKeys.ToArray();
		}

		public static void SetCurrentLanguage(Language language)
		{
			_translations.TryGetValue(language, out TranslationFile translationFile);
			foreach (TranslationKey translationKey in translationFile.GetTranslationKeys())
			{
				if (!_translationKeys.Contains(translationKey))
				{
					_translationKeys.Add(translationKey);
				}
			}
			_currentTranslationFile = translationFile;
			UpdateAllTranslatedTexts();
		}

		private static void UpdateAllTranslatedTexts()
		{
			TranslatedText[] translatedText = GameObject.FindObjectsOfType<TranslatedText>();
			foreach (TranslatedText text in translatedText)
			{
				text.UpdateTranslation();
			}
		}

		public static void SetTranslationFile(Language language, TranslationFile translationFile)
		{
			if (_translations.ContainsKey(language))
			{
				_translations[language] = translationFile;
			}
			else
			{
				_translations.Add(language, translationFile);
			}
			if (!WasInitialized)
			{
				WasInitialized = true;
			}
		}
	}
}

#endif