﻿#if !NOT_UNITY3D

using System.Collections.Generic;
using System.Text;

namespace Framework.Client.Translations
{
    public class CsvFileReader : ICsvFileReader
    {
        private readonly CsvFileReaderSettings _settings;

        public CsvFileReader(CsvFileReaderSettings settings)
        {
            _settings = settings;
        }

        public KeyTranslationPair[] Read(byte[] data)
        {
            string fileString = Encoding.UTF8.GetString(data);
            string[] words = fileString.Split(_settings.SeparationChar, '\n');

            KeyTranslationPair[] keyTranslationPairs = new KeyTranslationPair[words.Length / 2];

            int linesToSkipCounter = _settings.LinesToSkip;

            int iterationsCounter = 0;

            for (int i = 0; i < words.Length - 1; i += 2)
            {
                if (linesToSkipCounter > 0)
                {
                    linesToSkipCounter--;
                    continue;
                }
                keyTranslationPairs[iterationsCounter] = new KeyTranslationPair
                        {TranslationKey = new TranslationKey(words[i]), Translation = words[i + 1]};
                iterationsCounter++;
            }

            return keyTranslationPairs;
        }
    }
}

#endif