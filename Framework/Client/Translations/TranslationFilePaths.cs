﻿namespace Framework.Client.Translations
{
	[System.Serializable]
	public class TranslationFilePaths
	{
		public LanguagePathPair[] FilePaths;
	}
}