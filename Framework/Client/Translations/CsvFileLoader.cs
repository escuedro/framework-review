﻿#if !NOT_UNITY3D

using System.IO;
using System.Text;
using Framework.Model.Localization;

namespace Framework.Client.Translations
{
    public class CsvFileLoader : ICsvFileLoader
    {
        private CsvFileLoaderSettings _settings;

        public CsvFileLoader(CsvFileLoaderSettings settings)
        {
            _settings = settings;
        }

        public byte[] Load()
        {
            string fileString = File.ReadAllText(_settings.Path);
            return Encoding.UTF8.GetBytes(fileString);
        }

        public Language Language => _settings.Language;
    }
}

#endif