﻿#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework.Client.Translations
{
	public class TranslatedText : MonoBehaviour
	{
		[SerializeField]
		private UIText _text;
		[SerializeField]
		[CanBeNull]
		private string _translationKey;

		public bool NeedToFindVariant => _needToFindVariant;
		private bool _needToFindVariant;

		public string KeyTranslation => _translation;
		private string _translation = "";

		private object[] _arguments;

		private void Start()
		{
			TryGetComponent(out _text);
			UpdateTranslation();
		}

		private void Reset()
		{
			TryGetComponent(out _text);
		}

		public void SetTranslationKey(TranslationKey translationKey)
		{
			_needToFindVariant = !Translation.TryTranslate(translationKey, out string translation);
			if (!_needToFindVariant)
			{
				_text.text = translation;
				_translation = translation;
				_translationKey = translationKey.Value;
			}
			else
			{
				if (Translation.WasInitialized)
				{
					_text.text = translationKey.Value;
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
#endif
					{
						Log.Exception(new Exception($"Unknown translation key '{translationKey.Value}'"), this);
					}
				}
				_translation = "";
			}
		}

		public void UpdateTranslation()
		{
			if (!string.IsNullOrEmpty(_translationKey))
			{
				SetTranslationKey(new TranslationKey(_translationKey));
				if (_arguments != null)
				{
					InputFormattedData(_arguments);
				}
			}
		}

		public void InputFormattedData(params object[] data)
		{
			_arguments = data;
			string formattedData = String.Format(_translation, data);
			_text.text = formattedData;
		}

		public void SetRaw(string text)
		{
			_text.text = text;
		}
	}
}

#endif