﻿#if !NOT_UNITY3D
using System.Collections.Generic;
using Framework.Futures;
using Framework.RPC;

namespace Framework
{
	[Bind(BindTo = typeof(IController))]
	public class Controller : IController, IFutureHandler
	{
		[Inject]
		public IResolver Resolver;
		[Inject]
		public IClientRpcController ClientRpcController;

		private Dictionary<IFuture, AsyncCommand> _futureCommandDictionary =
				new Dictionary<IFuture, AsyncCommand>();
		public void Execute(SyncCommand syncCommand)
		{
			syncCommand.Resolver = Resolver;
			syncCommand.Execute();
		}

		public IFuture Execute(AsyncCommand asyncCommand)
		{
			Promise commandPromise = new Promise();
			asyncCommand.SetPromise(commandPromise);
			asyncCommand.Resolver = Resolver;
			asyncCommand.Execute();
			Future future = new Future(commandPromise);
			if (commandPromise.IsPending)
			{
				_futureCommandDictionary.Add(future, asyncCommand);
				future.Finally(this);
			}
			return future;
		}

		public IFuture Execute(RpcCommand rpcCommand)
		{
			return ClientRpcController.Execute(rpcCommand);
		}

		public void HandleFuture(IFuture future)
		{
			_futureCommandDictionary.Remove(future);
		}
	}
}
#endif