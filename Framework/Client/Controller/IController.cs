﻿using Framework.Futures;
using Framework.RPC;

namespace Framework
{
	public interface IController
	{
		public void Execute(SyncCommand syncCommand);

		public IFuture Execute(AsyncCommand asyncCommand);

		public IFuture Execute(RpcCommand rpcCommand);
	}
}