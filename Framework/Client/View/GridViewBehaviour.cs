﻿#if !NOT_UNITY3D
using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Reactive;
using Framework.View;
using UnityEngine;

namespace Framework.Client.View
{
	public class GridViewBehaviour : ViewBehaviour
	{
		[SerializeField]
		private LayoutView _layoutView;
		private IListView _listView;

		private Type _viewType;
		private readonly Dictionary<object, Component> _viewItemsDictionary = new Dictionary<object, Component>();

		private IPrefabProvider _prefabProvider;
		private bool _injectElements;

		public Iterator<TModel, TView> Init<TModel, TView>(IPrefabProvider prefabProvider,
				ListField<TModel> modelListField,
				bool injectElements,
				Action<TModel, TView> creatingCallback,
				Predicate<TModel> showCondition = null,
				IComparer<TModel> sortComparer = null)
				where TModel : class
				where TView : Component
		{
			_prefabProvider = prefabProvider;
			_injectElements = injectElements;
			Iterator<TModel, TView> iterator =
					new Iterator<TModel, TView>(this, _layoutView, modelListField, showCondition, sortComparer, creatingCallback);
			_listView = iterator;
			iterator.DrawAllElements();
			return iterator;
		}

		private void AddElementWithoutRecalculate(object model, int positionInHierarchy)
		{
			Component viewItem = _prefabProvider.InstantiateAt(_viewType, _layoutView.Content, _injectElements);
			viewItem.transform.SetSiblingIndex(positionInHierarchy);
			_viewItemsDictionary.Add(model, viewItem);
			_listView.InitItem(model, viewItem);
		}

		private void AddElement(object model, int positionInHierarchy)
		{
			AddElementWithoutRecalculate(model, positionInHierarchy);
			_listView.RecalculateLayout();
		}

		private void RemoveElement(object model)
		{
			Component viewItem = _viewItemsDictionary[model];
			Destroy(viewItem.gameObject);
			_viewItemsDictionary.Remove(model);
			_listView.RecalculateLayout();
		}

		private bool ContainsElement(object model)
		{
			return _viewItemsDictionary.ContainsKey(model);
		}

		protected override void AfterDestroy()
		{
			_listView.Dispose();
		}

		public class Iterator<TModel, TView> : ModelListener, IListView where TModel : class where TView : Component
		{
			private readonly GridViewBehaviour _gridViewBehaviour;
			private readonly LayoutView _layoutView;
			private readonly Action<TModel, TView> _creatingCallback;
			
			private readonly ListField<TModel> _modelListField;
			private readonly Predicate<TModel> _showCondition;
			private readonly IComparer<TModel> _sortComparer;

			public Iterator(GridViewBehaviour gridViewBehaviour,
					LayoutView layoutView,
					ListField<TModel> modelListField,
					Predicate<TModel> showCondition,
					IComparer<TModel> sortComparer,
					Action<TModel, TView> creatingCallback)
			{
				_gridViewBehaviour = gridViewBehaviour;
				_layoutView = layoutView;
				_modelListField = modelListField;
				_showCondition = showCondition;
				_sortComparer = sortComparer;
				_creatingCallback = creatingCallback;
				Subscribe(modelListField.OnAdd, AddElement);
				Subscribe(modelListField.OnRemove, RemoveElement);
				_gridViewBehaviour._viewType = typeof(TView);
			}

			private void AddElement(TModel element)
			{
				if (_showCondition == null || _showCondition(element))
				{
					_gridViewBehaviour.AddElement(element, GetSortedElements().IndexOf(element));
				}
			}

			private List<TModel> GetSortedElements()
			{
				List<TModel> elementsList = new List<TModel>();
				foreach (TModel model in _modelListField)
				{
					elementsList.Add(model);
				}
				if (_sortComparer != null)
				{
					elementsList.Sort(_sortComparer);
				}
				return elementsList;
			}

			private void RemoveElement(TModel element)
			{
				if (_gridViewBehaviour.ContainsElement(element))
				{
					_gridViewBehaviour.RemoveElement(element);
				}
			}

			public void DrawAllElements()
			{
				List<TModel> models = GetSortedElements();
				for (int i = 0; i < models.Count; i++)
				{
					TModel model = models[i];
					if (_showCondition == null || _showCondition(model))
					{
						_gridViewBehaviour.AddElementWithoutRecalculate(model, i);
					}
				}
				((IListView)this).RecalculateLayout();
			}

			private void InitItem(object model, Component view)
			{
				_creatingCallback.Invoke((TModel)model, (TView)view);
			}

			void IListView.RecalculateLayout()
			{
				_layoutView.Recalculate();
			}

			void IListView.Dispose()
			{
				Dispose();
			}

			public Enumerator GetEnumerator()
			{
				return new Enumerator(_gridViewBehaviour._viewItemsDictionary);
			}

			public struct Enumerator
			{
				private Dictionary<object, Component>.Enumerator _enumerator;

				public bool MoveNext()
				{
					return _enumerator.MoveNext();
				}

				public Enumerator(Dictionary<object, Component> dictionary) : this()
				{
					_enumerator = dictionary.GetEnumerator();
				}

				public KeyValuePair<TModel, TView> Current
				{
					get
					{
						KeyValuePair<object, Component> current = _enumerator.Current;
						return new KeyValuePair<TModel, TView>((TModel)current.Key, (TView)current.Value);
					}
				}
			}

			void IListView.InitItem(object model, Component view)
			{
				InitItem(model, view);
			}
		}
	}
}
#endif