﻿#if !NOT_UNITY3D
using System;
using Framework.Client.View;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButton : BaseButton, IPointerClickHandler
{
	[SerializeField]
	private string _clickSoundName = "SoundClickButton";

	private Action _action;
	private Action _onPressAction;

	public static Action<string> OnAnyClick;

	public void SetOnClick(Action action)
	{
		_action = action;
	}
	protected override void OnClick()
	{
		if (isActiveAndEnabled)
		{
			PlayClickSound();
			_action?.Invoke();
		}
	}

	private void PlayClickSound()
	{
		OnAnyClick?.Invoke(_clickSoundName);
	}

	void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
	{
		OnClick();
	}
}
#endif