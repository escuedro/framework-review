﻿#if !NOT_UNITY3D

using UnityEngine;
using UnityEngine.UI;

namespace Framework.View
{
	public class ResourceImage : MonoBehaviour
	{
		[SerializeField]
		private Image _image;
		
		private void Reset()
		{
			TryGetComponent(out _image);
		}

		public ResourceImage Load(ResourcePath resourcePath, bool setAlpha = true)
		{
			Sprite image = Resources.Load<Sprite>(resourcePath.Value);
			_image.sprite = image;
			if (setAlpha)
			{
				Color imageColor = _image.color;
				imageColor.a = 1;
				_image.color = imageColor;
			}
			return this;
		}

		public void SetPreferedSize()
		{
			_image.SetNativeSize();
		}
	}
}

#endif