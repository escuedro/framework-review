#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework.QualityGates
{
	public class QualityGateRoot : MonoBehaviour
	{
		public static Action<int> TestQuality;

		[SerializeField]
		private int _countMessages;

#if UNITY_EDITOR
		private void Awake()
		{
			TestQuality?.Invoke(_countMessages);
		}
#endif
	}
}
#endif