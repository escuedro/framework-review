﻿#if !NOT_UNITY3D
using UnityEngine;

namespace Framework.Client.View
{
	public interface IListView
	{
		void InitItem(object model, Component view);
		void RecalculateLayout();
		public void Dispose();
	}
}
#endif