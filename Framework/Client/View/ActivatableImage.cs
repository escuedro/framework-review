﻿#if !NOT_UNITY3D
using UnityEngine;
using UnityEngine.UI;

public class ActivatableImage : MonoBehaviour
{
	[SerializeField]
	private CanvasGroup _canvasGroup;
	[SerializeField]
	private Image _image;

	public void Reset()
	{
		if (!TryGetComponent(out _canvasGroup))
		{
			_canvasGroup = gameObject.AddComponent<CanvasGroup>();
		}
		_image = GetComponent<Image>();
	}

	public void SetActive(bool isActive)
	{
		_image.raycastTarget = isActive;
		_canvasGroup.alpha = isActive ? 1.0f : 0.75f;
	}
}
#endif