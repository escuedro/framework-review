#if !NOT_UNITY3D
using UnityEngine;

namespace Framework.Client.View
{
	public abstract class LayoutView : MonoBehaviour
	{
		public RectTransform Content;
		public abstract void Recalculate();
	}
}
#endif