﻿#if !NOT_UNITY3D

using System;
using Framework.Client.View;
using UnityEngine.EventSystems;

namespace Framework.Client.View
{
	public class LinkButton : BaseButton, IPointerDownHandler
	{
		private string _link;
		private Action _onButtonClicked;

		public void SetOnClick(string linkToOpen, [CanBeNull] Action onButtonClicked)
		{
			_link = linkToOpen;
			_onButtonClicked = onButtonClicked;
		}

		protected override void OnClick()
		{
			OpenWindowUtils.OpenLinkInNewTab(_link);
			_onButtonClicked?.Invoke();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			OnClick();
		}
	}
}

#endif