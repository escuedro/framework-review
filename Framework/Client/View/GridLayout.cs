﻿#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using Framework.Client.View;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Client.View
{
	public enum GridViewAlignmentMode
	{
		TopLeft,
		TopRight
	}

	public class GridLayout : LayoutView
	{
		public event Action<float> OnRecalculated;
		public RectTransform Rect { get; private set; }

		[SerializeField]
		private GridViewAlignmentMode _gridViewAlignmentMode;
		[SerializeField]
		protected RectOffset m_Padding = new RectOffset();
		[SerializeField]
		private int _columns = 1;
		[SerializeField]
		private int _paddingHorizontal;
		[SerializeField]
		private int _paddingVertical;
		[Header("Additional offsets")]
		[SerializeField]
		private Vector2 _additionalOffset;
		
		private Vector2 _cellSize;

		private void Awake()
		{
			Rect = GetComponent<RectTransform>();
		}

#if UNITY_EDITOR
		[MenuItem("CONTEXT/GridLayout/RecalculateStatic")]
		public static void RecalculateStatic(MenuCommand command)
		{
			GridLayout body = (GridLayout)command.context;
			body.Recalculate();
		}
#endif

		public override void Recalculate()
		{
			int childCount = Content.childCount;
			if (childCount == 0)
			{
				return;
			}
			List<RectTransform> children = new List<RectTransform>();
			for (int i = 0; i < childCount; i++)
			{
				children.Add(Content.GetChild(i).GetComponent<RectTransform>());
			}
			_cellSize = new Vector2(children[0].rect.width, children[0].rect.height);

			Vector2 startPosition = GetStartPoint();
			for (int i = 0; i < children.Count; i++)
			{
				int pos = i - i / _columns * _columns;
				Vector2 newPos = CalculateElementXPosition(pos) + startPosition;
				newPos.y += -(i / _columns) * (_cellSize.y + _paddingVertical);
				newPos.x += m_Padding.left - m_Padding.right;
				newPos.y += m_Padding.bottom - m_Padding.top;
				children[i].pivot = new Vector2(0, 1);
				children[i].anchorMin = new Vector2(0, 1);
				children[i].anchorMax = new Vector2(0, 1);
				children[i].localPosition = newPos;
			}
			int rows = (childCount - 1) / _columns;
			float contentSizeY = (rows + 1) * _cellSize.y + rows * _paddingVertical;
			Content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contentSizeY + _additionalOffset.y);
			Content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (_columns * _cellSize.x + (_columns - 1) * _paddingHorizontal) + _additionalOffset.x);
			LayoutRebuilder.ForceRebuildLayoutImmediate(Rect);
			OnRecalculated?.Invoke(contentSizeY);
		}

		private Vector2 CalculateElementXPosition(int elementRowPosition)
		{
			Vector2 position;
			switch (_gridViewAlignmentMode)
			{
				case GridViewAlignmentMode.TopLeft:
					position = new Vector2(elementRowPosition * (_paddingHorizontal + _cellSize.x), 0);
					break;
				case GridViewAlignmentMode.TopRight:
					position = new Vector2((elementRowPosition) * -(_paddingHorizontal + _cellSize.x) - _cellSize.x, 0);
					break;
				default: throw new NotImplementedException("Unknown grid state");
			}
			return position;
		}

		private Vector2 GetStartPoint()
		{
			Rect rect = Content.rect;
			switch (_gridViewAlignmentMode)
			{
				case GridViewAlignmentMode.TopLeft:
					return new Vector2(rect.position.x, rect.position.y + rect.height);
				case GridViewAlignmentMode.TopRight:
					return new Vector2(rect.position.x + rect.width, rect.position.y + rect.height);
				default: throw new NotImplementedException("Unknown grid state");
			}
		}
	}
}
#endif