﻿#if !NOT_UNITY3D

using System.Collections;
using Framework.Futures;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Client.View
{
	public abstract class BaseButton : MonoBehaviour, IFutureHandler
	{
		public void HandleFuture(IFuture future)
		{
			enabled = true;
		}
		
		public void EnableAfter(IEnumerator work)
		{
			enabled = false;
			StartCoroutine(WaitForEnable(work));
		}

		public void EnableAfter(IFuture future)
		{
			enabled = false;
			future.Finally(this);
		}
		
		private IEnumerator WaitForEnable(IEnumerator work)
		{
			yield return work;
			enabled = true;
		}
		
		private void Reset()
		{
			EventTrigger oldTrigger = GetComponent<EventTrigger>();
			if (oldTrigger != null)
			{
				DestroyImmediate(oldTrigger);
			}
		}

		public void Click()
		{
			OnClick();
		}

		protected abstract void OnClick();
	}
}

#endif