﻿#if !NOT_UNITY3D

using UnityEngine;

namespace Framework.Client.View
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class ResourceSprite : MonoBehaviour
	{
		[SerializeField]
		private SpriteRenderer _sprite;

		private void Reset()
		{
			TryGetComponent(out _sprite);
		}

		public ResourceSprite Load(ResourcePath resourcePath, bool setAlpha = true)
		{
			Sprite image = Resources.Load<Sprite>(resourcePath.Value);
			_sprite.sprite = image;
			if (setAlpha)
			{
				Color imageColor = _sprite.color;
				imageColor.a = 1;
				_sprite.color = imageColor;
			}
			return this;
		}
	}
}

#endif