﻿#if !NOT_UNITY3D

using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI
{
	public class TooltipRegistrationArea : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private Action<TooltipData, Vector2> _onTooltipReady;
		private Action<TooltipData, Vector2> _onTooltipCancelled;
		private TooltipData _tooltipData;

		private float _timerDuration;
		private float _timerValue;

		private Vector2 _lastCursorPosition;
		
		private bool _needListenCursor;
		private bool _cursorInPosition;

		private const float CursorDistancePrecision = 3f;

		public void Initialize(
				TooltipData tooltipData,
				Action<TooltipData, Vector2> onTooltipReady,
				Action<TooltipData, Vector2> onTooltipCancelled,
				float timerDuration)
		{
			_tooltipData = tooltipData;
			_onTooltipReady = onTooltipReady;
			_onTooltipCancelled = onTooltipCancelled;
			_timerDuration = timerDuration;
		}

		private void Update()
		{
			if (_needListenCursor)
			{
				Vector2 currentMousePosition = Input.mousePosition;
				if (!_cursorInPosition && Vector2.Distance(currentMousePosition, _lastCursorPosition) < CursorDistancePrecision)
				{
					_timerValue -= Time.deltaTime;
					if (_timerValue <= 0)
					{
						_onTooltipReady?.Invoke(_tooltipData, currentMousePosition);
						_cursorInPosition = true;
					}
				}
				else if (_cursorInPosition && Vector2.Distance(currentMousePosition, _lastCursorPosition) > CursorDistancePrecision)
				{
					_timerValue = _timerDuration;
					_cursorInPosition = false;
					_onTooltipCancelled?.Invoke(_tooltipData, currentMousePosition);
				}
				else
				{
					_timerValue = _timerDuration;
				}
				_lastCursorPosition = currentMousePosition;
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			_lastCursorPosition = Input.mousePosition;
			_timerValue = _timerDuration;
			_needListenCursor = true;
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			_timerValue = _timerDuration;
			_needListenCursor = false;
			_onTooltipCancelled?.Invoke(_tooltipData, eventData.position);
		}

		private void OnDisable()
		{
			_onTooltipCancelled?.Invoke(_tooltipData, Input.mousePosition);
		}

		private void OnDestroy()
		{
			_onTooltipCancelled?.Invoke(_tooltipData, Input.mousePosition);
		}
	}
}

#endif