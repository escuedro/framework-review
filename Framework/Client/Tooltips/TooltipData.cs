﻿#if !NOT_UNITY3D

using Framework.Client.Translations;

namespace Framework.UI
{
	public class TooltipData
	{
		[CanBeNull]
		public readonly string Text;
		public readonly TranslationKey TranslationKey;
		[CanBeNull]
		public readonly object[] CustomArgs;

		public TooltipData(TranslationKey translationKey, params object[] customArgs)
		{
			TranslationKey = translationKey;
			CustomArgs = customArgs;
		}

		public TooltipData(string text)
		{
			Text = text;
		}
	}
}

#endif