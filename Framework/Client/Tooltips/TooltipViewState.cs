﻿#if !NOT_UNITY3D

namespace Framework.UI
{
	public enum TooltipViewState
	{
		Closed,
		Closing,
		Opened,
		Opening
	}
}

#endif