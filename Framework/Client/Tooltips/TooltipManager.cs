﻿#if !NOT_UNITY3D

using System;
using Framework.Client.Translations;
using UnityEngine;

namespace Framework.UI
{
	[Bind(BindTo = typeof(ITooltipManager))]
	internal class TooltipManager : ITooltipManager
	{
		private TooltipView _tooltipView;

		private TooltipData _lastToooltipData;

		[Inject]
		public void Inject(IPrefabProvider prefabProvider)
		{
			TooltipManagerRoot root = prefabProvider.Instantiate<TooltipManagerRoot>(false);
			_tooltipView = prefabProvider.InstantiateAt<TooltipView>(root.transform, false);
		}

		public void BindTooltip(GameObject tooltipSource, TranslationKey translationKey, params object[] customArgs)
		{
			TooltipData tooltipData = new TooltipData(translationKey, customArgs);
			RegisterTooltip(tooltipSource, tooltipData);
		}

		public void BindTooltip(GameObject tooltipSource, TranslationKey translationKey)
		{
			TooltipData tooltipData = new TooltipData(translationKey, null);
			RegisterTooltip(tooltipSource, tooltipData);
		}

		public void BindTooltip(GameObject tooltipSource, string text)
		{
			TooltipData tooltipData = new TooltipData(text);
			RegisterTooltip(tooltipSource, tooltipData);
		}

		private void RegisterTooltip(GameObject tooltipSource, TooltipData tooltipData)
		{
			if (tooltipSource.GetComponent<RectTransform>())
			{
				TooltipRegistrationArea registrationArea = tooltipSource.AddComponent<TooltipRegistrationArea>();
				registrationArea.Initialize(tooltipData, OnTooltipReady, OnTooltipCancelled, 0.25f);
			}
			else
			{
				throw new Exception($"There is no Rect Transform component on gameObject [{tooltipSource.name}]");
			}
		}

		private void OnTooltipReady(TooltipData tooltipData, Vector2 position)
		{
			_lastToooltipData = tooltipData;
			_tooltipView.LoadData(tooltipData);
			CalculateTooltipPosition(position);
			_tooltipView.RequestShow();
		}

		private void CalculateTooltipPosition(Vector2 position)
		{
			Rect tooltipRect = _tooltipView.Rect;
			Vector2 offset = GetOffsetByPosition(position, tooltipRect,
					new Vector2(tooltipRect.width * 0.5f, -tooltipRect.height * 0.5f));
			_tooltipView.transform.position = position + offset;
		}

		private Vector2 GetOffsetByPosition(Vector2 position, Rect rect, Vector2 defaultOffset)
		{
			Vector2 screenSize = new Vector2(Screen.width, Screen.height);
			Vector2 rectHalfSize = new Vector2(rect.width * 0.5f, rect.height * 0.5f);
			Vector2 offset = Vector2.zero;
			if (position.x + rect.width > screenSize.x) // off the right border
			{
				offset.x -= rectHalfSize.x;
			}
			if (position.x - rect.width < 0) // off the left border
			{
				offset.x += rectHalfSize.x;
			}
			if (position.y + rect.height > screenSize.y) //off the up border
			{
				offset.y -= rectHalfSize.y;
			}
			if (position.y - rect.height < 0) //off the down border
			{
				offset.y += rectHalfSize.y;
			}
			if (Mathf.Abs(offset.x) < Mathf.Epsilon)
			{
				offset.x += defaultOffset.x;
			}
			if (Mathf.Abs(offset.y) < Mathf.Epsilon)
			{
				offset.y += defaultOffset.y;
			}
			return offset;
		}

		private void OnTooltipCancelled(TooltipData tooltipData, Vector2 position)
		{
			if (_lastToooltipData != tooltipData)
			{
				return;
			}
			_tooltipView.RequestHide();
			_lastToooltipData = null;
		}
	}
}

#endif