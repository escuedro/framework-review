﻿#if !NOT_UNITY3D

using Framework.Client.Translations;
using UnityEngine;

namespace Framework.UI
{
	public interface ITooltipManager
	{
		public void BindTooltip(GameObject tooltipSource, TranslationKey translationKey, params object[] customArgs);
		public void BindTooltip(GameObject tooltipSource, TranslationKey translationKey);
		public void BindTooltip(GameObject tooltipSource, string text);
	}
}

#endif