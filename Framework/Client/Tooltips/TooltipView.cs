﻿#if !NOT_UNITY3D

using Framework.Client.Translations;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.UI
{
	public class TooltipView : MonoBehaviour
	{
		[SerializeField]
		private CanvasGroup _canvasGroup;
		[SerializeField]
		private TranslatedText _translatedText;
		[SerializeField]
		private ContentSizeFitter _rootSizeFilter;
		[SerializeField]
		private ContentSizeFitter _textElementSizeFilter;
		[SerializeField]
		private VerticalLayoutGroup _layoutGroup;

		private IWindowAnimation _animation;
		private RectTransform _rectTransform;
		private bool _hideRequested;
		private bool _showRequested;

		public Rect Rect => _rectTransform.rect;

		private TooltipViewState _tooltipState;

		private void Awake()
		{
			_animation = new FadeInOutAnimation(this, _canvasGroup, 0.1f);
			_canvasGroup.alpha = 0;
			_rectTransform = GetComponent<RectTransform>();
		}

		public void RequestShow()
		{
			_showRequested = true;
			_hideRequested = false;
		}

		private void Show()
		{
			_tooltipState = TooltipViewState.Opening;
			_animation.PlayIn().Then(() => _tooltipState = TooltipViewState.Opened, Log.Exception);
		}

		public void RequestHide()
		{
			_hideRequested = true;
			_showRequested = false;
		}

		private void Hide()
		{
			_tooltipState = TooltipViewState.Closing;
			_animation.PlayOut().Then(() => _tooltipState = TooltipViewState.Closed, Log.Exception);
		}

		private void FixedUpdate()
		{
			if (_hideRequested && _tooltipState == TooltipViewState.Opened)
			{
				_hideRequested = false;
				Hide();
			}
			if (_showRequested && _tooltipState == TooltipViewState.Closed)
			{
				_showRequested = false;
				Show();
			}
		}

		public void LoadData(TooltipData tooltipData)
		{
			if (tooltipData.Text != null)
			{
				_translatedText.SetRaw(tooltipData.Text);
			}
			else
			{
				_translatedText.SetTranslationKey(tooltipData.TranslationKey);
				if (tooltipData.CustomArgs != null)
				{
					_translatedText.InputFormattedData(tooltipData.CustomArgs);
				}
			}
			RecalculateSize();
		}

		private void RecalculateSize()
		{
			_textElementSizeFilter.SetLayoutVertical();
			_layoutGroup.CalculateLayoutInputVertical();
			_rootSizeFilter.SetLayoutVertical();
		}
	}
}

#endif