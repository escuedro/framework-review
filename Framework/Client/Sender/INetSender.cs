#if !NOT_UNITY3D
using Framework.Futures;

namespace Framework.Sender
{
	public interface INetSender
	{
		IFuture<byte[]> PostRequest(string method, byte[] requestData);
	}
}
#endif