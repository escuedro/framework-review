#if !NOT_UNITY3D
using Framework.Futures;

namespace Framework.Sender
{
	[Bind(BindTo = typeof(INetSender))]
	internal class NetSender : INetSender
	{
		public IFuture<byte[]> PostRequest(string method, byte[] requestData)
		{
			throw new System.NotImplementedException();
		}
	}
}
#endif