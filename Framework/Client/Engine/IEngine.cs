#if !NOT_UNITY3D
using System.Collections;
using UnityEngine;

namespace Framework
{
	public interface IEngine
	{
		Coroutine StartCoroutine(IEnumerator coroutine);
		void StopCoroutine(Coroutine coroutine);
	}
}
#endif