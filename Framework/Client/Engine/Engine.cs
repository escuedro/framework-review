#if !NOT_UNITY3D
using System.Collections;
using UnityEngine;

namespace Framework.Internal
{
	[Bind(BindTo = typeof(IEngine))]
	internal class Engine : IEngine
	{
		private readonly EngineComponent _engineComponent;

		public Engine()
		{
			GameObject engine = new GameObject("Engine", typeof(EngineComponent));
			_engineComponent = engine.GetComponent<EngineComponent>();
			Object.DontDestroyOnLoad(engine);
		}

		public Coroutine StartCoroutine(IEnumerator coroutine)
		{
			return _engineComponent.StartCoroutine(coroutine);
		}

		public void StopCoroutine(Coroutine coroutine)
		{
			_engineComponent.StopCoroutine(coroutine);
		}
	}
}
#endif