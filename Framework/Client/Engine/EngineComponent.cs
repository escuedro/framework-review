#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework.Internal
{
	internal class EngineComponent : MonoBehaviour
	{
		public Action OnUpdate;
		public Action OnFixedUpdate;
		public Action OnLateUpdate;
		public Action OnSecondUpdate;

		private float _secondTimer = 1.0f;

		private void Update()
		{
			OnUpdate?.Invoke();
			_secondTimer -= Time.deltaTime;
			if (_secondTimer <= 0)
			{
				OnSecondUpdate?.Invoke();
				_secondTimer += 1.0f;
			}
		}

		private void FixedUpdate()
		{
			OnFixedUpdate?.Invoke();
		}

		private void LateUpdate()
		{
			OnLateUpdate?.Invoke();
		}
	}
}
#endif