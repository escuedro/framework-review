#if !NOT_UNITY3D
namespace Framework.Client
{
	public interface IUpdatable
	{
		void Update();
	}
}
#endif