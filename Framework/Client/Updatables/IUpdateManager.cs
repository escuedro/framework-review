#if !NOT_UNITY3D
namespace Framework.Client
{
	public interface IUpdateManager
	{
		void StartUpdate(IUpdatable updatable, UpdateType updateType);
		void StopUpdate(IUpdatable updatable, UpdateType updateType);
	}
}
#endif