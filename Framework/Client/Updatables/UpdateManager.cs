#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using Framework.Internal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.Client
{
	[Bind(BindTo = typeof(IUpdateManager))]
	internal class UpdateManager : IUpdateManager
	{
		private readonly EngineComponent _engineComponent;
		private readonly List<IUpdatable> _itearateList = new List<IUpdatable>();

		private Dictionary<UpdateType, List<IUpdatable>> _updatables = new Dictionary<UpdateType, List<IUpdatable>>();

		public UpdateManager()
		{
			GameObject engine = new GameObject("UpdateManager", typeof(EngineComponent));
			_engineComponent = engine.GetComponent<EngineComponent>();
			Object.DontDestroyOnLoad(engine);
			_engineComponent.OnUpdate = Update;
			_engineComponent.OnFixedUpdate = FixedUpdate;
			_engineComponent.OnLateUpdate = LateUpdate;
			_engineComponent.OnSecondUpdate = SecondUpdate;
		}

		private void Update()
		{
			if (_updatables.TryGetValue(UpdateType.ByUpdate, out List<IUpdatable> updatables))
			{
				IterateUpdate(updatables);
			}
		}

		private void FixedUpdate()
		{
			if (_updatables.TryGetValue(UpdateType.ByFixedUpdate, out List<IUpdatable> updatables))
			{
				IterateUpdate(updatables);
			}
		}

		private void LateUpdate()
		{
			if (_updatables.TryGetValue(UpdateType.ByLateUpdate, out List<IUpdatable> updatables))
			{
				IterateUpdate(updatables);
			}
		}

		private void SecondUpdate()
		{
			if (_updatables.TryGetValue(UpdateType.BySecond, out List<IUpdatable> updatables))
			{
				IterateUpdate(updatables);
			}
		}

		private void IterateUpdate(List<IUpdatable> updatables)
		{
			_itearateList.Clear();
			_itearateList.AddRange(updatables);
			foreach (IUpdatable updatable in _itearateList)
			{
				try
				{
					updatable.Update();
				}
				catch (Exception exception)
				{
					Log.Exception(exception);
				}
			}
		}

		public void StartUpdate(IUpdatable updatable, UpdateType updateType)
		{
			if (!_updatables.ContainsKey(updateType))
			{
				List<IUpdatable> updatablesList = new List<IUpdatable>();
				updatablesList.Add(updatable);
				_updatables.Add(updateType, updatablesList);
			}
			else
			{
				List<IUpdatable> updatablesList = _updatables[updateType];
				if (updatablesList.Contains(updatable))
				{
					throw new Exception($"Updatable {updatable} already exists in updatable list");
				}
				updatablesList.Add(updatable);
			}
		}

		public void StopUpdate(IUpdatable updatable, UpdateType updateType)
		{
			List<IUpdatable> updatablesList = _updatables[updateType];
			if (!updatablesList.Contains(updatable))
			{
				throw new Exception($"Updatable {updatable} doesn't exists in updatable list");
			}
			updatablesList.Remove(updatable);
		}
	}
}
#endif