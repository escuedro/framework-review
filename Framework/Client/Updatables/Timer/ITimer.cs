﻿#if !NOT_UNITY3D

using System;
using Framework.Reactive;
using SAS.Model.Time;

namespace Framework.Client.Timer
{
	public enum TimerStatus : byte
	{
		Stopped,
		Running,
		Ready
	}
	public interface ITimer
	{
		public Field<TimerStatus> Status { get; }
		public Field<int> SecondsLeft { get; }
		
		public void Start();

		public void Stop();

		public void SetSecondsLeft(TimeDuration duration);

		public void SetOnReset(Action onReset);
	}
}

#endif