﻿using System;
using Framework.Reactive;
using SAS.Model.Time;

#if !NOT_UNITY3D

namespace Framework.Client.Timer
{
	public class Timer : ITimer, IUpdatable
	{
		public Field<TimerStatus> Status { get; } = new Field<TimerStatus>();
		public Field<int> SecondsLeft { get; } = new Field<int>();
		
		private Action _onResetAction;
		
		private readonly TimeDuration _period;
		private readonly IUpdateManager _updateManager;
		private readonly bool _isCyclic;

		public Timer(IUpdateManager updateManager, TimeDuration timeDuration, bool isCyclic)
		{
			_updateManager = updateManager;
			_period = timeDuration;
			_isCyclic = isCyclic;
			SetSecondsLeft(timeDuration);
		}

		public void Start()
		{
			if (Status.Value != TimerStatus.Running)
			{
				_updateManager.StartUpdate(this, UpdateType.BySecond);
				Status.Value = TimerStatus.Running;
			}
		}

		public void Stop()
		{
			if (Status.Value != TimerStatus.Stopped)
			{
				_updateManager.StopUpdate(this, UpdateType.BySecond);
				Status.Value = TimerStatus.Stopped;
			}
		}

		public void SetSecondsLeft(TimeDuration duration)
		{
			SecondsLeft.Value = (int)duration.Seconds;
		}

		public void SetOnReset(Action onReset)
		{
			_onResetAction = onReset;
		}

		public void Update()
		{
			if (SecondsLeft.Value > 0)
			{
				SecondsLeft.Value--;
			}
			if (SecondsLeft.Value <= 0)
			{
				_onResetAction?.Invoke();
				if (_isCyclic)
				{
					SecondsLeft.Value = (int)_period.Seconds;
				}
				else
				{
					Status.Value = TimerStatus.Ready;
					Stop();
				}
			}
		}
	}
}

#endif