﻿namespace Framework.Client
{
	public enum UpdateType
	{
		ByUpdate,
		ByFixedUpdate,
		ByLateUpdate,
		BySecond
	}
}