#if !NOT_UNITY3D
namespace Framework
{
	public struct ResourcePath
	{
		public readonly string Value;

		public ResourcePath(string value)
		{
			Value = value;
		}

		public static implicit operator ResourcePath(string value)
		{
			return new ResourcePath(value);
		}

		public override string ToString()
		{
			return Value;
		}
	}
}
#endif