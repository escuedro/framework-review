#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework
{
	public interface IResourceBinder
	{
		void Bind<T>(ResourcePath resourcePath, object id = null) where T : UnityEngine.Object;
		ResourcePath GetPath(Type componentType, [CanBeNull] object id);
	}
}
#endif