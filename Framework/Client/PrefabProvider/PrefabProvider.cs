﻿#if !NOT_UNITY3D

using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework
{
	[Bind(BindTo = typeof(IPrefabProvider))]
	internal class PrefabProvider : IPrefabProvider
	{
		[Inject]
		public IResourceBinder ResourceBinder;
		[Inject]
		public IInjector Injector;

		public T Instantiate<T>(bool needToInject, object id = null) where T : Component
		{
			T component = GetComponent<T>(id);
			T instantiatedComponent = Object.Instantiate(component);
			if (needToInject)
			{
				Injector.Inject(instantiatedComponent);
			}
			return instantiatedComponent;
		}

		public Component Instantiate(Type componentType, bool needToInject, object id = null)
		{
			Component component = GetComponent(componentType, id);
			Component instantiatedComponent = Object.Instantiate(component);
			if (needToInject)
			{
				Injector.Inject(instantiatedComponent);
			}
			return instantiatedComponent;
		}

		public T InstantiateAt<T>(Transform parent, bool needToInject, object id = null) where T : Component
		{
			T component = GetComponent<T>(id);
			T instantiatedComponent = Object.Instantiate(component, parent);
			if (needToInject)
			{
				Injector.Inject(instantiatedComponent);
			}
			return instantiatedComponent;
		}

		public Component InstantiateAt(Type componentType, Transform parent, bool needToInject, object id = null)
		{
			Component component = GetComponent(componentType, id);
			Component instantiatedComponent = Object.Instantiate(component, parent);
			if (needToInject)
			{
				Injector.Inject(instantiatedComponent);
			}
			return instantiatedComponent;
		}

		public GameObject Instantiate(ResourcePath resourcePath)
		{
			GameObject instantiatedObject = Object.Instantiate(Resources.Load<GameObject>(resourcePath.Value));
			return instantiatedObject;
		}
		
		public GameObject InstantiateAt(ResourcePath resourcePath, Transform parent)
		{
			GameObject instantiatedObject = Object.Instantiate(Resources.Load<GameObject>(resourcePath.Value), parent);
			return instantiatedObject;
		}

		private T GetComponent<T>(object id = null) where T : Component
		{
			ResourcePath componentResourcePath = ResourceBinder.GetPath(typeof(T), id);
			return Resources.Load<T>(componentResourcePath.Value);
		}

		private Component GetComponent(Type componentType, object id = null)
		{
			ResourcePath componentResourcePath = ResourceBinder.GetPath(componentType, id);
			return Resources.Load<Component>(componentResourcePath.Value).GetComponent(componentType);
		}
	}
}

#endif