#if !NOT_UNITY3D
using System;
using UnityEngine;

namespace Framework
{
	public interface IPrefabProvider
	{
		T Instantiate<T>(bool needToInject, object id = null) where T : Component;
		Component Instantiate(Type componentType, bool needToInject, object id = null);
		T InstantiateAt<T>(Transform parent, bool needToInject, object id = null) where T : Component;
		Component InstantiateAt(Type componentType, Transform parent, bool needToInject, object id = null);
		GameObject Instantiate(ResourcePath resourcePath);
		GameObject InstantiateAt(ResourcePath resourcePath, Transform parent);
	}
}
#endif