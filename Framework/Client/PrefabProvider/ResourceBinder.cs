﻿#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace Framework
{
	[Bind(BindTo = typeof(IResourceBinder))]
	internal class ResourceBinder : IResourceBinder
	{
		private Dictionary<Type, ResourcePath> _typeResourceDictionary = new Dictionary<Type, ResourcePath>();
		private Dictionary<Type, Dictionary<object, ResourcePath>> _idResources =
				new Dictionary<Type, Dictionary<object, ResourcePath>>();


		public void Bind<T>(ResourcePath resourcePath, object id = null) where T : Object
		{
			TryBind(typeof(T), resourcePath, id);
		}

		public ResourcePath GetPath(Type componentType, object id = null)
		{
			ResourcePath path;
			if (id == null)
			{
				if (!_typeResourceDictionary.ContainsKey(componentType))
				{
					throw new ArgumentException($"Resources binder has no component with type {componentType.Name}");
				}
				path = _typeResourceDictionary[componentType];
			}
			else
			{
				if (!_idResources.ContainsKey(componentType))
				{
					throw new ArgumentException($"Resources binder has no component with type {componentType.Name}");
				}
				if (!_idResources[componentType].ContainsKey(id))
				{
					throw new ArgumentException($"Resources binder has no component with id {id}");
				}
				path = _idResources[componentType][id];
			}
			return path;
		}

		private void TryBind(Type type, ResourcePath resourcePath, object id = null)
		{
			if (id == null)
			{
				_typeResourceDictionary.Add(type, resourcePath);
			}
			else
			{
				if (!_idResources.ContainsKey(type))
				{
					_idResources.Add(type, new Dictionary<object, ResourcePath>());
				}
				Dictionary<object, ResourcePath> idDictionary = _idResources[type];
				if (!idDictionary.ContainsKey(id))
				{
					idDictionary.Add(id, null);
				}
				idDictionary[id] = resourcePath;
			}
		}
	}
}
#endif