﻿#if !NOT_UNITY3D

namespace Framework.Audio
{
	public interface IAudioManager
	{
		public void PlaySound(object id);
		public void SetVolume(float normalizedVolume);
		public void ChangeBackgroundSound(object id, bool fromStart = true);
	}
}

#endif