﻿#if !NOT_UNITY3D

using System.Collections;
using UnityEngine;

namespace Framework.Audio
{
	public class AudioManagerRoot : MonoBehaviour
	{
		[SerializeField]
		private int _channelsCount = 30;

		private AudioSource[] _channels;
		private AudioSource _backgroundMusicChannel;
		private float _globalVolume;

		public void Init()
		{
			DontDestroyOnLoad(gameObject);
			CreateChannels(gameObject);
		}

		private void CreateChannels(GameObject target)
		{
			_channels = new AudioSource[_channelsCount];
			for (int i = 0; i < _channelsCount; i++)
			{
				AudioSource audioSourceComponent = target.AddComponent<AudioSource>();
				audioSourceComponent.playOnAwake = false;
				_channels[i] = audioSourceComponent;
			}
			_backgroundMusicChannel = target.AddComponent<AudioSource>();
			_backgroundMusicChannel.playOnAwake = false;
		}

		public void PlaySound(SoundRegistration soundRegistration)
		{
			AudioSource freeChannel = GetFreeChannel();
			if (freeChannel != null)
			{
				freeChannel.clip = soundRegistration.AudioClip;
				freeChannel.volume = _globalVolume;
				freeChannel.loop = soundRegistration.Loop;
				freeChannel.Play();
			}
		}

		public void PlayBackgroundMusic(SoundRegistration soundRegistration, bool fromStart)
		{
			float time = fromStart? 0.0f : _backgroundMusicChannel.time;
			StartCoroutine(ChangeMusic(soundRegistration, time));
		}

		private IEnumerator ChangeMusic(SoundRegistration soundRegistration, float time)
		{
			float animationDuration = 0.1f;
			float timer = 0f;
			float currentVolume = _globalVolume;
			while (timer < animationDuration)
			{
				timer += Time.deltaTime;
				_backgroundMusicChannel.volume = Mathf.Lerp(currentVolume, 0f, timer / animationDuration);
				yield return null;
			}
			_backgroundMusicChannel.clip = soundRegistration.AudioClip;
			_backgroundMusicChannel.loop = soundRegistration.Loop;
			_backgroundMusicChannel.Play();
			_backgroundMusicChannel.time = time;
			timer = 0f;
			while (timer < animationDuration)
			{
				timer += Time.deltaTime;
				_backgroundMusicChannel.volume = Mathf.Lerp(0f, currentVolume, timer / animationDuration);
				yield return null;
			}
		}

		private AudioSource GetFreeChannel()
		{
			foreach (AudioSource channel in _channels)
			{
				if (!channel.isPlaying)
				{
					return channel;
				}
			}
			return null;
		}

		public void SetVolume(float normalizedVolume)
		{
			_globalVolume = normalizedVolume;
			foreach (AudioSource channel in _channels)
			{
				channel.volume = normalizedVolume;
			}
			_backgroundMusicChannel.volume = normalizedVolume;
		}
	}
}

#endif