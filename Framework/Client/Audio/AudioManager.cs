﻿#if !NOT_UNITY3D

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Audio
{
	[Bind(BindTo = typeof(IAudioManager))]
	public class AudioManager : IAudioManager
	{
		private AudioManagerRoot _audioManagerRoot;
		private IResourceBinder _resourceBinder;

		private Dictionary<object, SoundRegistration> _sounds = new Dictionary<object, SoundRegistration>();

		[Inject]
		public AudioManager(IResourceBinder resourceBinder, IPrefabProvider prefabProvider)
		{
			_resourceBinder = resourceBinder;
			_audioManagerRoot = prefabProvider.Instantiate<AudioManagerRoot>(false);
			_audioManagerRoot.Init();
		}

		public void PlaySound(object id)
		{
			_audioManagerRoot.PlaySound(GetSound(id));
		}

		public void SetVolume(float normalizedVolume)
		{
			_audioManagerRoot.SetVolume(normalizedVolume);
		}

		private SoundRegistration GetSound(object id)
		{
			if (!_sounds.TryGetValue(id, out SoundRegistration value))
			{
				SoundRegistration soundRegistration =
						Resources.Load<SoundRegistration>(_resourceBinder.GetPath(typeof(SoundRegistration), id).Value);
				if (soundRegistration == null)
				{
					throw new Exception($"Resource binder has no sound with id [{id}]");
				}
				_sounds.Add(id, soundRegistration);
				return soundRegistration;
			}
			return value;
		}

		public void ChangeBackgroundSound(object id, bool fromStart)
		{
			_audioManagerRoot.PlayBackgroundMusic(GetSound(id), fromStart);
		}
	}
}

#endif