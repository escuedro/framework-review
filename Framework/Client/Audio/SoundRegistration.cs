﻿#if !NOT_UNITY3D

using UnityEngine;

namespace Framework.Audio
{
	[CreateAssetMenu(fileName = "Sound", menuName = "Data/Sound", order = 0)]
	public class SoundRegistration : ScriptableObject
	{
		public AudioClip AudioClip;
		public bool Loop;
		[Range(0f, 1f)]
		public float Volume;
	}
}

#endif