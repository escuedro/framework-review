﻿#if !NOT_UNITY3D

using System;
using System.Collections.Generic;
using Framework.Futures;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.UI
{
	[Bind(BindTo = typeof(IWindowManager))]
	internal class WindowManager : IWindowManager, IFutureHandler
	{
		private readonly IPrefabProvider _prefabProvider;

		private readonly Dictionary<Type, WindowParametersPair> _activeWindows = new Dictionary<Type, WindowParametersPair>();
		private readonly List<IWindow> _windowsHierarchy = new List<IWindow>();

		private readonly WindowManagerRoot _windowsParentCanvas;

		private readonly List<IWindow> _closingWindows = new List<IWindow>();
		private readonly List<IWindow> _openingWindows = new List<IWindow>();
		private readonly Dictionary<IFuture, IWindow> _futureWindowDictionary = new Dictionary<IFuture, IWindow>();

		[Inject]
		public WindowManager(IPrefabProvider prefabProvider)
		{
			_prefabProvider = prefabProvider;
			_windowsParentCanvas = _prefabProvider.Instantiate<WindowManagerRoot>(false);
			_windowsParentCanvas.SetOverlayActive(false);
			_windowsParentCanvas.Init(OnOverlayClick);
		}

		private void OnOverlayClick()
		{
			Close(_windowsHierarchy[_windowsHierarchy.Count - 1]);
		}

		public void Open<TWindow>(params object[] arguments) where TWindow : IWindow
		{
			Open(typeof(TWindow), arguments);
		}

		public void Open(Type windowType, params object[] arguments)
		{
			_windowsParentCanvas.SetOverlayActive(true);
			WindowParametersPair activeWindowWithParameters;
			if (!_activeWindows.ContainsKey(windowType))
			{
				IWindow window = InstantiateWindow(windowType);
				activeWindowWithParameters = new WindowParametersPair(window, arguments);
				_activeWindows.Add(windowType, activeWindowWithParameters);
				_windowsHierarchy.Add(window);
				_windowsParentCanvas.MoveOverlayUnderLastWindow();
			}
			else
			{
				activeWindowWithParameters = _activeWindows[windowType];
				if (_closingWindows.Contains(activeWindowWithParameters.Window))
				{
					return;
				}
				if (activeWindowWithParameters.EqualsArguments(arguments))
				{
					Log.Info($"Window with type {windowType.Name} already opened with same data");
					activeWindowWithParameters.Window.Transform.SetAsLastSibling();
					_windowsHierarchy.Remove(activeWindowWithParameters.Window);
					_windowsHierarchy.Add(activeWindowWithParameters.Window);
					_windowsParentCanvas.MoveOverlayUnderLastWindow();
					return;
				}
			}
			IWindow openingWindow = activeWindowWithParameters.Window;
			openingWindow.Transform.SetAsLastSibling();
			try
			{
				IFuture future = openingWindow.Open(arguments);
				_futureWindowDictionary.Add(future, openingWindow);
				if (future.IsRejected)
				{
					throw new Exception("Opened window is in an invalid state");
				}
				if (!future.IsResolved)
				{
					_openingWindows.Add(openingWindow);
					future.Finally(this);
				}
			}
			catch (Exception e)
			{
				Close(openingWindow, true);
				Log.Error(e.Message);
			}
		}

		private IWindow InstantiateWindow(Type windowType)
		{
			Component instantiatedWindow =
					_prefabProvider.InstantiateAt(windowType, _windowsParentCanvas.transform, true);
			return (IWindow)instantiatedWindow;
		}

		public void Close<TWindow>() where TWindow : IWindow
		{
			IWindow windowToClose = _activeWindows[typeof(TWindow)].Window;
			Close(windowToClose);
		}

		public void Close(IWindow windowToClose)
		{
			Close(windowToClose, false);
		}

		private void Close(IWindow windowToClose, bool emergencyClosing)
		{
			if (!emergencyClosing && _closingWindows.Contains(windowToClose))
			{
				return;
			}
			if (_openingWindows.Contains(windowToClose))
			{
				return;
			}
			if (!emergencyClosing)
			{
				try
				{
					IFuture future = windowToClose.OnClose();
					_futureWindowDictionary.Add(future, windowToClose);
					if (!future.IsResolved)
					{
						_closingWindows.Add(windowToClose);
						future.Finally(this);
					}
					else
					{
						DeleteWindow(windowToClose);
					}
				}
				catch (Exception e)
				{
					Log.Exception(e);
				}
			}
			else
			{
				DeleteWindow(windowToClose);
			}
		}

		private void DeleteWindow(IWindow windowToClose)
		{
			int windowHierarchyPosition = _windowsHierarchy.IndexOf(windowToClose);
			Type windowType = windowToClose.GetType();
			Object.Destroy(windowToClose.Transform.gameObject);
			_activeWindows.Remove(windowType);
			_windowsHierarchy.Remove(windowToClose);
			if (_windowsHierarchy.Count == 0)
			{
				_windowsParentCanvas.SetOverlayActive(false);
			}
			else
			{
				_windowsParentCanvas.SetOverlayHierarchyPosition(windowHierarchyPosition - 1);
			}
		}

		public void CloseAllWindows()
		{
			foreach (WindowParametersPair windowParametersPair in _activeWindows.Values)
			{
				Close(windowParametersPair.Window);
			}
		}

		private bool IsWindowOpened<TWindow>() where TWindow : IWindow
		{
			return _activeWindows.ContainsKey(typeof(TWindow));
		}

		public void HandleFuture(IFuture future)
		{
			IWindow window = _futureWindowDictionary[future];
			if (future.IsRejected && future.Reason != null)
			{
				Log.Exception(future.Reason);
				DeleteWindow(window);
			}
			else
			{
				if (_closingWindows.Contains(window))
				{
					DeleteWindow(window);
					_closingWindows.Remove(window);
				}
				if (_openingWindows.Contains(window))
				{
					_openingWindows.Remove(window);
				}
			}
			_futureWindowDictionary.Remove(future);
			FuturePool.Release(future);
		}
	}
}

#endif