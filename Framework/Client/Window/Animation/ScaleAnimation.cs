﻿#if !NOT_UNITY3D
using System.Collections;
using Framework.Futures;
using UnityEngine;

namespace Framework.UI
{
	public class ScaleAnimation : IWindowAnimation
	{
		private readonly MonoBehaviour _animationSource;
		private readonly Transform _transform;
		private readonly float _duration;

		public ScaleAnimation(MonoBehaviour animationSource, Transform transform, float duration)
		{
			_animationSource = animationSource;
			_transform = transform;
			_duration = duration;
		}

		public IFuture PlayIn()
		{
			Promise promise = PromisePool.Pull();
			_animationSource.StartCoroutine(AnimationInCoroutine(promise));
			return FuturePool.Pull(promise);
		}

		public IFuture PlayOut()
		{
			Promise promise = PromisePool.Pull();
			_animationSource.StartCoroutine(AnimationOutCoroutine(promise));
			return FuturePool.Pull(promise);
		}

		private IEnumerator AnimationInCoroutine(Promise promise)
		{
			float timer = 0f;
			_transform.localScale = Vector3.zero;
			do
			{
				yield return null;
				timer += Time.deltaTime;
				_transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, timer / _duration);
			}
			while (timer < _duration);
			promise.Resolve();
			promise.Reset();
			PromisePool.Release(promise);
		}

		private IEnumerator AnimationOutCoroutine(Promise promise)
		{
			float timer = 0f;
			_transform.localScale = Vector3.one;
			do
			{
				yield return null;
				timer += Time.deltaTime;
				_transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, timer / _duration);
			}
			while (timer < _duration);
			promise.Resolve();
			promise.Reset();
			PromisePool.Release(promise);
		}
	}
}
#endif