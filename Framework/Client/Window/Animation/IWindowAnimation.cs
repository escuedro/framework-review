﻿#if !NOT_UNITY3D
using Framework.Futures;

namespace Framework.UI
{
	public interface IWindowAnimation
	{
		public IFuture PlayIn();
		public IFuture PlayOut();
	}
}
#endif