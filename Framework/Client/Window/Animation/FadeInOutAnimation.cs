﻿#if !NOT_UNITY3D
using System.Collections;
using Framework.Futures;
using UnityEngine;

namespace Framework.UI
{
	public class FadeInOutAnimation : IWindowAnimation
	{
		private readonly MonoBehaviour _animationSource;
		private readonly CanvasGroup _canvasGroup;
		private readonly float _duration;
		public const float DefaultDuration = 0.1f;

		public FadeInOutAnimation(MonoBehaviour animationSource, CanvasGroup canvasGroup, float duration = DefaultDuration)
		{
			_animationSource = animationSource;
			_canvasGroup = canvasGroup;
			_duration = duration;
		}

		public IFuture PlayIn()
		{
			Promise promise = PromisePool.Pull();
			_animationSource.StartCoroutine(AnimationInCoroutine(promise));
			return FuturePool.Pull(promise);
		}

		public IFuture PlayOut()
		{
			Promise promise = PromisePool.Pull();
			_animationSource.StartCoroutine(AnimationOutCoroutine(promise));
			return FuturePool.Pull(promise);
		}

		private IEnumerator AnimationInCoroutine(Promise promise)
		{
			float timer = 0f;
			_canvasGroup.alpha = 0f;
			do
			{
				yield return null;
				timer += Time.deltaTime;
				_canvasGroup.alpha = Mathf.Lerp(0f, 1f, timer / _duration);
			}
			while (timer < _duration);
			promise.Resolve();
			promise.Reset();
			PromisePool.Release(promise);
		}

		private IEnumerator AnimationOutCoroutine(Promise promise)
		{
			float timer = 0f;
			_canvasGroup.alpha = 1f;
			do
			{
				yield return null;
				timer += Time.deltaTime;
				_canvasGroup.alpha = Mathf.Lerp(1f, 0f, timer / _duration);
			}
			while (timer < _duration);
			promise.Resolve();
			promise.Reset();
			PromisePool.Release(promise);
		}
	}
}
#endif