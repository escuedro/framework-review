﻿#if !NOT_UNITY3D

using System;
using UnityEngine;

namespace Framework.UI
{
	public class WindowManagerRoot : MonoBehaviour
	{
		[SerializeField]
		private UIButton _uiButton;

		public void Init(Action onClickAction)
		{
			_uiButton.SetOnClick(onClickAction);
		}

		public void MoveOverlayUnderLastWindow()
		{
			int lastWindowSiblingIndex = transform.childCount - 1;
			_uiButton.transform.SetSiblingIndex(lastWindowSiblingIndex - 1);
		}

		public void SetOverlayHierarchyPosition(int positionIndex)
		{
			_uiButton.transform.SetSiblingIndex(positionIndex);
		}

		public void SetOverlayActive(bool isActive)
		{
			_uiButton.gameObject.SetActive(isActive);
		}
	}
}

#endif