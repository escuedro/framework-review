﻿#if !NOT_UNITY3D

namespace Framework.UI
{
	public readonly struct WindowParametersPair
	{
		public readonly IWindow Window;
		public readonly object[] Parameters;

		public WindowParametersPair(IWindow window, object[] parameters)
		{
			Window = window;
			Parameters = parameters;
		}

		public bool EqualsArguments(object[] argsToCompare)
		{
			for (int i = 0; i < argsToCompare.Length; i++)
			{
				if (!argsToCompare[i].Equals(Parameters[i]))
				{
					return false;
				}
			}
			return true;
		}
	}
}

#endif