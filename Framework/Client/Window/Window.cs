﻿#if !NOT_UNITY3D

using System;
using Framework.Futures;
using Framework.RPC;
using Framework.View;
using UnityEngine;

namespace Framework.UI
{
	public abstract class BaseWindow : ViewBehaviour, IWindow
	{
		[Inject]
		public IWindowManager WindowManager;
		[Inject]
		public IController Controller;

		public IFuture Open(object[] arguments)
		{
			return Init(arguments);
		}

		public abstract IFuture Init(object[] arguments);

		protected void Close()
		{
			WindowManager.Close(this);
		}

		protected void OpenWindow<TWindow>(params object[] args) where TWindow : IWindow
		{
			WindowManager.Open(typeof(TWindow), args);
		}

		protected void OpenWindow(Type windowType, params object[] args)
		{
			WindowManager.Open(windowType, args);
		}

		protected void Execute(SyncCommand syncCommand)
		{
			Controller.Execute(syncCommand);
		}

		protected IFuture Execute(AsyncCommand asyncCommand)
		{
			return Controller.Execute(asyncCommand);
		}

		protected IFuture Execute(RpcCommand rpcCommand)
		{
			return Controller.Execute(rpcCommand);
		}

		public abstract IFuture OnClose();

		public Transform Transform { get => transform; }
	}

	public abstract class Window : BaseWindow
	{
		public override IFuture Init(object[] arguments)
		{
			if (arguments.Length > 0)
			{
				throw new Exception($"Arguments amount for window with type [{GetType().Name}] must be equals 0");
			}
			return Open();
		}

		public abstract IFuture Open();
	}

	public abstract class Window<T1> : BaseWindow
	{
		public override IFuture Init(object[] arguments)
		{
			if (arguments.Length != 1)
			{
				throw new Exception($"Arguments amount for window with type [{GetType().Name}] must be equals 1");
			}
			if (arguments[0] is T1 typedArgument)
			{
				return Open(typedArgument);
			}
			else
			{
				throw new ArgumentException(
						$"Wrong argument 1. Expected {typeof(T1).Name} but found {arguments[0].GetType().Name}");
			}
		}

		protected abstract IFuture Open(T1 argument);
	}

	public abstract class Window<T1, T2> : BaseWindow
	{
		public override IFuture Init(object[] arguments)
		{
			if (arguments.Length != 2)
			{
				throw new Exception($"Arguments amount for window with type [{GetType().Name}] must be equals 2");
			}
			if (arguments[0] is T1 typedArg1)
			{
				if (arguments[1] is T2 typedArg2)
				{
					return Open(typedArg1, typedArg2);
				}
				else
				{
					throw new ArgumentException(
							$"wrong Argument 2 expected {typeof(T2).Name} but found {arguments[1].GetType().Name}");
				}
			}
			else
			{
				throw new ArgumentException(
						$"wrong Argument 1 expected {typeof(T1).Name} but found {arguments[0].GetType().Name}");
			}
		}

		protected abstract IFuture Open(T1 argument1, T2 argument2);
	}

	public abstract class Window<T1, T2, T3> : BaseWindow
	{
		public override IFuture Init(object[] arguments)
		{
			if (arguments.Length != 3)
			{
				throw new Exception($"Arguments amount for window with type [{GetType().Name}] must be equals 3");
			}
			if (arguments[0] is T1 typedArg1)
			{
				if (arguments[1] is T2 typedArg2)
				{
					if (arguments[2] is T3 typedArg3)
					{
						return Open(typedArg1, typedArg2, typedArg3);
					}
					else
					{
						throw new ArgumentException(
								$"wrong Argument 3 expected {typeof(T3).Name} but found {arguments[2].GetType().Name}");
					}
				}
				else
				{
					throw new ArgumentException(
							$"wrong Argument 2 expected {typeof(T2).Name} but found {arguments[1].GetType().Name}");
				}
			}
			else
			{
				throw new ArgumentException(
						$"wrong Argument 1 expected {typeof(T1).Name} but found {arguments[0].GetType().Name}");
			}
		}

		protected abstract IFuture Open(T1 argument1, T2 argument2, T3 argument3);
	}
}

#endif