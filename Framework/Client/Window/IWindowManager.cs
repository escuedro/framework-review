﻿#if !NOT_UNITY3D

using System;

namespace Framework.UI
{
	public interface IWindowManager
	{
		public void Open<TWindow>(params object[] arguments) where TWindow : IWindow;

		public void Open(Type windowType, params object[] arguments);

		public void Close<TWindow>() where TWindow : IWindow;

		public void Close(IWindow window);

		public void CloseAllWindows();
	}
}

#endif