﻿#if !NOT_UNITY3D

using Framework.Futures;
using UnityEngine;

namespace Framework.UI
{
	public interface IWindow
	{
		public IFuture Open(params object[] arguments);
		public IFuture OnClose();

		public Transform Transform { get; }
	}
}

#endif