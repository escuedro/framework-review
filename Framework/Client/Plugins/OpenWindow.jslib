var OpenWindowPlugin = {
    OpenNewTab: function(link)
    {
    	try
    	{
    		document.gotourl = Pointer_stringify(link);
        	document.onmouseup = function()
        	{
        		try
        		{
        			window.open(document.gotourl);
        		}
        		catch(clickErr)
        		{
    				console.log(clickErr)
        		}

        		document.onmouseup = null;
	        }
    	}catch (err)
    	{
    		console.log(err)
    	}
    }
};

mergeInto(LibraryManager.library, OpenWindowPlugin);