#if !NOT_UNITY3D
using UnityEngine;

namespace Framework
{
	public static class OpenWindowUtils
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		[System.Runtime.InteropServices.DllImport("__Internal")]
		private static extern void OpenNewTab(string url);
#endif

		public static void OpenLinkInNewTab(string url)
		{
#if UNITY_WEBGL && !UNITY_EDITOR
			OpenNewTab(url);
#else
			Application.OpenURL(url);
#endif
		}
	}
}
#endif