﻿namespace Framework
{
	public abstract class SyncCommand : Command
	{
		public abstract void Execute();
	}
}