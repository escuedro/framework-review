﻿using Framework.Futures;

namespace Framework
{
	public abstract class AsyncCommand : Command
	{
		protected Promise Promise;
		public abstract void Execute();

		public void SetPromise(Promise promise)
		{
			Promise = promise;
		}
	}
}