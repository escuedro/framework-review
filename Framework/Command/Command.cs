﻿namespace Framework
{
	public abstract class Command
	{
		public IResolver Resolver;

		[Pure]
		protected virtual T Get<T>()
		{
			return Resolver.Resolve<T>();
		}
	}
}