﻿using System.Collections.Generic;

namespace Framework
{
	public class ProgramParameters
	{
		private readonly Dictionary<string, string> _namePathDictionary;

		public ProgramParameters(string[] args)
		{
			_namePathDictionary = LoadParameters(args);
		}

		private Dictionary<string, string> LoadParameters(string[] args)
		{
			Dictionary<string, string> namesPaths = new Dictionary<string, string>();
			string argName = null;
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i].StartsWith("-"))
				{
					if (argName != null)
					{
						namesPaths.Add(argName, "true");
					}
					argName = args[i];
				}
				else
				{
					if (argName != null)
					{
						namesPaths.Add(argName, args[i]);
						argName = null;
					}
				}
			}
			return namesPaths;
		}

		public string GetPathByName(string name)
		{
			if (_namePathDictionary.TryGetValue(name, out string path))
			{
				return path;
			}
			return "";
		}
	}
}