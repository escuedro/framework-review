﻿#if NOT_UNITY3D
namespace Framework
{
	public class ServerConfig
	{
		public static ServerConfig Default =>
				new ServerConfig
				{
						GameName = "Test Game",
						ServerPort = 5000,
						DatabaseConnectionString = "Host=127.0.0.1;" +
								"Username=postgres;" +
								"Password=mysecretpassword;" +
								"Database=postgres;" +
								"Port=5432",
						PrometheusEnabled = false,
						ConsoleLog = Log.Level.Info,
						WarmupDbConnection = false,
						OpenIdAuthority = "https://my.inanomo.com",
						OpenIdClientId = "ad5323ed-3f09-406e-a496-2642bdd75017",
						OpenIdClientSecret = "NRTQubDWz4oB7qsfUsz72f68baXQs2",
						OpenIdRedirectUri = "https://localhost:5000/open_id",
						OpenIdLogoutRedirectUri = "https://localhost:5000/logout_complete",
						X509Password = "X509Password",
						X509File = "Game/dev_cert.pfx",
						CheatsEnabled = true
				};

		public string GameName;
		public int ServerPort;
		public string DatabaseConnectionString;
		public Log.Level ConsoleLog;
		public string LokiUrl;
		public Log.Level LokiLog;
		public bool PrometheusEnabled;
		public bool WarmupDbConnection;
		public bool CheatsEnabled;
		public string OpenIdAuthority;
		public string OpenIdClientId;
		public string OpenIdClientSecret;
		public string OpenIdRedirectUri;
		public string OpenIdLogoutRedirectUri;
		public string X509Password;
		public string X509File;
	}
}
#endif