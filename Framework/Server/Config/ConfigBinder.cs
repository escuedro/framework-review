﻿#if NOT_UNITY3D

using System.IO;
using System.Text.Json;

namespace Framework
{
	[Bind(BindTo = typeof(IConfigBinder))]
	public class ConfigBinder : IConfigBinder
	{
		private readonly ProgramParameters _programParameters;
		private readonly IBinder _binder;

		private static readonly JsonSerializerOptions Options = new JsonSerializerOptions { IncludeFields = true };

		[Inject]
		public ConfigBinder(ProgramParameters programParameters, IBinder binder)
		{
			_programParameters = programParameters;
			_binder = binder;
		}

		public void BindConfig<TConfig>(string parameterName, TConfig defaultConfig)
		{
			string configPath = _programParameters.GetPathByName(parameterName);
			if (!string.IsNullOrEmpty(configPath))
			{
				Log.Info($"use config {typeof(TConfig).Name} from file {configPath}");
				byte[] fileBytes = File.ReadAllBytes(configPath);
				TConfig deserializedConfig = JsonSerializer.Deserialize<TConfig>(fileBytes, Options);
				_binder.BindInstance(deserializedConfig);
			}
			else
			{
				Log.Info($"use default config {typeof(TConfig).Name}");
				_binder.BindInstance(defaultConfig);
			}
		}
	}
}

#endif