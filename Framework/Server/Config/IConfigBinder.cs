﻿namespace Framework
{
	public interface IConfigBinder
	{
		public void BindConfig<TConfig>(string parameterName, TConfig defaultConfig);
	}
}