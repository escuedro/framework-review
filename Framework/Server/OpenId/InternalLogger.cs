#if NOT_UNITY3D
using System;
using Microsoft.Extensions.Logging;
using Serilog.Core;
using Serilog.Events;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Framework.Server.OpenId
{
	public class InternalLogger : ILogEventSink
	{
		public void Emit(LogEvent logEvent)
		{
			Log.Level logEventLevel = (Log.Level)(int)logEvent.Level;
			Log.OnLog?.Invoke(logEventLevel, logEvent.RenderMessage());
		}
	}

	public class InternalLoggerProvider : ILoggerProvider, ILogger
	{
		public void Dispose()
		{
		}

		public ILogger CreateLogger(string categoryName)
		{
			return this;
		}

		public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
		{
			Log.Level logEventLevel = (Log.Level)(int)logLevel;
			string message = formatter.Invoke(state, exception);

			Framework.Log.OnLog?.Invoke(logEventLevel, message);
		}

		public bool IsEnabled(LogLevel logLevel)
		{
			return logLevel > LogLevel.Debug;
		}

		public IDisposable BeginScope<TState>(TState state)
		{
			return this;
		}
	}
}
#endif