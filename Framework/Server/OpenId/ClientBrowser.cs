#if NOT_UNITY3D
using System.Threading;
using System.Threading.Tasks;
using IdentityModel.OidcClient.Browser;

namespace Framework.Server.OpenId
{
	public class ClientBrowser : IBrowser
	{
		private readonly AutoResetEvent _resultReady = new AutoResetEvent(true);
		private BrowserResult _result;
		private CancellationToken _cancellationToken;
		private string _redirectUrl;
		public string RedirectUrl => _redirectUrl;

		public void SetResult(BrowserResult browserResult)
		{
			_result = browserResult;
			_resultReady.Set();
		}

		public Task<BrowserResult> InvokeAsync(BrowserOptions options,
				CancellationToken cancellationToken = new CancellationToken())
		{
			_cancellationToken = cancellationToken;
			_redirectUrl = options.StartUrl;
			return Task.Run(GetExternal, cancellationToken);
		}

		private BrowserResult GetExternal()
		{
			_cancellationToken.ThrowIfCancellationRequested();
			_resultReady.WaitOne();
			while (_result == null)
			{
				Thread.Sleep(10);
			}
			return _result;
		}

		public string WaitRedirectUrl()
		{
			while (string.IsNullOrEmpty(_redirectUrl))
			{
				_cancellationToken.ThrowIfCancellationRequested();
				Thread.Sleep(1);
			}
			return _redirectUrl;
		}
	}
}
#endif