﻿#if NOT_UNITY3D
using PetaPoco;

namespace Framework.Server.OpenId
{
	[ExplicitColumns]
	[PrimaryKey("open_id", AutoIncrement = false)]
	public class OpenIdToGameId
	{
		[Column(Name = "open_id")]
		public string OpenId { get; set; }

		[Column(Name = "game_id")]
		public long GameId { get; set; }
	}
}
#endif