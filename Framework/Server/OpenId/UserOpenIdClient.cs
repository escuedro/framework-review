#if NOT_UNITY3D
using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Framework.Server.DataBase;
using IdentityModel.Client;
using IdentityModel.OidcClient;
using IdentityModel.OidcClient.Browser;
using SAS.Model.Time;

namespace Framework.Server.OpenId
{
	public class UserOpenIdClient
	{
		private readonly DatabaseLoader _databaseLoader;
		private readonly OidcClientOptions _oidcClientOptions;
		private readonly DiscoveryCache _discoveryCache;
		private OidcClient _oidcClient;
		private Task<LoginResult> _inProgress;
		private readonly ClientBrowser _clientBrowser;

		private string _userId;
		private string _accessToken;
		private string _refreshToken;

		[CanBeNull]
		public string UserId
		{
			get
			{
				if (_userId == null)
				{
					_userId = _claims?.FindFirst("sub")?.Value;
				}
				return _userId;
			}
		}

		private ulong _gameId;
		private readonly string _logoutRedirectUri;

		public ulong GameId => _gameId;

		[CanBeNull]
		public string AccessToken => _accessToken;
		[CanBeNull]
		public string RefreshToken => _refreshToken;
		public TimePoint ExpirationAccessToken;


		[CanBeNull]
		private ClaimsIdentity _claims;

		public UserOpenIdClient(ServerConfig serverConfig, DatabaseLoader databaseLoader, DiscoveryCache discoveryCache)
		{
			_databaseLoader = databaseLoader;
			_clientBrowser = new ClientBrowser();
			_discoveryCache = discoveryCache;
			_logoutRedirectUri = serverConfig.OpenIdLogoutRedirectUri;

			DiscoveryDocumentResponse discoveryDocument = ResolveDiscovery();
			OidcClientOptions options = new OidcClientOptions
			{
					Authority = serverConfig.OpenIdAuthority,
					ClientId = serverConfig.OpenIdClientId,
					RedirectUri = serverConfig.OpenIdRedirectUri,
					ClientSecret = serverConfig.OpenIdClientSecret,
					ProviderInformation = new ProviderInformation()
					{
							AuthorizeEndpoint = discoveryDocument.AuthorizeEndpoint,
							IssuerName = serverConfig.OpenIdAuthority,
							KeySet = discoveryDocument.KeySet,
							TokenEndpoint = discoveryDocument.TokenEndpoint,
							EndSessionEndpoint = discoveryDocument.EndSessionEndpoint,
							UserInfoEndpoint = discoveryDocument.UserInfoEndpoint,
							TokenEndPointAuthenticationMethods =
									discoveryDocument.TokenEndpointAuthenticationMethodsSupported
					},
					RefreshDiscoveryDocumentForLogin = false,
					Scope = "openid offline_access",
					FilterClaims = false,
					Browser = _clientBrowser,
					IdentityTokenValidator = new JwtHandlerIdentityTokenValidator(),
					RefreshTokenInnerHttpHandler = new HttpClientHandler(),
			};
			options.LoggerFactory.AddProvider(new InternalLoggerProvider());
			_oidcClientOptions = options;
		}

		private DiscoveryDocumentResponse ResolveDiscovery()
		{
			Task<DiscoveryDocumentResponse> discoveryTask = _discoveryCache.GetAsync();
			discoveryTask.Wait();
			if (!discoveryTask.IsCompletedSuccessfully)
			{
				throw new Exception("Fail to load discovery document");
			}
			return discoveryTask.Result;
		}

		public string StartAuth()
		{
			if (_oidcClient == null)
			{
				_oidcClient = new OidcClient(_oidcClientOptions);
			}

			if (_inProgress == null || _inProgress.IsFaulted || _inProgress.IsCanceled)
			{
				_inProgress = _oidcClient.LoginAsync(new LoginRequest());
				string redirectUrl = _clientBrowser.WaitRedirectUrl();
				return redirectUrl;
			}
			return _clientBrowser.RedirectUrl;
		}

		public void TryRestore(string accessToken)
		{
			if (_oidcClient == null)
			{
				_oidcClient = new OidcClient(_oidcClientOptions);
			}
			var client = new HttpClient();

			Task<UserInfoResponse> introspectTask = client.GetUserInfoAsync(new UserInfoRequest
			{
					Address = $"{_oidcClientOptions.Authority}/connect/userinfo",
					ClientId = _oidcClientOptions.ClientId,
					ClientSecret = _oidcClientOptions.ClientSecret,

					Token = accessToken
			});
			introspectTask.Wait();
			if (introspectTask.IsFaulted || introspectTask.IsCanceled)
			{
				throw introspectTask.Exception ?? new Exception("unknown GetUserInfoAsync exception");
			}
			if (introspectTask.IsCompletedSuccessfully)
			{
				if (introspectTask.Result.IsError)
				{
					throw introspectTask.Exception ?? new Exception(introspectTask.Result.Error);
				}
				_accessToken = accessToken;
				//TODO realize real time left
				ExpirationAccessToken = TimePoint.Now + TimeDuration.CreateBySeconds(60 * 30);
				_claims = new ClaimsIdentity(introspectTask.Result.Claims);
				if (UserId == null)
				{
					throw new Exception("No user id in claims");
				}
				OpenIdToGameId gameId = _databaseLoader.TryGetGameId(UserId);
				if (gameId == null || gameId.GameId == 0)
				{
					throw new Exception("No game id found to this open id");
				}
				_gameId = (ulong)gameId.GameId;
			}
		}

		public void TryRefresh(string refreshToken)
		{
			if (_oidcClient == null)
			{
				_oidcClient = new OidcClient(_oidcClientOptions);
			}
			var client = new HttpClient();

			var refreshTokenTask = client.RequestRefreshTokenAsync(new RefreshTokenRequest
			{
					Address = $"{_oidcClientOptions.Authority}/connect/token",
					ClientId = _oidcClient.Options.ClientId,
					ClientSecret = _oidcClient.Options.ClientSecret,
					ClientAssertion = _oidcClient.Options.ClientAssertion,
					ClientCredentialStyle = _oidcClient.Options.TokenClientCredentialStyle,
					RefreshToken = refreshToken
			});

			refreshTokenTask.Wait();
			if (refreshTokenTask.IsFaulted || refreshTokenTask.IsCanceled)
			{
				throw refreshTokenTask.Exception ?? new Exception("unknown GetUserInfoAsync exception");
			}
			if (refreshTokenTask.IsCompletedSuccessfully)
			{
				if (refreshTokenTask.Result.IsError)
				{
					throw refreshTokenTask.Exception ?? new Exception(refreshTokenTask.Result.Error);
				}
				_accessToken = refreshTokenTask.Result.AccessToken;
				ExpirationAccessToken = TimePoint.Now + TimeDuration.CreateBySeconds(refreshTokenTask.Result.ExpiresIn);
				_refreshToken = refreshTokenTask.Result.RefreshToken;

				if (UserId == null)
				{
					throw new Exception("No user id in claims");
				}
				OpenIdToGameId gameId = _databaseLoader.TryGetGameId(UserId);
				if (gameId == null || gameId.GameId == 0)
				{
					throw new Exception("No game id found to this open id");
				}
				_gameId = (ulong)gameId.GameId;
			}
		}

		public void TryLogout(string refreshToken)
		{
			TryRefresh(refreshToken);
		}

		public void OnOpenIdCallback(string address)
		{
			string result = address.Remove(0, "/open_id".Length);
			_clientBrowser.SetResult(new BrowserResult()
					{ Response = result, ResultType = BrowserResultType.Success });
			_inProgress.Wait();
			if (_inProgress.Result == null)
			{
				throw new Exception("Empty open_id result");
			}
			ExpirationAccessToken = TimePoint.CreateByUtc(_inProgress.Result.AccessTokenExpiration.UtcDateTime);

			_claims = new ClaimsIdentity((_inProgress.Result.User?.Identity as ClaimsIdentity)?.Claims);
			_accessToken = _inProgress.Result.AccessToken;
			_refreshToken = _inProgress.Result.RefreshToken;
			if (_claims != null)
			{
				string userId = UserId;
				if (!string.IsNullOrEmpty(userId))
				{
					OpenIdToGameId gameId = _databaseLoader.TryGetGameId(userId);
					if (gameId == null)
					{
						try
						{
							using DatabaseTransaction databaseTransaction = _databaseLoader.StartTransaction();
							gameId = _databaseLoader.Register(userId);
							_gameId = (ulong)gameId.GameId;
							_databaseLoader.AddEmpty(new LongData { UserId = gameId.GameId, Data = "{}" });
							_databaseLoader.AddEmpty(new ShortData { UserId = gameId.GameId, Data = "{}" });
							_databaseLoader.AddEmpty(new UserInfo
									{ UserId = gameId.GameId, Name = _claims.Name ?? "Unknown", Data = "{}" });
							databaseTransaction.Complete();
						}
						catch (Exception e)
						{
							Log.Fatal(e);
						}
					}
					else
					{
						_gameId = (ulong)gameId.GameId;
					}
				}
				else
				{
					throw new Exception($"Hello {_claims.Name}, Fail to obtain your id");
				}
				return;
			}
			throw new Exception("Fail to authorize, please try again");
		}
	}
}
#endif