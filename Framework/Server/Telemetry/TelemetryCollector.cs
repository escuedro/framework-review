#if NOT_UNITY3D
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Prometheus;
using Prometheus.DotNetRuntime;

namespace Framework.Server.Telemetry
{
	[Bind]
	public class TelemetryCollector : IDisposable
	{
		[Inject]
		public DisposeShutdown DisposeShutdown;
		[Inject]
		public ServerConfig ServerConfig;

		private IDisposable _collector;
		private readonly byte[] _disabledResponse = Encoding.UTF8.GetBytes("telemetry disabled");

		[Inject]
		public void StartCollect()
		{
			if (ServerConfig.PrometheusEnabled)
			{
				DisposeShutdown.DisposeOnShutdown(this);
				_collector = DotNetRuntimeStatsBuilder.Default().StartCollecting();
				Log.Info("telemetry available at '/metrics' endpoint");
			}
			else
			{
				Log.Info("telemetry disabled");
			}
		}

		public void Dispose()
		{
			_collector?.Dispose();
		}

		public byte[] GetMetrics()
		{
			if (_collector != null)
			{
				using MemoryStream memoryStream = new MemoryStream();
				Task collectTask = Metrics.DefaultRegistry.CollectAndExportAsTextAsync(memoryStream);
				collectTask.Wait();
				return memoryStream.ToArray();
			}
			{
				return _disabledResponse;
			}
		}
	}
}
#endif