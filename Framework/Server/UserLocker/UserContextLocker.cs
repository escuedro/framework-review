#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Threading;

namespace Framework.Server
{
	public class UserContextLocker : IDisposable
	{
		private readonly List<UserContext> _lockedUsers = new List<UserContext>();
		private readonly UserContextComparer _contextComparer = new UserContextComparer();
		private readonly IUserStorage _userStorage;

		public UserContextLocker(IUserStorage userStorage)
		{
			_userStorage = userStorage;
		}

		public UserContext Lock(ulong userId)
		{
			UnlockAll();
			UserContext userContext = _userStorage.GetUser(userId);
			_lockedUsers.Add(userContext);
			LockInternal();
			return userContext;
		}

		public IEnumerable<UserContext> GetAllUsers(bool needToSave)
		{
			ulong minUserId = 1;
			ulong maxUserId = _userStorage.GetMaxUserId();
			for (ulong i = minUserId; i < maxUserId + 1; i++)
			{
				UserContext user = Lock(i);

				yield return user;

				if (needToSave)
				{
					_userStorage.SaveUserTransaction(user);
				}

				Monitor.Exit(user);
				_lockedUsers.Remove(user);
			}
		}

		private void LockInternal()
		{
			_lockedUsers.Sort(_contextComparer);
			foreach (UserContext lockedUser in _lockedUsers)
			{
				Monitor.Enter(lockedUser);
			}
		}

		private void UnlockAll()
		{
			foreach (UserContext lockedUser in _lockedUsers)
			{
				Monitor.Exit(lockedUser);
			}
		}

		public void RollbackAll()
		{
			foreach (UserContext lockedUser in _lockedUsers)
			{
				_userStorage.RollbackUser(lockedUser);
			}
		}

		public void SaveAll()
		{
			foreach (UserContext lockedUser in _lockedUsers)
			{
				_userStorage.SaveUser(lockedUser);
			}
		}

		public void Dispose()
		{
			UnlockAll();
			_lockedUsers.Clear();
		}
	}
}
#endif