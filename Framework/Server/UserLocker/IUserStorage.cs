#if NOT_UNITY3D

namespace Framework.Server
{
	public interface IUserStorage
	{
		UserContext GetUser(ulong id);
		void RollbackUser(UserContext userContext);
		ulong GetMaxUserId();
		void SaveUser(UserContext userContext);
		void SaveUserTransaction(UserContext userContext);
	}
}
#endif