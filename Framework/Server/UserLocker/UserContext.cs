#if NOT_UNITY3D
using System.Collections.Generic;

namespace Framework.Server
{
	public class UserContext
	{
		public readonly ulong Id;
		public string Guid;

		public Container Container;

		public UserContext(ulong id, Container container)
		{
			Id = id;
			Container = container;
		}
	}

	public class UserContextComparer : IComparer<UserContext>
	{
		public int Compare(UserContext x, UserContext y)
		{
			if (ReferenceEquals(x, y))
			{
				return 0;
			}
			if (ReferenceEquals(null, y))
			{
				return 1;
			}
			if (ReferenceEquals(null, x))
			{
				return -1;
			}
			return x.Id.CompareTo(y.Id);
		}
	}
}
#endif