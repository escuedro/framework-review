#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Threading;
using Framework.Injection;
using Framework.Server.DataBase;
using Framework.Server.DataParser;

namespace Framework.Server
{
	[Bind(BindTo = typeof(IUserStorage))]
	internal class UserStorage : IUserStorage
	{
		private readonly Dictionary<ulong, UserContext> _userContexts = new Dictionary<ulong, UserContext>();
		[Inject]
		public DbDataParser DbDataParser;
		[Inject]
		public BaseBinder BaseBinder;
		[Inject]
		public DatabaseLoader DatabaseLoader;
		[Inject]
		public TypeCollection TypeCollection;
		[Inject]
		public IResolver Resolver;

		private readonly object _createMutex = new object();

		[NotNull]
		public UserContext GetUser(ulong id)
		{
			UserContext result;
			lock (_userContexts)
			{
				_userContexts.TryGetValue(id, out result);
			}
			if (result != null)
			{
				return result;
			}
			return LoadContext(id);
		}

		[NotNull]
		private UserContext LoadContext(ulong id)
		{
			UserContext userContext;
			lock (_createMutex)
			{
				UserContext result;
				lock (_userContexts)
				{
					_userContexts.TryGetValue(id, out result);
				}
				if (result != null)
				{
					return result;
				}
				userContext = new UserContext(id, new Container());
				Monitor.Enter(userContext);
				lock (_userContexts)
				{
					_userContexts.Add(id, userContext);
				}
			}
			try
			{
				LoadContainer(id, userContext.Container);
			}
			catch (Exception)
			{
				lock (_userContexts)
				{
					_userContexts.Remove(id);
				}
				throw;
			}
			finally
			{
				Monitor.Exit(userContext);
			}

			return userContext;
		}

		private void LoadContainer(ulong id, Container container)
		{
			BaseBinder.Bind(container);
			UserInfo userInfo = DatabaseLoader.Get<UserInfo>(id);
			ShortData shortData = DatabaseLoader.Get<ShortData>(id);
			LongData longData = DatabaseLoader.Get<LongData>(id);
			FillContainer(container, userInfo, shortData, longData);
		}

		public void RollbackUser(UserContext userContext)
		{
			Container container = userContext.Container;
			UserInfo userInfo = container.Resolve<UserInfo>();
			ShortData shortData = container.Resolve<ShortData>();
			LongData longData = container.Resolve<LongData>();
			container.Clear();

			FillContainer(container, userInfo, shortData, longData);
		}

		public void SaveUser(UserContext userContext)
		{
			Container container = userContext.Container;
			RootHashInfo rootHashInfo = container.Resolve<RootHashInfo>();

			UpdateIfChanged<UserInfo>(container, rootHashInfo);
			UpdateIfChanged<ShortData>(container, rootHashInfo);
			UpdateIfChanged<LongData>(container, rootHashInfo);
		}

		public void SaveUserTransaction(UserContext userContext)
		{
			using DatabaseTransaction databaseTransaction = DatabaseLoader.StartTransaction();
			SaveUser(userContext);
			databaseTransaction.Complete();
		}

		private void UpdateIfChanged<TData>(Container container, RootHashInfo rootHashInfo) where TData : IUserData
		{
			TData userData = container.Resolve<TData>();
			int oldUserDataHash = rootHashInfo.HashInfo[typeof(TData)];
			DbDataParser.Save(container, userData);
			int userDataHash = rootHashInfo.HashInfo[typeof(TData)];
			if (oldUserDataHash != userDataHash)
			{
				Log.Trace($"{typeof(TData).Name} component changed. Update db");
				DatabaseLoader.Update(userData);
			}
		}

		private void FillContainer(Container container, UserInfo userInfo, ShortData shortData, LongData longData)
		{
			TypeCollection.CopyContainer(Resolver, container);
			DbDataParser.Parse(container, userInfo);
			DbDataParser.Parse(container, shortData);
			DbDataParser.Parse(container, longData);

			container.BindInstance(userInfo);
			container.BindInstance(shortData);
			container.BindInstance(longData);
			container.BindInstance(userInfo.Name);
		}

		public ulong GetMaxUserId()
		{
			return DatabaseLoader.GetMaxUserId();
		}
	}
}
#endif