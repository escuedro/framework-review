#if NOT_UNITY3D
using System;
using System.Collections.Generic;

namespace Framework.Server
{
	[Bind]
	public class DisposeShutdown
	{
		private readonly List<IDisposable> _disposables = new List<IDisposable>();

		public void DisposeOnShutdown(IDisposable disposable)
		{
			lock (_disposables)
			{
				_disposables.Add(disposable);
			}
		}

		public void Shutdown()
		{
			lock (_disposables)
			{
				foreach (IDisposable disposable in _disposables)
				{
					disposable.Dispose();
				}
				_disposables.Clear();
			}
		}
	}
}
#endif