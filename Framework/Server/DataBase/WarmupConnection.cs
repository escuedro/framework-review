#if NOT_UNITY3D
using System;
using System.Threading;
using System.Threading.Tasks;
using Framework;
using Framework.Server.DataBase;

namespace Global.Service
{
	[Bind]
	internal class WarmupConnection
	{
		[Inject]
		public void Warmup(DatabaseLoader databaseLoader, ServerConfig serverConfig)
		{
			if (!serverConfig.WarmupDbConnection)
			{
				return;
			}

			int processorCount = Environment.ProcessorCount;
			Task[] warmups = new Task[processorCount];

			for (int i = 0; i < processorCount; i++)
			{
				int index = i;
				Task task = Task.Run(() => WarmupDbConnection(databaseLoader, index));
				warmups[i] = task;
			}
			Task.WaitAll(warmups);
		}

		private static void WarmupDbConnection(DatabaseLoader databaseLoader, int index)
		{
			Thread.Sleep(100 + 200 * index);
			Thread.CurrentThread.Name = $"prewarmed_{Thread.CurrentThread.ManagedThreadId.ToString()}";
			Log.Trace($"warmup thread {Thread.CurrentThread.Name}");
			UserInfo userInfo = databaseLoader.Get<UserInfo>(1);
		}
	}
}
#endif