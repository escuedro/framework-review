#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Threading;
using Framework.Model.IdentityGenerator;
using Framework.Server.Marketplace;
using Framework.Server.OpenId;
using PetaPoco;
using PetaPoco.Core.Inflection;
using PetaPoco.Providers;

namespace Framework.Server.DataBase
{
	[Bind]
	public class DatabaseLoader
	{
		private readonly IInflector _inflector = new EnglishInflector();
		private readonly ThreadLocal<IDatabase> _databaseLocal;

		[Obsolete("Not use raw db in features")]
		public IDatabase GetDatabase() => _databaseLocal.Value;

		[Inject]
		public DatabaseLoader(ServerConfig serverConfig)
		{
			_databaseLocal = new ThreadLocal<IDatabase>(() => DatabaseConfiguration.Build().
					UsingConnectionString(serverConfig.DatabaseConnectionString).
					UsingProvider<PostgreSQLDatabaseProvider>().UsingDefaultMapper<ConventionMapper>(mapper =>
					{
						mapper.InflectTableName = (inflector, item) => inflector.Underscore(item);
						mapper.InflectColumnName = (inflector, item) => inflector.Underscore(item);
					}).Create());
		}

		public void InsertMain<T>([NotNull] T userData) where T : IUserData
		{
			string tableName = _inflector.Underscore(userData.GetType().Name);
			_databaseLocal.Value.Insert(tableName, "user_id", true, userData);
		}

		public void Insert<T>([NotNull] T userData) where T : IUserData
		{
			if (userData.UserId <= 0)
			{
				throw new ArgumentException($"Not user data {typeof(T).Name} has invalid UserId {userData.UserId}");
			}

			string tableName = _inflector.Underscore(userData.GetType().Name);
			_databaseLocal.Value.Insert(tableName, "user_id", false, userData);
		}

		[CanBeNull]
		public IdentityCounter TryGetIdentityCounter(string identityName)
		{
			IdentityCounter counter = _databaseLocal.Value.SingleOrDefault<IdentityCounter>((object)identityName);
			return counter;
		}

		[CanBeNull]
		public OpenIdToGameId TryGetGameId(string openId)
		{
			OpenIdToGameId gameId = _databaseLocal.Value.SingleOrDefault<OpenIdToGameId>((object)openId);
			return gameId;
		}

		public void UpdateIdentityCounter(IdentityCounter identityCounter, bool insert)
		{
			if (insert)
			{
				_databaseLocal.Value.Insert(identityCounter);
			}
			else
			{
				_databaseLocal.Value.Update(identityCounter);
			}
		}

		[MustUseReturnValue]
		public DatabaseTransaction StartTransaction()
		{
			_databaseLocal.Value.BeginTransaction();
			return new DatabaseTransaction(this);
		}

		public void AbortTransaction()
		{
			_databaseLocal.Value.CompleteTransaction();
		}

		public void CompleteTransaction()
		{
			_databaseLocal.Value.CompleteTransaction();
		}

		private void InsertOpenId(OpenIdToGameId openIdToGameId)
		{
			_databaseLocal.Value.Insert(openIdToGameId);
		}

		public void InsertMarketplaceGuid(MarketplaceGuidToGameId marketplaceGuidToGameId)
		{
			_databaseLocal.Value.Insert(marketplaceGuidToGameId);
		}

		public void UpdateMarketplaceGuid(MarketplaceGuidToGameId marketplaceGuidToGameId)
		{
			_databaseLocal.Value.Update(marketplaceGuidToGameId);
		}

		[CanBeNull]
		public MarketplaceGuidToGameId TryResolveGuid(string guid)
		{
			MarketplaceGuidToGameId result =
					_databaseLocal.Value.SingleOrDefault<MarketplaceGuidToGameId>((object)guid);
			return result;
		}

		public OpenIdToGameId Register(string openId)
		{
			OpenIdToGameId gameId = new OpenIdToGameId {OpenId = openId};
			IdentityCounter identityCounter = TryGetIdentityCounter("game_id");
			if (identityCounter == null)
			{
				identityCounter = new IdentityCounter {IdentityName = "game_id", Counter = 1};
				gameId.GameId = identityCounter.Counter++;
				UpdateIdentityCounter(identityCounter, true);
				InsertOpenId(gameId);
			}
			else
			{
				gameId.GameId = identityCounter.Counter++;
				UpdateIdentityCounter(identityCounter, false);
				InsertOpenId(gameId);
			}
			return gameId;
		}

		public void AddEmpty<T>([NotNull] T userData) where T : IUserData
		{
			if (userData.UserId <= 0)
			{
				throw new ArgumentException($"Not user data {typeof(T).Name} has invalid UserId {userData.UserId}");
			}
			_databaseLocal.Value.Insert(userData);
		}

		[NotNull]
		public T Get<T>(ulong userId) where T : IUserData
		{
			T userData = _databaseLocal.Value.SingleOrDefault<T>((long)userId);
			if (userData == null || userData.UserId != (long)userId)
			{
				throw new KeyNotFoundException($"Not found data {typeof(T).Name} user '{userId}' in database");
			}
			return userData;
		}

		public ulong GetMaxUserId()
		{
			Sql sql = Sql.Builder.Select("max(user_id)").From("short_data");
			return _databaseLocal.Value.First<ulong>(sql);
		}


		public void Update<T>([NotNull] T userData) where T : IUserData
		{
			if (userData.UserId <= 0)
			{
				throw new ArgumentException($"Not user data {typeof(T).Name} has invalid UserId {userData.UserId}");
			}
			_databaseLocal.Value.Update(userData);
		}
	}
}
#endif