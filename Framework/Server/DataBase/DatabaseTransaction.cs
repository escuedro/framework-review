﻿#if NOT_UNITY3D
using System;

namespace Framework.Server.DataBase
{
	public class DatabaseTransaction : IDisposable
	{
		private DatabaseLoader _databaseLoader;

		public DatabaseTransaction(DatabaseLoader databaseLoader)
		{
			_databaseLoader = databaseLoader;
		}

		public void Complete()
		{
			_databaseLoader.CompleteTransaction();
			_databaseLoader = null;
		}

		public void Dispose()
		{
			_databaseLoader?.AbortTransaction();
		}
	}
}
#endif