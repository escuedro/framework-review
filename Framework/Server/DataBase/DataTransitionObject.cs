#if NOT_UNITY3D
using PetaPoco;

namespace Framework.Server.DataBase
{
	[ExplicitColumns]
	[PrimaryKey("user_id", AutoIncrement = false)]
	public class LongData : IUserData
	{
		[Column(Name = "user_id")]
		public long UserId { get; set; }
		[Column]
		public string Data { get; set; }
	}

	[ExplicitColumns]
	[PrimaryKey("user_id", AutoIncrement = false)]
	public class ShortData : IUserData
	{
		[Column(Name = "user_id")]
		public long UserId { get; set; }
		[Column]
		public string Data { get; set; }
	}

	[ExplicitColumns]
	[PrimaryKey("user_id", AutoIncrement = false)]
	public class UserInfo : IUserData
	{
		[Column(Name = "user_id")]
		public long UserId { get; set; }
		[Column]
		public string Name { get; set; }
		[Column]
		public string Data { get; set; }
	}

	public interface IUserData
	{
		public long UserId { get; set; }
		public string Data { get; set; }
	}
}
#endif