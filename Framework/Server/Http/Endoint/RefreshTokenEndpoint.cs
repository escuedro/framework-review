﻿#if NOT_UNITY3D
using System;
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class RefreshTokenEndpoint : HttpEndpoint
	{
		private const string Name = "/refresh_token";
		public override string UrlStartsWith => Name;

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			string accessToken = GetHeader(request, AccessToken);
			if (string.IsNullOrEmpty(accessToken))
			{
				SendErrorResponse(response, $"Not found '{AccessToken}' header");
				return;
			}
			string refreshToken = GetHeader(request, RefreshToken);
			if (string.IsNullOrEmpty(refreshToken))
			{
				SendErrorResponse(response, $"Not found '{RefreshToken}' header");
				return;
			}
			UserOpenIdClient userOpenIdClient = HttpServer.GetByToken(accessToken);

			if (userOpenIdClient == null)
			{
				try
				{
					userOpenIdClient = HttpServer.RestoreClientByRefreshToken(refreshToken);
				}
				catch (Exception exception)
				{
					SendTextResponse(response, $"token cannot be refreshed {exception}",
							HttpErrorCodes.RefreshTokenNotAllowed);
					return;
				}
				SendTextResponse(response, $"{userOpenIdClient.AccessToken};{userOpenIdClient.RefreshToken}", 200);
			}
			else
			{
				try
				{
					userOpenIdClient.TryRefresh(refreshToken);
					HttpServer.UpdateToken(accessToken, userOpenIdClient);
				}
				catch (Exception exception)
				{
					SendTextResponse(response, $"token cannot be refreshed {exception}",
							HttpErrorCodes.RefreshTokenNotAllowed);
					return;
				}
				SendTextResponse(response, $"{userOpenIdClient.AccessToken};{userOpenIdClient.RefreshToken}", 200);
			}
		}
	}
}
#endif