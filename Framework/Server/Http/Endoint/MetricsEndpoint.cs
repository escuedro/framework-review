﻿#if NOT_UNITY3D
using Framework.Server.Telemetry;
using NetCoreServer;

namespace Framework.Server
{
	public class MetricsEndpoint : HttpEndpoint
	{
		public override string UrlStartsWith => "/metrics";

		private readonly TelemetryCollector _telemetryCollector;

		public MetricsEndpoint(TelemetryCollector telemetryCollector)
		{
			_telemetryCollector = telemetryCollector;
		}

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			byte[] metrics = _telemetryCollector.GetMetrics();
			SendResponse(response, metrics, metrics.Length);
		}
	}
}
#endif