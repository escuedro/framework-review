﻿#if NOT_UNITY3D
using NetCoreServer;

namespace Framework.Server
{
	public class DefaultEndpoint : HttpEndpoint
	{
		public override string UrlStartsWith => "";

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			SendGetResponse(response, "Server works fine!");
		}
	}
}
#endif