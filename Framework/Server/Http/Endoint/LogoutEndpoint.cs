#if NOT_UNITY3D
using System;
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class LogoutEndpoint : HttpEndpoint
	{
		private const string Name = "/logout";
		public override string UrlStartsWith => Name;

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			string accessToken = GetHeader(request, AccessToken);
			if (string.IsNullOrEmpty(accessToken))
			{
				SendErrorResponse(response, $"Not found '{AccessToken}' header");
				return;
			}
			string refreshToken = GetHeader(request, RefreshToken);
			if (string.IsNullOrEmpty(refreshToken))
			{
				SendErrorResponse(response, $"Not found '{RefreshToken}' header");
				return;
			}

			UserOpenIdClient userOpenIdClient = HttpServer.RemvoeByToken(accessToken);

			if (userOpenIdClient == null)
			{
				SendErrorResponse(response, "Not authorized");
			}
			else
			{
				try
				{
					userOpenIdClient.TryLogout(refreshToken);
				}
				catch (Exception exception)
				{
					SendTextResponse(response, $"Logout exception {exception}", HttpErrorCodes.Exception);
					return;
				}
			}
			SendTextResponse(response, "Success", 200);
		}
	}
}
#endif