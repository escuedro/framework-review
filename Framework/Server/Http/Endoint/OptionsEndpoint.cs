﻿#if NOT_UNITY3D
using NetCoreServer;

namespace Framework.Server
{
	public class OptionsEndpoint : HttpEndpoint
	{
		public override string Method => "OPTIONS";
		public override string UrlStartsWith => "";

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			SendOptions(response);
		}
	}
}
#endif