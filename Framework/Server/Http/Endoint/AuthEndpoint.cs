﻿#if NOT_UNITY3D
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class AuthEndpoint : HttpEndpoint
	{
		private const string Name = "/auth";
		public override string UrlStartsWith => Name;

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			string tempKey = request.Url.Remove(0, Name.Length);
			if (string.IsNullOrEmpty(tempKey))
			{
				SendErrorResponse(response, "Please add your temp key");
				return;
			}
			UserOpenIdClient openIdClient = HttpServer.ResolveClientByTempKey(tempKey, session);
			string redirectUrl = openIdClient.StartAuth();
			if (!string.IsNullOrEmpty(redirectUrl))
			{
				SendRedirect(response, redirectUrl);
			}
			else
			{
				SendErrorResponse(response, "Fail to obtain openid login url");
			}
		}
	}
}
#endif