﻿#if NOT_UNITY3D
using NetCoreServer;

namespace Framework.Server
{
	public class HeadEndpoint : HttpEndpoint
	{
		public override string Method => "HEAD";
		public override string UrlStartsWith => "";

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			response.MakeHeadResponse();
		}
	}
}
#endif