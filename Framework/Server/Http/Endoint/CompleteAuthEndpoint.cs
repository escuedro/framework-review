﻿#if NOT_UNITY3D
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class CompleteAuthEndpoint: HttpEndpoint
	{
		private const string Name = "/complete_auth";

		public override string UrlStartsWith => Name;

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			string tempKey = request.Url.Remove(0, Name.Length);
			UserOpenIdClient userOpenIdClient = HttpServer.CompleteClient(tempKey);
			if (userOpenIdClient != null)
			{
				if (!string.IsNullOrEmpty(userOpenIdClient.AccessToken))
				{
					SendTextResponse(response, $"{userOpenIdClient.AccessToken};{userOpenIdClient.RefreshToken}",
							200);
				}
				else
				{
					SendTextResponse(response, "in progress", 425);
				}
			}
			else
			{
				SendTextResponse(response, "not found", 404);
			}
		}
	}
}
#endif