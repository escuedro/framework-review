﻿#if NOT_UNITY3D
using System;
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class RpcEndpoint : HttpEndpoint
	{
		public override string Method => "POST";
		private const string Name = "/rpc/";
		public override string UrlStartsWith => Name;

		private readonly GameServer _gameServer;

		public RpcEndpoint(GameServer gameServer)
		{
			_gameServer = gameServer;
		}

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			string token = GetHeader(request, AccessToken);
			if (string.IsNullOrEmpty(token))
			{
				SendErrorResponse(response, $"Not found '{AccessToken}' header");
				return;
			}
			UserOpenIdClient userOpenIdClient = HttpServer.GetByToken(token);

			if (userOpenIdClient == null)
			{
				try
				{
					userOpenIdClient = HttpServer.RestoreClientByToken(token);
				}
				catch (Exception)
				{
					SendTextResponse(response, "token expired", HttpErrorCodes.SessionExpired);
					return;
				}
			}
			if (userOpenIdClient.GameId == 0)
			{
				SendErrorResponse(response, "Not authorized");
				return;
			}
			if (!userOpenIdClient.ExpirationAccessToken.IsFuture)
			{
				SendTextResponse(response, "token expired", HttpErrorCodes.SessionExpired);
				return;
			}

			string rpcMethod = request.Url.Substring(Name.Length);
			Log.Trace($"Execute method {rpcMethod}");
			int bytesCount;
			byte[] responseBytes;
			try
			{
				responseBytes = _gameServer.ExecuteRpc(userOpenIdClient.GameId, userOpenIdClient.UserId, rpcMethod,
						request.BodyBytes,
						out bytesCount);
			}
			catch (ValidateException validateException)
			{
				SendErrorResponse(response, $"Validate: {validateException.Message}");
				return;
			}
			catch (Exception e)
			{
				Log.Exception(e);
				SendErrorResponse(response, $"Exception: {e.Message}");
				return;
			}
			SendResponse(response, responseBytes, bytesCount);
		}
	}
}
#endif