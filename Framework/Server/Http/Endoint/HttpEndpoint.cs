﻿#if NOT_UNITY3D
using System;
using NetCoreServer;

namespace Framework.Server
{
	public abstract class HttpEndpoint
	{
		public const string AccessToken = "AccessToken";
		public const string RefreshToken = "RefreshToken";

		public HttpsGameServer HttpServer;
		public virtual string Method => "GET";
		public abstract string UrlStartsWith { get; }

		public void FillResponse(HttpRequest request, HttpResponse response, SslSession session)
		{
			try
			{
				Handle(request, response, session);
			}
			catch (Exception e)
			{
				SendErrorResponse(response, e.Message);
			}
		}

		protected abstract void Handle(HttpRequest request, HttpResponse response, SslSession session);


		protected void SendResponse(HttpResponse response, byte[] buffer, int byteCount)
		{
			response.Clear();
			response.SetBegin(200);
			response.SetHeader("Content-Type", "application/octet-stream");
			AddCors(response);
			response.SetBody(buffer, byteCount);
		}

		protected void SendErrorResponse(HttpResponse response, string errorMessage)
		{
			SendTextResponse(response, errorMessage, 500);
		}

		protected void SendGetResponse(HttpResponse response, string message)
		{
			SendTextResponse(response, message, 200);
		}

		protected void SendRedirect(HttpResponse response, string url)
		{
			response.Clear();
			response.SetBegin(307); //Temporary Redirect
			response.SetHeader("Content-Type", "text/plain; charset=UTF-8");
			response.SetHeader("Location", url);
			AddCors(response);
			response.SetBody();
		}

		protected void SendTextResponse(HttpResponse response, string message, int code)
		{
			response.Clear();
			response.SetBegin(code);
			response.SetHeader("Content-Type", "text/plain; charset=UTF-8");
			AddCors(response);
			response.SetBody(message);
		}

		protected void SendHtml(HttpResponse response, string message, int code)
		{
			response.Clear();
			response.SetBegin(code);
			response.SetHeader("Content-Type", "text/html; charset=UTF-8");
			AddCors(response);
			response.SetBody($"<html><head></head><body>{message}</body>");
		}

		protected void SendOptions(HttpResponse response)
		{
			response.Clear();
			response.SetBegin(200);
			response.SetHeader("Allow", "GET,POST,OPTIONS");
			AddCors(response);
			response.SetBody();
		}

		private void AddCors(HttpResponse response)
		{
			response.SetHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
			response.SetHeader("Access-Control-Allow-Origin", "*");
			response.SetHeader("Access-Control-Allow-Headers", "*");
		}

		[CanBeNull]
		protected string GetHeader(HttpRequest request, string headerName)
		{
			for (int i = 0; i < request.Headers; i++)
			{
				(string, string) keyValue = request.Header(i);
				if (keyValue.Item1 == headerName)
				{
					return keyValue.Item2;
				}
			}
			return null;
		}
	}
}
#endif