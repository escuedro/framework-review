﻿#if NOT_UNITY3D
using Framework.Server.OpenId;
using NetCoreServer;

namespace Framework.Server
{
	public class OpenIdEndpoint: HttpEndpoint
	{
		public override string UrlStartsWith => "/open_id";

		protected override void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			UserOpenIdClient userOpenIdClient = HttpServer.GetClientBySessionAndDelete(session);
			if (userOpenIdClient != null)
			{
				userOpenIdClient.OnOpenIdCallback(request.Url);
				SendHtml(response , "Now you can back to game <script>setTimeout(function() {window.close()}, 3000);</script>", 200);
			}
		}
	}
}
#endif