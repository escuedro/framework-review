﻿#if NOT_UNITY3D
using System.Collections.Generic;
using NetCoreServer;

namespace Framework.Server
{
	public class EndpointLocator
	{
		private readonly Dictionary<string, List<HttpEndpoint>> _endpointsByMethod =
				new Dictionary<string, List<HttpEndpoint>>();
		private HttpEndpoint _defaultEndpoint;

		public void Register(HttpEndpoint endpoint)
		{
			string endpointMethod = endpoint.Method;
			if (!_endpointsByMethod.TryGetValue(endpointMethod, out List<HttpEndpoint> endpoints))
			{
				endpoints = new List<HttpEndpoint>();
				_endpointsByMethod[endpointMethod] = endpoints;
			}
			endpoints.Add(endpoint);
		}

		public void SetDefault(HttpEndpoint endpoint)
		{
			_defaultEndpoint = endpoint;
		}

		public void Handle(HttpRequest request, HttpResponse response, SslSession session)
		{
			if (_endpointsByMethod.TryGetValue(request.Method, out List<HttpEndpoint> endpoints))
			{
				foreach (HttpEndpoint endpoint in endpoints)
				{
					if (request.Url.StartsWith(endpoint.UrlStartsWith))
					{
						endpoint.FillResponse(request, response, session);
						return;
					}
				}
			}
			_defaultEndpoint.FillResponse(request, response, session);
		}
	}
}
#endif