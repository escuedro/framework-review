#if NOT_UNITY3D
using System.Net.Sockets;
using NetCoreServer;

namespace Framework.Server
{
	public class HttpsGameSession : HttpsSession
	{
		private readonly EndpointLocator _endpointLocator;

		public HttpsGameSession(HttpsServer server, EndpointLocator endpointLocator) : base(server)
		{
			_endpointLocator = endpointLocator;
		}

		protected override void OnReceivedRequest(HttpRequest request)
		{
			_endpointLocator.Handle(request, Response, this);
			SendResponseAsync(Response);
		}

		private void Send(HttpResponse response)
		{
			SendResponseAsync(response);
		}

		protected override void OnReceivedRequestError(HttpRequest request, string error)
		{
			Log.Info($"Request error: {error}");
		}

		protected override void OnError(SocketError error)
		{
			Log.Info($"HTTP session caught an error: {error}");
		}
	}
}
#endif