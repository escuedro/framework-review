#if NOT_UNITY3D
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using NetCoreServer;
using System.Net.Sockets;
using Framework.Server.DataBase;
using Framework.Server.OpenId;
using IdentityModel.Client;

namespace Framework.Server
{
	public class HttpsGameServer : HttpsServer
	{
		[Inject]
		public ServerConfig ServerConfig;
		[Inject]
		public DatabaseLoader DatabaseLoader;

		private readonly ConcurrentDictionary<string, UserOpenIdClient> _userOpenIdByClientKey =
				new ConcurrentDictionary<string, UserOpenIdClient>();
		private readonly ConcurrentDictionary<SslSession, UserOpenIdClient> _userOpenIdBySession =
				new ConcurrentDictionary<SslSession, UserOpenIdClient>();
		private readonly ConcurrentDictionary<string, UserOpenIdClient> _userOpenIdByAccessToken =
				new ConcurrentDictionary<string, UserOpenIdClient>();

		private readonly EndpointLocator _endpointLocator = new EndpointLocator();
		private DiscoveryCache _discoveryCache;

		public HttpsGameServer(SslContext sslContext, IPAddress address, int port) : base(sslContext,
				new IPEndPoint(address, port))
		{
		}

		[Inject]
		public void Inject()
		{
			_discoveryCache = new DiscoveryCache(ServerConfig.OpenIdAuthority);
		}

		public void Register(HttpEndpoint endpoint)
		{
			endpoint.HttpServer = this;
			_endpointLocator.Register(endpoint);
		}

		public void SetDefault(HttpEndpoint endpoint)
		{
			endpoint.HttpServer = this;
			_endpointLocator.SetDefault(endpoint);
		}

		[MustUseReturnValue]
		public UserOpenIdClient ResolveClientByTempKey(string tempKey, SslSession session)
		{
			if (!_userOpenIdByClientKey.TryGetValue(tempKey, out UserOpenIdClient result))
			{
				result = new UserOpenIdClient(ServerConfig, DatabaseLoader, _discoveryCache);
				_userOpenIdByClientKey.TryAdd(tempKey, result);
				_userOpenIdBySession.TryAdd(session, result);
			}
			return result;
		}

		[MustUseReturnValue]
		public UserOpenIdClient RestoreClientByToken(string accessToken)
		{
			var result = new UserOpenIdClient(ServerConfig, DatabaseLoader, _discoveryCache);
			result.TryRestore(accessToken);
			SetWithToken(result);
			return result;
		}

		[MustUseReturnValue]
		public UserOpenIdClient RestoreClientByRefreshToken(string refreshToken)
		{
			var result = new UserOpenIdClient(ServerConfig, DatabaseLoader, _discoveryCache);
			result.TryRefresh(refreshToken);
			SetWithToken(result);
			return result;
		}

		[CanBeNull]
		[MustUseReturnValue]
		public UserOpenIdClient CompleteClient(string tempKey)
		{
			if (_userOpenIdByClientKey.TryGetValue(tempKey, out UserOpenIdClient result))
			{
				if (!string.IsNullOrEmpty(result.AccessToken))
				{
					_userOpenIdByClientKey.TryRemove(new KeyValuePair<string, UserOpenIdClient>(tempKey, result));
					string accessToken = result.AccessToken;
					_userOpenIdByAccessToken.TryAdd(accessToken, result);
				}
			}
			return result;
		}

		[CanBeNull]
		public UserOpenIdClient GetByToken(string accessToken)
		{
			_userOpenIdByAccessToken.TryGetValue(accessToken, out UserOpenIdClient result);
			return result;
		}
		[CanBeNull]
		public UserOpenIdClient RemvoeByToken(string accessToken)
		{
			_userOpenIdByAccessToken.TryRemove(accessToken, out UserOpenIdClient result);
			return result;
		}

		public void UpdateToken(string oldAccessToken, UserOpenIdClient client)
		{
			_userOpenIdByAccessToken.TryRemove(new KeyValuePair<string, UserOpenIdClient>(oldAccessToken, client));
			_userOpenIdByAccessToken.TryAdd(client.AccessToken, client);
		}

		public void SetWithToken(UserOpenIdClient client)
		{
			_userOpenIdByAccessToken.TryAdd(client.AccessToken, client);
		}

		[CanBeNull]
		public UserOpenIdClient GetClientBySessionAndDelete(SslSession session)
		{
			_userOpenIdBySession.TryGetValue(session, out UserOpenIdClient result);
			if (result != null)
			{
				_userOpenIdBySession.TryRemove(new KeyValuePair<SslSession, UserOpenIdClient>(session, result));
			}
			return result;
		}

		protected override SslSession CreateSession()
		{
			return new HttpsGameSession(this, _endpointLocator);
		}


		protected override void OnError(SocketError error)
		{
			Log.Error($"HTTP session caught an error: {error}");
		}
	}
}
#endif