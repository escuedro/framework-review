#if NOT_UNITY3D
using Framework.Hasher;
using Framework.Serialization;

namespace Framework.Model
{
	[GenerateHash]
	[GenerateSerialize]
	public struct LocalIdentityRange
	{
		public string IdentityName;
		public long NextIdentity;
		public long MaxIdentity;

		public LocalIdentityRange(string identityName, long nextIdentity, long maxIdentity)
		{
			IdentityName = identityName;
			NextIdentity = nextIdentity;
			MaxIdentity = maxIdentity;
		}
	}
}
#endif