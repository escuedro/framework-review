#if NOT_UNITY3D
using System.Collections.Generic;
using Framework.Collections;

namespace Framework.Model.IdentityGenerator
{
	[Bind]
	public class LocalIdentityGenerator
	{
		private const int IdRange = 50;
		private readonly Dictionary<string, LocalIdentityRangeModel> _identityRanges =
				new Dictionary<string, LocalIdentityRangeModel>();
		private readonly IdentityRepository _identityRepository;

		[Inject]
		public LocalIdentityGenerator(IdentityRepository repository)
		{
			_identityRepository = repository;
		}

		public void Add(LocalIdentityRange localIdentityRange)
		{
			_identityRanges[localIdentityRange.IdentityName] = new LocalIdentityRangeModel(
					localIdentityRange.IdentityName, localIdentityRange.MaxIdentity, localIdentityRange.MaxIdentity);
		}

		public ulong GetIdentity(string identityName)
		{
			if (_identityRanges.TryGetValue(identityName, out LocalIdentityRangeModel localIdentityRange) &&
					localIdentityRange.NextIdentity <= localIdentityRange.MaxIdentity)
			{
				return (ulong)localIdentityRange.NextIdentity++;
			}
			LocalIdentityRangeModel identityRange = _identityRepository.GetNextRange(identityName, IdRange);
			_identityRanges[identityName] = identityRange;
			return (ulong)identityRange.NextIdentity++;
		}

		public ConstArray<LocalIdentityRange> Serialize()
		{
			LocalIdentityRange[] constArray = new LocalIdentityRange[_identityRanges.Count];
			int next = 0;
			foreach (LocalIdentityRangeModel localIdentityRange in _identityRanges.Values)
			{
				constArray[next++] = new LocalIdentityRange(localIdentityRange.IdentityName,
						localIdentityRange.NextIdentity, localIdentityRange.MaxIdentity);
			}
			return new ConstArray<LocalIdentityRange>(constArray);
		}
	}
}
#endif