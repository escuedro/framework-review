#if NOT_UNITY3D
using System.Collections.Generic;
using Framework.Server.DataBase;

namespace Framework.Model.IdentityGenerator
{
	public class IdentityRepository
	{
		[Inject]
		public DatabaseLoader DatabaseLoader;
		private readonly Dictionary<string, IdentityCounter> _counterByName = new Dictionary<string, IdentityCounter>();

		public LocalIdentityRangeModel GetNextRange(string identityName, int count)
		{
			lock (_counterByName)
			{
				bool insert = false;
				if (!_counterByName.TryGetValue(identityName, out IdentityCounter identityCounter))
				{
					identityCounter = DatabaseLoader.TryGetIdentityCounter(identityName);
					if (identityCounter == null)
					{
						identityCounter = new IdentityCounter { IdentityName = identityName, Counter = 1 };
						insert = true;
					}
					_counterByName.Add(identityName, identityCounter);
				}

				LocalIdentityRangeModel identityRange = new LocalIdentityRangeModel(identityName,
						identityCounter.Counter,
						identityCounter.Counter + count);
				identityCounter.Counter += count + 1;
				DatabaseLoader.UpdateIdentityCounter(identityCounter, insert);
				return identityRange;
			}
		}
	}
}
#endif