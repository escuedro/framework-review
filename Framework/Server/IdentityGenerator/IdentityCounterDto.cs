#if NOT_UNITY3D
using PetaPoco;

namespace Framework.Model.IdentityGenerator
{
	[ExplicitColumns]
	[PrimaryKey("identity_name", AutoIncrement = false)]
	public class IdentityCounter
	{
		[Column(Name = "identity_name")]
		public string IdentityName { get; set; }

		[Column(Name = "counter")]
		public long Counter { get; set; }
	}
}
#endif