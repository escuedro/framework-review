#if NOT_UNITY3D
using Framework.Hasher;
using Framework.Serialization;

namespace Framework.Model
{
	public class LocalIdentityRangeModel
	{
		public string IdentityName;
		public long NextIdentity;
		public long MaxIdentity;

		public LocalIdentityRangeModel(string identityName, long nextIdentity, long maxIdentity)
		{
			IdentityName = identityName;
			NextIdentity = nextIdentity;
			MaxIdentity = maxIdentity;
		}
	}
}
#endif