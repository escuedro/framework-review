#if NOT_UNITY3D
using System;

namespace Framework.Server.Marketplace
{
	public interface IMarketplaceGuidResolver
	{
		public Guid CreateGuid(ulong identity, string identityName, ulong owner);
		public IdentityWithOwner GetGameIdentity(Guid guid, string identityName);
		public void SetOwner(Guid marketGuid, ulong identity, string identityName, ulong newOwner);
	}
}
#endif