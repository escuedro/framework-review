#if NOT_UNITY3D
using System;
using Framework.Server.DataBase;

namespace Framework.Server.Marketplace
{
	public class MarketplaceGuidResolver : IMarketplaceGuidResolver
	{
		[Inject]
		public DatabaseLoader DatabaseLoader;

		public Guid CreateGuid(ulong identity, string identityName, ulong owner)
		{
			if (owner == 0)
			{
				throw new Exception("Zero owner");
			}
			if (string.IsNullOrEmpty(identityName))
			{
				throw new Exception("Empty identityName");
			}
			if (identity == 0)
			{
				throw new Exception("Identity is 0");
			}

			Guid characterGuid = Guid.NewGuid(); // TODO resolve in marketplace

			MarketplaceGuidToGameId marketplaceGuidToGameId = new MarketplaceGuidToGameId
			{
					GameId = (long)identity,
					IdentityName = identityName,
					MarketGuid = characterGuid.ToString(),
					GameOwner = (long)owner
			};
			DatabaseLoader.InsertMarketplaceGuid(marketplaceGuidToGameId);
			return characterGuid;
		}

		public void SetOwner(Guid marketGuid, ulong identity, string identityName, ulong newOwner)
		{
			if (marketGuid.Equals(Guid.Empty))
			{
				throw new Exception("Empty guid");
			}
			if (newOwner == 0)
			{
				throw new Exception("Zero owner");
			}
			if (string.IsNullOrEmpty(identityName))
			{
				throw new Exception("Empty identityName");
			}
			if (identity == 0)
			{
				throw new Exception("Identity is 0");
			}
			MarketplaceGuidToGameId marketplaceGuidToGameId = new MarketplaceGuidToGameId
			{
					GameId = (long)identity,
					IdentityName = identityName,
					MarketGuid = marketGuid.ToString(),
					GameOwner = (long)newOwner
			};
			DatabaseLoader.UpdateMarketplaceGuid(marketplaceGuidToGameId);
		}

		public IdentityWithOwner GetGameIdentity(Guid guid, string identityName)
		{
			if (string.IsNullOrEmpty(identityName))
			{
				throw new Exception("Empty identityName");
			}
			MarketplaceGuidToGameId marketplaceGuidToGameId = DatabaseLoader.TryResolveGuid(guid.ToString());
			if (marketplaceGuidToGameId == null)
			{
				throw new Exception("Not found identity");
			}
			if (marketplaceGuidToGameId.IdentityName == identityName)
			{
				throw new Exception(
						$"Identity found {marketplaceGuidToGameId.IdentityName} but expected {identityName}");
			}

			return new IdentityWithOwner
			{
					GameId = (ulong)marketplaceGuidToGameId.GameId,
					OwnerId = (ulong)marketplaceGuidToGameId.GameOwner
			};
		}
	}
}
#endif