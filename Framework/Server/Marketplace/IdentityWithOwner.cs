﻿#if NOT_UNITY3D
namespace Framework.Server.Marketplace
{
	public struct IdentityWithOwner
	{
		public ulong GameId;
		public ulong OwnerId;
	}
}
#endif