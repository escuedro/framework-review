#if NOT_UNITY3D
using PetaPoco;

namespace Framework.Server.Marketplace
{
	[ExplicitColumns]
	[PrimaryKey("market_guid", AutoIncrement = false)]
	public class MarketplaceGuidToGameId
	{
		[Column(Name = "market_guid")]
		public string MarketGuid { get; set; }

		[Column(Name = "identity_name")]
		public string IdentityName { get; set; }

		[Column(Name = "game_id")]
		public long GameId { get; set; }

		[Column(Name = "game_owner")]
		public long GameOwner { get; set; }
	}
}
#endif