#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Threading;
using Framework.RPC;
using Framework.Serialization;
using Framework.Server.DataBase;

namespace Framework.Server
{
	[Bind]
	public class GameServer
	{
		private readonly Dictionary<string, RpcCommandFactory> _rpcCommands =
				new Dictionary<string, RpcCommandFactory>();
		[Inject]
		public BaseSerializer Serializer;
		[Inject]
		public IUserStorage UserStorage;
		[Inject]
		public DatabaseLoader DatabaseLoader;

		private readonly ThreadLocal<BinarySerializer> _binarySerializer =
				new ThreadLocal<BinarySerializer>(() => new BinarySerializer());

		public void RegisterRpc<TCommand>() where TCommand : RpcCommand, new()
		{
			_rpcCommands.Add(typeof(TCommand).Name, new RpcCommandFactory<TCommand>());
		}

		[MustUseReturnValue]
		public byte[] ExecuteRpc(ulong userId, string userGuid, string rpcName, byte[] bodyBytes, out int bytesCount)
		{
			if (_rpcCommands.TryGetValue(rpcName, out RpcCommandFactory factory))
			{
				RpcCommand rpcCommand = factory.Create();
				BinarySerializer binarySerializer = _binarySerializer.Value;
				binarySerializer.Clear();
				rpcCommand.Input = new BinaryDeserializer(bodyBytes);
				rpcCommand.Output = binarySerializer;
				rpcCommand.Serializer = Serializer;

				using (UserContextLocker userContextLocker = new UserContextLocker(UserStorage))
				{
					UserContext userContext = userContextLocker.Lock(userId);
					userContext.Guid = userGuid;
					rpcCommand.UserContext = userContext;
					rpcCommand.UserContextLocker = userContextLocker;
					try
					{
						rpcCommand.Execute();
					}
					catch (ValidateException)
					{
						//Do not rollback because it did not broke model
						throw;
					}
					catch (Exception exception)
					{
						Log.Exception(exception);

						try
						{
							userContextLocker.RollbackAll();
						}
						catch (Exception rollbackException)
						{
							Log.Fatal(rollbackException);
							throw new Exception($"FATAL execute and rollback {rpcName}");
						}

						throw new Exception($"Fail to execute {rpcName}");
					}
					using DatabaseTransaction databaseTransaction = DatabaseLoader.StartTransaction();
					userContextLocker.SaveAll();
					databaseTransaction.Complete();

				}

				return binarySerializer.GetResult(out bytesCount);
			}
			else
			{
				throw new Exception($"Not found rpc {rpcName}");
			}
		}
	}
}
#endif