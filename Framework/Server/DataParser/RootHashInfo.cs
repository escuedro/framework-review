#if NOT_UNITY3D
using System;
using System.Collections.Generic;

namespace Framework.Server.DataParser
{
	[Bind]
	public class RootHashInfo
	{
		public readonly Dictionary<Type, int> HashInfo = new Dictionary<Type, int>();
	}
}
#endif