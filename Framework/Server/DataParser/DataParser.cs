#if NOT_UNITY3D
using System;
using Framework.Server.DataBase;
using System.Text.Json;
using Framework.Collections;
using Framework.Hasher;

namespace Framework.Server.DataParser
{
	public interface IDataParser
	{
		void Parse(IContainer container, IUserData data);
		void Save(IContainer container, IUserData data);
	}


	public abstract class DataParser<TDto> : IDataParser where TDto : IUserData
	{
		protected abstract void Parse(IContainer container, TDto data);
		protected abstract void SaveData(IContainer container, TDto data);

		public void Parse(IContainer container, IUserData data)
		{
			Parse(container, (TDto)data);
		}

		public void Save(IContainer container, IUserData data)
		{
			SaveData(container, (TDto)data);
		}
	}

	public class DataParser<TDto, TRootModel> : DataParser<TDto> where TDto : IUserData, new()
	{
		private readonly IModelLoader<TRootModel> _modelLoader;
		private readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions
		{
				IncludeFields = true,
				IgnoreReadOnlyProperties = true,
				Converters = { new ConstArrayConverterFactory() }
		};

		public DataParser(IModelLoader<TRootModel> modelLoader)
		{
			_modelLoader = modelLoader;
		}

		protected override void Parse(IContainer container, TDto data)
		{
			TRootModel rootModel = JsonSerializer.Deserialize<TRootModel>(data.Data, _jsonOptions);
			RootHashInfo rootHashInfo = container.Resolve<RootHashInfo>();
			int hash = container.Resolve<BaseHasher>().GetHash(rootModel);
			rootHashInfo.HashInfo[typeof(TDto)] = hash;
			_modelLoader.Load(container, rootModel);
		}

		protected override void SaveData(IContainer container, TDto dto)
		{
			TRootModel rootModel = _modelLoader.Save(container);
			int hash = container.Resolve<BaseHasher>().GetHash(rootModel);
			RootHashInfo rootHashInfo = container.Resolve<RootHashInfo>();

			Type type = typeof(TDto);
			if (!rootHashInfo.HashInfo.TryGetValue(type, out int oldHash) || oldHash != hash)
			{
				dto.Data = JsonSerializer.Serialize(rootModel, _jsonOptions);
				rootHashInfo.HashInfo[type] = hash;
			}
		}
	}
}
#endif