#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using Framework.Server.DataBase;

namespace Framework.Server.DataParser
{
	[Bind]
	public class DbDataParser
	{
		private readonly Dictionary<Type, IDataParser> _parserByDataType = new Dictionary<Type, IDataParser>();

		public void RegisterParser<TData>(DataParser<TData> dataParser) where TData : IUserData
		{
			_parserByDataType.Add(typeof(TData), dataParser);
		}


		public void Parse(IContainer container, IUserData data)
		{
			_parserByDataType[data.GetType()].Parse(container, data);
		}

		public void Save<TData>(IContainer container, TData data) where TData: IUserData
		{
			Type type = typeof(TData);
			_parserByDataType[type].Save(container, data);
		}
	}
}
#endif