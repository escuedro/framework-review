﻿using Framework.Hasher;
using Framework.Serialization;

namespace Framework
{
    [GenerateSerialize]
    [GenerateHash]
    [System.Serializable]
    public struct TranslationKey
    {
        public string Value;

        public TranslationKey(string value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}