#if !NOT_UNITY3D
using System.Collections.Generic;
using Framework.Client;
using SAS.Model.Time;

namespace Framework
{
	[Bind(BindTo = typeof(ITaskScheduler))]
	internal class TaskSchedulerClient : TaskShedulerBase, IUpdatable
	{
		private readonly List<TaskEntry> _jobs = new List<TaskEntry>();
		private readonly List<TaskEntry> _jobsIterating = new List<TaskEntry>();

		[Inject]
		public TaskSchedulerClient(IUpdateManager updateManager)
		{
			updateManager.StartUpdate(this, UpdateType.BySecond);
		}

		public override void Schedule(IJob job, TimeDuration period)
		{
			_jobs.Add(new TaskEntry(job, period));
		}

		public override void UnSchedule(IJob job)
		{
			for (int i = _jobs.Count - 1; i >= 0; i--)
			{
				TaskEntry entry = _jobs[i];
				if (entry.Job == job)
				{
					_jobs.RemoveAt(i);
					break;
				}
			}
		}

		public void Update()
		{
			_jobsIterating.Clear();
			_jobsIterating.AddRange(_jobs);
			foreach (TaskEntry entry in _jobsIterating)
			{
				if (!entry.NextUpdate.IsFuture)
				{
					entry.NextUpdate = TimePoint.Now + entry.RepeatDuration;
					ExecuteJob(entry.Job);
				}
			}
		}

		private class TaskEntry
		{
			public readonly IJob Job;
			public readonly TimeDuration RepeatDuration;
			public TimePoint NextUpdate;

			public TaskEntry(IJob job, TimeDuration repeatDuration)
			{
				Job = job;
				RepeatDuration = repeatDuration;
				NextUpdate = TimePoint.Now + RepeatDuration;
			}
		}
	}
}
#endif