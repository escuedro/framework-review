using System;
using SAS.Model.Time;

namespace Framework
{
	internal abstract class TaskShedulerBase : ITaskScheduler
	{
		protected void ExecuteJob(IJob job)
		{
			try
			{
				job.Action();
			}
			catch (Exception e)
			{
				Log.Fatal(e);
				UnSchedule(job);
			}
		}

		public abstract void Schedule(IJob job, TimeDuration period);

		public abstract void UnSchedule(IJob job);
	}
}