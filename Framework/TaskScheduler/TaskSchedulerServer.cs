﻿#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Threading;
using SAS.Model.Time;

namespace Framework
{
	[Bind(BindTo = typeof(ITaskScheduler))]
	internal class TaskSchedulerServer : TaskShedulerBase
	{
		private readonly Dictionary<IJob, Timer> _jobsWithTimers = new Dictionary<IJob, Timer>();

		public override void Schedule(IJob job, TimeDuration period)
		{
			ulong nowTotalMilliseconds = TimePoint.Now.Milliseconds;
			int dueTimeMilliseconds = (int)(period.Milliseconds - nowTotalMilliseconds % period.Milliseconds);

			TimerCallback timerCallback = new TimerCallback(_ => { ExecuteJob(job); });
			TimeSpan timeSpan = TimeSpan.FromSeconds(period.Seconds);
			Timer timer = new Timer(timerCallback, null, TimeSpan.FromMilliseconds(dueTimeMilliseconds), timeSpan);
			lock (_jobsWithTimers)
			{
				_jobsWithTimers.Add(job, timer);
			}
		}

		public override void UnSchedule(IJob job)
		{
			Timer timer;
			lock (_jobsWithTimers)
			{
				if (_jobsWithTimers.TryGetValue(job, out timer))
				{
					_jobsWithTimers.Remove(job);
				}
			}

			timer?.Dispose();
		}
	}
}
#endif