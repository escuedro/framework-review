﻿namespace Framework
{
	public interface IJob
	{
		public void Action();
	}
}