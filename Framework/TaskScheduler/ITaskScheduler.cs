﻿using SAS.Model.Time;

namespace Framework
{
	public interface ITaskScheduler
	{
		public void Schedule(IJob job, TimeDuration period);

		public void UnSchedule(IJob job);
	}
}