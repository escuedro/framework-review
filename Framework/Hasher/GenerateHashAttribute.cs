using System;

namespace Framework.Hasher
{
	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class GenerateHashAttribute : Attribute
	{
	}
}