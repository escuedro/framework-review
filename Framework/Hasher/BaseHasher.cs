#if NOT_UNITY3D
using System;

namespace Framework.Hasher
{
	public class BaseHasher
	{
		public virtual int GetHash<T>(T data)
		{
			throw new Exception("Please run hash generator");
		}
	}
}
#endif