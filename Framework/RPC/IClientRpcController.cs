#if !NOT_UNITY3D
using Framework.Futures;

namespace Framework.RPC
{
	public interface IClientRpcController
	{
		ClientAuth ClientAuth { get; }

		IFuture Execute(RpcCommand command);
		void SetServerUrl(string url);
		void SetEnabled(bool enabled);
	}
}
#endif