namespace Framework.RPC
{
	public abstract class RpcCommandFactory
	{
		public abstract RpcCommand Create();
	}

	public sealed class RpcCommandFactory<TCommand> : RpcCommandFactory where TCommand : RpcCommand, new()
	{
		public override RpcCommand Create()
		{
			return new TCommand();
		}
	}
}