﻿#if !NOT_UNITY3D
using UnityEngine.Networking;

namespace Framework.RPC
{
	public class EmptyCertificateHandler : CertificateHandler
	{
		public static  EmptyCertificateHandler Instance => new EmptyCertificateHandler();

		protected override bool ValidateCertificate(byte[] certificateData)
		{
			return true;
		}

		public EmptyCertificateHandler()
		{
			System.Net.ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertifications;
		}

		public bool AcceptAllCertifications(object sender,
				System.Security.Cryptography.X509Certificates.X509Certificate certification,
				System.Security.Cryptography.X509Certificates.X509Chain chain,
				System.Net.Security.SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}
	}
}
#endif