#if !NOT_UNITY3D
using Framework.Futures;
using UnityEngine.Networking;

namespace Framework.RPC
{
	internal struct ClientRpcRequest
	{
		public string CommandName;
		public RpcCommand Command;
		public Promise Promise;
		public byte[] Request;
		public UnityWebRequestAsyncOperation RequestOperation;
	}
}
#endif