#if !NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Client;
using Framework.Futures;
using Framework.Pools;
using Framework.Serialization;
using UnityEngine.Networking;

namespace Framework.RPC
{
	[Bind(BindTo = typeof(IClientRpcController))]
	internal class ClientRpcController : IClientRpcController, IUpdatable, IFutureHandler
	{
		private readonly Dictionary<Type, object[]> _urlParamsByType = new Dictionary<Type, object[]>();
		private readonly Dictionary<Type, string> _commandNameByType = new Dictionary<Type, string>();
		private readonly MethodInfo _setUrlMethod;
		private readonly BaseSerializer _serializer;
		private readonly IEngine _engine;
		private readonly IResolver _resolver;
		private readonly BinarySerializer _binarySerializer = new BinarySerializer();
		private string _serverUrl = "https://localhost:5000/";
		private readonly List<ClientRpcRequest> _activeRequests = new List<ClientRpcRequest>();
		private readonly List<ClientRpcRequest> _repeatAfterAuth = new List<ClientRpcRequest>();
		private ClientAuth _clientAuth;
		private bool _enabled = true;

		public ClientAuth ClientAuth => _clientAuth;

		[Inject]
		public ClientRpcController(IUpdateManager updateManager,
				BaseSerializer baseSerializer,
				IEngine engine,
				IResolver resolver)
		{
			_setUrlMethod =
					typeof(UnityWebRequest).GetMethod("InternalSetUrl", BindingFlags.NonPublic | BindingFlags.Instance);
			_serializer = baseSerializer;
			_engine = engine;
			_resolver = resolver;
			_clientAuth = new ClientAuth(_serverUrl, _setUrlMethod, engine);
			updateManager.StartUpdate(this, UpdateType.BySecond);
		}

		public void SetEnabled(bool enabled)
		{
			_enabled = enabled;
		}

		public void SetServerUrl(string url)
		{
			_serverUrl = url;
			_clientAuth = new ClientAuth(_serverUrl, _setUrlMethod, _engine);
			_urlParamsByType.Clear();
		}

		public IFuture Execute(RpcCommand command)
		{
			if (!_enabled)
			{
				return FuturePool.PullRejected(new Exception("RPC is disabled"));
			}

			command.Serializer = _serializer;
			command.Output = _binarySerializer;
			command.Resolver = _resolver;
			command.SerializeRequest();
			byte[] request = _binarySerializer.GetResult(out int bytesCount);

			_binarySerializer.Clear();
			Promise promise = PromisePool.Pull();
			SendCommand(command, request, bytesCount, promise);
			return FuturePool.Pull(promise);
		}

		private void SendCommand(RpcCommand command, byte[] requestBuffer, int bytesCount, Promise promise)
		{
			if (_clientAuth.AccessToken == null)
			{
				promise.Reject(new Exception("not ready, authorize first"));
				return;
			}
			Type commandType = command.GetType();
			_commandNameByType.TryGetValue(commandType, out string commandName);
			if (!_urlParamsByType.TryGetValue(commandType, out object[] urlParameters))
			{
				commandName = command.GetName();
				_commandNameByType[commandType] = commandName;
				string serverUrl = _serverUrl + "rpc/" + commandName;
				urlParameters = new object[] { serverUrl };
				_urlParamsByType[commandType] = urlParameters;
			}

			UnityWebRequest webRequest = new UnityWebRequest();
			webRequest.downloadHandler = new DownloadHandlerBuffer();
			webRequest.certificateHandler = EmptyCertificateHandler.Instance;
			byte[] request = BufferPool.Pull(bytesCount);
			if (bytesCount > 0)
			{
				Array.Copy(requestBuffer, request, bytesCount);
				webRequest.uploadHandler = new UploadHandlerRaw(request);
			}

			_setUrlMethod.Invoke(webRequest, urlParameters);

			webRequest.SetRequestHeader("AccessToken", _clientAuth.AccessToken);
			webRequest.method = UnityWebRequest.kHttpVerbPOST;
			webRequest.timeout = 10;
			webRequest.useHttpContinue = true;
			UnityWebRequestAsyncOperation requestOperation = webRequest.SendWebRequest();
			_activeRequests.Add(new ClientRpcRequest
			{
					Command = command, Promise = promise, CommandName = commandName,
					RequestOperation = requestOperation, Request = request
			});
		}

		public void Update()
		{
			for (int i = _activeRequests.Count - 1; i >= 0; i--)
			{
				ClientRpcRequest activeRequest = _activeRequests[i];
				if (activeRequest.RequestOperation.isDone)
				{
					_activeRequests.RemoveAt(i);
					OnComplete(activeRequest);
				}
			}
		}

		private void OnComplete(ClientRpcRequest rpcRequest)
		{
			string commandName = rpcRequest.CommandName;

			UnityWebRequest webRequest = rpcRequest.RequestOperation.webRequest;
			Promise promise = rpcRequest.Promise;
			Exception exception;
			switch (webRequest.result)
			{
				case UnityWebRequest.Result.ConnectionError:
					exception = new Exception(
							$"Command {commandName} ConnectionError: code {webRequest.responseCode} {webRequest.error}");
					promise.Reject(exception);
					rpcRequest.Command.ResponseDelegate?.Invoke(exception);
					break;
				case UnityWebRequest.Result.ProtocolError:
					if (webRequest.responseCode == HttpErrorCodes.SessionExpired)
					{
						Log.Info("token expired start refresh");
						_repeatAfterAuth.Add(rpcRequest);
						ClientAuth.TryRefreshToken().Finally(this);
						return;
					}
					string error = webRequest.downloadHandler.text;
					if (string.IsNullOrEmpty(error))
					{
						error = webRequest.error;
					}
					exception = new Exception(
							$"Command {commandName} ProtocolError: code {webRequest.responseCode} {error}");
					promise.Reject(exception);
					rpcRequest.Command.ResponseDelegate?.Invoke(exception);
					break;
				case UnityWebRequest.Result.DataProcessingError:
					exception = new Exception(
							$"Command {commandName} DataProcessingError: code {webRequest.responseCode} {webRequest.error}");
					promise.Reject(exception);
					rpcRequest.Command.ResponseDelegate?.Invoke(exception);
					break;
				case UnityWebRequest.Result.Success:
					byte[] response = webRequest.downloadHandler.data;
					OnResponse(rpcRequest.Command, response, promise);
					break;
			}
			BufferPool.Release(rpcRequest.Request);
		}

		private void OnResponse(RpcCommand command, byte[] response, Promise promise)
		{
			command.Input = new BinaryDeserializer(response);
			try
			{
				command.DeserializeResponse();
				if (!command.Input.IsReadToEnd())
				{
					throw new Exception("Protocol mismatch, rebuild generated code");
				}
			}
			catch (Exception exception)
			{
				Log.Exception(exception);
				promise.Reject(exception);
				promise.Reset();
				command.ResponseDelegate?.Invoke(exception);
				PromisePool.Release(promise);
				return;
			}
			promise.Resolve();
			promise.Reset();
			PromisePool.Release(promise);
		}

		public void HandleFuture(IFuture future)
		{
			for (int i = _repeatAfterAuth.Count - 1; i >= 0; i--)
			{
				ClientRpcRequest rpcRequest = _repeatAfterAuth[i];
				if (future.IsRejected)
				{
					Log.Info($"cancel '{rpcRequest.CommandName}' after failed update token {future.Reason.Message}");
					rpcRequest.Promise.Reject(future.Reason);
				}
				else
				{
					Log.Info($"resend '{rpcRequest.CommandName}' after success update token");
					SendCommand(rpcRequest.Command, rpcRequest.Request, rpcRequest.Request.Length, rpcRequest.Promise);
					BufferPool.Release(rpcRequest.Request);
				}
				_repeatAfterAuth.RemoveAt(i);
			}
		}
	}
}
#endif