using System;
using System.Reflection;
using Framework.Futures;

namespace Framework.RPC
{
	public static class RpcExtension
	{
		public static IFuture<TResponse> OnResponse<TResponse>(this RpcCommand command)
		{
#if UNITY_EDITOR
			//todo runtime error because of invalid TResponse
			// if (command.GetType().GenericTypeArguments[1] != typeof(TResponse))
			// {
			// 	throw new Exception($"command {command.GetName()} has no response {typeof(TResponse).FullName}");
			// }
#endif
			var responseDelegate = new Promise<TResponse>();
			command.ResponseDelegate = responseOrException =>
			{
				if(responseOrException is TResponse response)
				{
					responseDelegate.Resolve(response);
				}
				else if(responseOrException is Exception exception)
				{
					responseDelegate.Reject(exception);
				}
				else
				{
					responseDelegate.Reject(new Exception($"ResponseDelegate is {responseOrException?.GetType().Name??"NULL"}"));
				}
			};
			return new Future<TResponse>(responseDelegate);
		}
	}
}