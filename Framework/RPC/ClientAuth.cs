#if !NOT_UNITY3D
using System;
using System.Collections;
using System.Reflection;
using Framework.Futures;
using SAS.Model.Time;
using UnityEngine;
using UnityEngine.Networking;

namespace Framework.RPC
{
	public class ClientAuth
	{
		private const string AccessTokenKey = "ClientAuth.AccessToken";
		private const string RefreshTokenKey = "ClientAuth.RefreshToken";

		private readonly MethodInfo _setUrlMethod;
		private readonly IEngine _engine;
		private readonly string _serverUrl;
		private readonly string _tempKey;
		private string _accessToken;
		private string _refreshToken;

		public string AuthUrl => $"{_serverUrl}auth/{_tempKey}";
		public string LogoutUrl => $"{_serverUrl}logout/{AccessToken}";

		public string AccessToken
		{
			get => _accessToken;
			private set
			{
				_accessToken = value;
				PlayerPrefs.SetString(AccessTokenKey, _accessToken);
			}
		}

		private string RefreshToken
		{
			get => _refreshToken;
			set
			{
				_refreshToken = value;
				PlayerPrefs.SetString(RefreshTokenKey, _refreshToken);
			}
		}

		public ClientAuth(string serverUrl, MethodInfo setUrlMethod, IEngine engine)
		{
			_serverUrl = serverUrl;
			_setUrlMethod = setUrlMethod;
			_engine = engine;
			int randomInt = UnityEngine.Random.Range(0, int.MaxValue);
			_tempKey = $"{randomInt.ToString()}_{TimePoint.Now.Milliseconds.ToString()}";
			_accessToken = PlayerPrefs.GetString(AccessTokenKey, null);
			_refreshToken = PlayerPrefs.GetString(RefreshTokenKey, null);
		}

		public IFuture TryRefreshToken()
		{
			if (string.IsNullOrEmpty(_refreshToken))
			{
				return FuturePool.PullRejected(new Exception("No stored refresh token"));
			}
			Promise promise = PromisePool.Pull();
			_engine.StartCoroutine(RefreshTokenInternal(promise));
			return new Future(promise);
		}

		public IFuture TryLogout()
		{
			if (string.IsNullOrEmpty(_refreshToken))
			{
				return FuturePool.PullRejected(new Exception("No stored refresh token"));
			}
			Promise promise = PromisePool.Pull();
			_engine.StartCoroutine(LogoutTokenInternal(promise));
			return new Future(promise);
		}

		public void ClearStoredTokens()
		{
			AccessToken = "";
			RefreshToken = "";
			PlayerPrefs.Save();
		}

		public IFuture Authorize()
		{
			Promise promise = PromisePool.Pull();
			_engine.StartCoroutine(AuthorizeInternal(promise));
			return new Future(promise);
		}

		private IEnumerator AuthorizeInternal(Promise promise)
		{
			object[] urlParameters = { $"{_serverUrl}complete_auth/{_tempKey}" };
			int triesLeft = 20;
			do
			{
				triesLeft--;
				yield return new WaitForSeconds(2f);

				UnityWebRequest webRequest = new UnityWebRequest();
				webRequest.downloadHandler = new DownloadHandlerBuffer();
				webRequest.certificateHandler = EmptyCertificateHandler.Instance;
				_setUrlMethod.Invoke(webRequest, urlParameters);

				webRequest.method = UnityWebRequest.kHttpVerbGET;
				webRequest.timeout = 10;
				webRequest.useHttpContinue = true;
				webRequest.redirectLimit = 20;
				UnityWebRequestAsyncOperation requestOperation = webRequest.SendWebRequest();
				Log.Info("try receive token");
				yield return requestOperation;
				switch (webRequest.result)
				{
					case UnityWebRequest.Result.ConnectionError:
						if (triesLeft == 0)
						{
							promise.Reject(
									new Exception(
											$"Authorize ConnectionError: code {webRequest.responseCode} {webRequest.error}"));
						}
						continue;
					case UnityWebRequest.Result.ProtocolError:
						string error = webRequest.downloadHandler.text;
						if (string.IsNullOrEmpty(error))
						{
							error = webRequest.error;
						}
						if (triesLeft == 0)
						{
							promise.Reject(
									new Exception(
											$"Authorize ProtocolError: code {webRequest.responseCode} {error}"));
						}
						continue;
					case UnityWebRequest.Result.DataProcessingError:
						if (triesLeft == 0)
						{
							promise.Reject(
									new Exception(
											$"Authorize DataProcessingError: code {webRequest.responseCode} {webRequest.error}"));
						}
						continue;
					case UnityWebRequest.Result.Success:
						ParseTokens(webRequest.downloadHandler.text, promise);
						break;
				}
			}
			while (promise.IsPending);
		}

		private IEnumerator RefreshTokenInternal(Promise promise)
		{
			object[] urlParameters = { $"{_serverUrl}refresh_token" };
			yield return new WaitForSeconds(1f);

			UnityWebRequest webRequest = new UnityWebRequest();
			webRequest.downloadHandler = new DownloadHandlerBuffer();
			webRequest.certificateHandler = EmptyCertificateHandler.Instance;
			_setUrlMethod.Invoke(webRequest, urlParameters);

			webRequest.method = UnityWebRequest.kHttpVerbGET;
			webRequest.timeout = 10;
			webRequest.useHttpContinue = true;
			webRequest.redirectLimit = 20;
			webRequest.SetRequestHeader("AccessToken", _accessToken);
			webRequest.SetRequestHeader("RefreshToken", _refreshToken);
			UnityWebRequestAsyncOperation requestOperation = webRequest.SendWebRequest();
			yield return requestOperation;
			switch (webRequest.result)
			{
				case UnityWebRequest.Result.ConnectionError:
					promise.Reject(
							new Exception(
									$"Authorize ConnectionError: code {webRequest.responseCode} {webRequest.error}"));
					break;
				case UnityWebRequest.Result.ProtocolError:
					string error = webRequest.downloadHandler.text;
					if (string.IsNullOrEmpty(error))
					{
						error = webRequest.error;
					}

					promise.Reject(
							new Exception(
									$"Authorize ProtocolError: code {webRequest.responseCode} {error}"));

					break;
				case UnityWebRequest.Result.DataProcessingError:
					promise.Reject(
							new Exception(
									$"Authorize DataProcessingError: code {webRequest.responseCode} {webRequest.error}"));
					break;
				case UnityWebRequest.Result.Success:
					ParseTokens(webRequest.downloadHandler.text, promise);
					break;
			}
		}

		private IEnumerator LogoutTokenInternal(Promise promise)
		{
			object[] urlParameters = { $"{_serverUrl}logout" };
			yield return new WaitForSeconds(1f);

			UnityWebRequest webRequest = new UnityWebRequest();
			webRequest.downloadHandler = new DownloadHandlerBuffer();
			webRequest.certificateHandler = EmptyCertificateHandler.Instance;
			_setUrlMethod.Invoke(webRequest, urlParameters);

			webRequest.method = UnityWebRequest.kHttpVerbGET;
			webRequest.timeout = 10;
			webRequest.useHttpContinue = true;
			webRequest.redirectLimit = 20;
			webRequest.SetRequestHeader("AccessToken", _accessToken);
			webRequest.SetRequestHeader("RefreshToken", _refreshToken);
			UnityWebRequestAsyncOperation requestOperation = webRequest.SendWebRequest();
			yield return requestOperation;
			switch (webRequest.result)
			{
				case UnityWebRequest.Result.ConnectionError:
					promise.Reject(
							new Exception(
									$"Authorize ConnectionError: code {webRequest.responseCode} {webRequest.error}"));
					break;
				case UnityWebRequest.Result.ProtocolError:
					string error = webRequest.downloadHandler.text;
					if (string.IsNullOrEmpty(error))
					{
						error = webRequest.error;
					}

					promise.Reject(
							new Exception(
									$"Authorize ProtocolError: code {webRequest.responseCode} {error}"));

					break;
				case UnityWebRequest.Result.DataProcessingError:
					promise.Reject(
							new Exception(
									$"Authorize DataProcessingError: code {webRequest.responseCode} {webRequest.error}"));
					break;
				case UnityWebRequest.Result.Success:
					promise.Resolve();
					break;
			}
		}

		private void ParseTokens(string result, Promise promise)
		{
			Log.Info("Access token received");
			string[] tokens = result.Split(';');
			if (tokens.Length != 2)
			{
				promise.Reject(new Exception("cant parse tokens"));
				return;
			}
			AccessToken = tokens[0];
			RefreshToken = tokens[1];
			if (string.IsNullOrEmpty(AccessToken) || string.IsNullOrEmpty(RefreshToken))
			{
				promise.Reject(new Exception("tokens are empty"));
				return;
			}
			promise.Resolve();
		}
	}
}
#endif