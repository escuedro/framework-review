using System;
using Framework.Serialization;

namespace Framework.RPC
{
	public abstract class RpcCommand : Command
	{
		internal Action<object> ResponseDelegate;
		public IBinaryDeserializer Input;
		public IBinarySerializer Output;
		public BaseSerializer Serializer;

		public string GetName()
		{
			return $"{GetType().Name}";
		}
#if NOT_UNITY3D
		public Server.UserContext UserContext;
		public Server.UserContextLocker UserContextLocker;
		public abstract void Execute();
		protected override T Get<T>()
		{
			return UserContext.Container.Resolve<T>();
		}
#else
		public abstract void SerializeRequest();
		public abstract void DeserializeResponse();
#endif
	}

	public abstract class RpcCommand<TRequest, TResponse> : RpcCommand
	{
#if NOT_UNITY3D
		public override void Execute()
		{
			Serializer.Deserialize(Input, out TRequest request);
			TResponse response = Execute(request);
			Serializer.Serialize(Output, response);
		}
		protected abstract TResponse Execute(TRequest request);
#else

		public override void SerializeRequest()
		{
			TRequest request = GetRequest();
			Serializer.Serialize(Output, request);
		}

		public override void DeserializeResponse()
		{
			Serializer.Deserialize(Input, out TResponse response);
			OnResponse(response);
			ResponseDelegate?.Invoke(response);
		}

		protected abstract TRequest GetRequest();
		protected abstract void OnResponse(TResponse response);
#endif
	}
}