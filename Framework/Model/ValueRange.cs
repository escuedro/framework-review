using System;

namespace Framework.Model
{
	[Serializable]
	public struct ValueRange
	{
		public int Min;
		public int Max;
	}

	[Serializable]
	public struct ValueRangeFloat
	{
		public float Min;
		public float Max;
	}
}