using System;
using Framework;

namespace SAS.Model.Time
{
	public readonly struct TimeDuration
	{
		private readonly ulong _milliseconds;

		public static TimeDuration Day => new TimeDuration(24 * Hour._milliseconds);

		public static TimeDuration Hour => new TimeDuration(60 * Minute._milliseconds);

		public bool IsZero => _milliseconds == 0;

		public static TimeDuration Millisecond => new TimeDuration(1);

		public ulong Milliseconds => _milliseconds;

		public static TimeDuration Minute => new TimeDuration(60 * Second._milliseconds);

		public static TimeDuration Month => new TimeDuration(30 * Day._milliseconds);

		public static TimeDuration Second => new TimeDuration(1000);

		public uint Seconds => (uint)(_milliseconds / Second._milliseconds);

		public static TimeDuration Week => new TimeDuration(7 * Day._milliseconds);

		public static TimeDuration Year => new TimeDuration(365 * Day._milliseconds);

		public static TimeDuration Zero => new TimeDuration(0);

		internal TimeDuration(ulong milliseconds)
		{
			_milliseconds = milliseconds;
		}

		[Pure]
		public static bool ApproximatelyEquals(TimeDuration left, TimeDuration right)
		{
			return Distance(left, right) < Second;
		}

		public static TimeDuration CreateByMilliseconds(ulong milliseconds)
		{
			return new TimeDuration(milliseconds);
		}

		public static TimeDuration CreateBySeconds(uint seconds)
		{
			return new TimeDuration(seconds * Second._milliseconds);
		}

		public static TimeDuration CreateBySeconds(float seconds)
		{
			return new TimeDuration((ulong)(seconds * Second._milliseconds));
		}

		[Pure]
		public TimeDuration Distance(TimeDuration other)
		{
			return Distance(this, other);
		}

		[Pure]
		public static TimeDuration Distance(TimeDuration left, TimeDuration right)
		{
			if (left > right)
			{
				return left - right;
			}
			if (right <= left)
			{
				return Zero;
			}
			return right - left;
		}

		[Pure]
		public bool Equals(TimeDuration other)
		{
			return _milliseconds == other._milliseconds;
		}

		[Pure]
		public override bool Equals(object other)
		{
			if (!(other is TimeDuration))
			{
				return false;
			}
			return Equals((TimeDuration)other);
		}

		[Pure]
		public static bool Equals(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds == right._milliseconds;
		}

		[Pure]
		public override int GetHashCode()
		{
			return _milliseconds.GetHashCode();
		}

		[Pure]
		public static TimeDuration Max(TimeDuration left, TimeDuration right)
		{
			if (right._milliseconds <= left._milliseconds)
			{
				return left;
			}
			return right;
		}

		[Pure]
		public static TimeDuration Min(TimeDuration left, TimeDuration right)
		{
			if (right._milliseconds >= left._milliseconds)
			{
				return left;
			}
			return right;
		}

		public static TimeDuration operator +(TimeDuration left, TimeDuration right)
		{
			return new TimeDuration(left._milliseconds + right._milliseconds);
		}

		public static TimeDuration operator /(TimeDuration duration, double value)
		{
			return new TimeDuration((ulong)((float)duration._milliseconds / value));
		}

		public static double operator /(TimeDuration left, TimeDuration right)
		{
			if (right._milliseconds == 0)
			{
				throw new DivideByZeroException();
			}
			return (float)left._milliseconds / (double)((float)right._milliseconds);
		}

		public static bool operator ==(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds == right._milliseconds;
		}

		public static bool operator >(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds > right._milliseconds;
		}

		public static bool operator >=(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds >= right._milliseconds;
		}

		public static bool operator !=(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds != right._milliseconds;
		}

		public static bool operator <(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds < right._milliseconds;
		}

		public static bool operator <=(TimeDuration left, TimeDuration right)
		{
			return left._milliseconds <= right._milliseconds;
		}

		public static TimeDuration operator *(TimeDuration duration, double value)
		{
			return new TimeDuration((ulong)((float)duration._milliseconds * value));
		}

		public static TimeDuration operator *(double value, TimeDuration duration)
		{
			return new TimeDuration((ulong)((float)duration._milliseconds * value));
		}

		public static TimeDuration operator -(TimeDuration left, TimeDuration right)
		{
			if (right >= left)
			{
				return Zero;
			}
			return new TimeDuration(left._milliseconds - right._milliseconds);
		}

		[Pure]
		public override string ToString()
		{
			return _milliseconds.ToString();
		}
	}
}