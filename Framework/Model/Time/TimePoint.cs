using System;
using Framework;

namespace SAS.Model.Time
{
	public readonly struct TimePoint
	{
		internal static readonly DateTime Epoch = new DateTime(2021, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		private readonly ulong _milliseconds;

		private TimePoint(ulong milliseconds)
		{
			_milliseconds = milliseconds;
		}

		public static TimePoint CreateByMilliseconds(ulong milliseconds)
		{
			return new TimePoint(milliseconds);
		}

		public static TimePoint CreateByUtc(DateTime utcDate)
		{
			return new TimePoint((ulong)utcDate.Subtract(Epoch).TotalMilliseconds);
		}

		public ulong Milliseconds => _milliseconds;
		public uint Seconds => (uint)(_milliseconds / TimeDuration.Second.Milliseconds);

		public static TimePoint CreateBySeconds(uint seconds)
		{
			return new TimePoint(seconds * TimeDuration.Second.Milliseconds);
		}

		public static TimePoint Now => Date.GetInstance().Now;

		public static TimePoint Zero => new TimePoint(0);

		public bool IsFuture => Now._milliseconds < _milliseconds;
		public DateTime DateTime => Epoch.AddMilliseconds(_milliseconds);

		[Pure]
		public bool ApproximatelyEquals(TimePoint other)
		{
			return ApproximatelyEquals(this, other);
		}

		[Pure]
		public static bool ApproximatelyEquals(TimePoint left, TimePoint right)
		{
			return Distance(left, right) < TimeDuration.Second;
		}


		public DateTime LocalTime => DateTime.ToLocalTime();

		public static TimePoint operator +(TimePoint left, TimeDuration right)
		{
			return new TimePoint(left._milliseconds + right.Milliseconds);
		}

		[Pure]
		public TimeDuration Distance(TimePoint other)
		{
			return Distance(this, other);
		}

		[Pure]
		public static TimeDuration Distance(TimePoint left, TimePoint right)
		{
			if (left > right)
			{
				return left - right;
			}
			if (right <= left)
			{
				return TimeDuration.Zero;
			}
			return right - left;
		}

		[Pure]
		public bool Equals(TimePoint other)
		{
			return _milliseconds == other._milliseconds;
		}

		[Pure]
		public override bool Equals(object other)
		{
			if (!(other is TimePoint))
			{
				return false;
			}
			return Equals((TimePoint)other);
		}

		[Pure]
		public static bool Equals(TimePoint left, TimePoint right)
		{
			return left._milliseconds == right._milliseconds;
		}

		[Pure]
		public override int GetHashCode()
		{
			return _milliseconds.GetHashCode();
		}

		[Pure]
		public static TimePoint Max(TimePoint left, TimePoint right)
		{
			if (right._milliseconds <= left._milliseconds)
			{
				return left;
			}
			return right;
		}

		[Pure]
		public static TimePoint Min(TimePoint left, TimePoint right)
		{
			if (right._milliseconds >= left._milliseconds)
			{
				return left;
			}
			return right;
		}

		public static bool operator ==(TimePoint left, TimePoint right)
		{
			return left._milliseconds == right._milliseconds;
		}

		public static bool operator >(TimePoint left, TimePoint right)
		{
			return left._milliseconds > right._milliseconds;
		}

		public static bool operator >=(TimePoint left, TimePoint right)
		{
			return left._milliseconds >= right._milliseconds;
		}

		public static bool operator !=(TimePoint left, TimePoint right)
		{
			return left._milliseconds != right._milliseconds;
		}

		public static bool operator <(TimePoint left, TimePoint right)
		{
			return left._milliseconds < right._milliseconds;
		}

		public static bool operator <=(TimePoint left, TimePoint right)
		{
			return left._milliseconds <= right._milliseconds;
		}

		public static TimePoint operator -(TimePoint left, TimeDuration right)
		{
			if (right.Milliseconds >= left._milliseconds)
			{
				return Zero;
			}
			return new TimePoint(left._milliseconds - right.Milliseconds);
		}

		public static TimeDuration operator -(TimePoint left, TimePoint right)
		{
			if (right._milliseconds >= left._milliseconds)
			{
				return TimeDuration.Zero;
			}
			return new TimeDuration(left._milliseconds - right._milliseconds);
		}

		[Pure]
		public override string ToString()
		{
			return LocalTime.ToString("dd.MM.yyyy HH:mm:ss.fff");
		}
	}
}