using System;
using System.Diagnostics;

namespace SAS.Model.Time
{
	public sealed class Date : IDisposable
	{
		private static Date _instance;

		private ulong _timeSync;
		private Stopwatch _stopwatch;

		public TimePoint Now => TimePoint.CreateByMilliseconds((ulong)_stopwatch.ElapsedMilliseconds + _timeSync);

		private Date()
		{
			_stopwatch = Stopwatch.StartNew();
			TimeSpan timeSpan = DateTime.UtcNow.Subtract(TimePoint.Epoch);
			_timeSync = (ulong)timeSpan.TotalMilliseconds;
		}

		public void SetTime(TimePoint actualTime)
		{
			_stopwatch?.Stop();
			_stopwatch = Stopwatch.StartNew();
			TimeSpan timeSpan = actualTime.DateTime.Subtract(TimePoint.Epoch);
			_timeSync = (ulong)timeSpan.TotalMilliseconds;
		}

		public void Dispose()
		{
			_instance = null;
		}

		public static Date GetInstance()
		{
			if (_instance == null)
			{
				_instance = new Date();
			}
			return _instance;
		}
	}
}