﻿namespace Framework.Model.Localization
{
	public enum Language
	{
		None = 0,
		English = 1,
		Russian = 2,
		Espanol = 3,
		Portuguese = 4,
		Pinoy = 5
	}

	public static class LanguageExtension
	{
		public static string ToPrintName(this Language language)
		{
			switch (language)
			{
				case Language.None:
					return "None";
				case Language.English:
					return "English";
				case Language.Russian:
					return "Русский";
				case Language.Espanol:
					return "Español";
				case Language.Portuguese:
					return "Português";
				case Language.Pinoy:
					return "Pinoy";
				default:
					return "Unknown";
			}
		}
	}
}