using System;
using System.Text;
using Framework.FixedMath;

namespace Framework.Serialization
{
	public sealed class BinarySerializer : IBinarySerializer
	{
		private static readonly Encoding Encoding = Encoding.UTF8;
		private byte[] _stringBuffer;

		private byte[] _buffer;
		private int _position;

		public BinarySerializer(int capacity = 0)
		{
			_buffer = new byte[GetNextPowerOfTwo(capacity)];
		}

		public byte[] GetResult(out int bytesCount)
		{
			bytesCount = _position;
			return _buffer;
		}

		public void Clear()
		{
			_position = 0;
		}

		private void EnsureCapacity(int capacity)
		{
			if (_buffer.Length >= capacity + _position)
			{
				return;
			}
			byte[] biggerArray = new byte[GetNextPowerOfTwo(capacity + _position)];
			if (_position > 0)
			{
				Array.Copy(_buffer, 0, biggerArray, 0, _position);
			}
			_buffer = biggerArray;
		}

		private static int GetNextPowerOfTwo(int value)
		{
			if (value < 32)
			{
				return 32;
			}
			int num = 1;
			while (value > 0)
			{
				value >>= 1;
				num <<= 1;
			}
			return num;
		}

		public void Serialize(bool value)
		{
			EnsureCapacity(1);
			_buffer[_position] = (byte)(value ? 1 : 0);
			_position += 1;
		}

		public void Serialize([CanBeNull] bool[] value)
		{
			if (value != null)
			{
				int length = value.Length;
				Serialize(length);
				for (int i = 0; i < length; i++)
				{
					Serialize(value[i]);
				}
			}
			else
			{
				Serialize(0);
			}
		}

		public void Serialize(byte value)
		{
			EnsureCapacity(1);
			_buffer[_position] = value;
			_position += 1;
		}

		public void Serialize(byte[] value)
		{
			if (value != null)
			{
				int length = value.Length;
				Serialize(length);
				for (int i = 0; i < length; i++)
				{
					Serialize(value[i]);
				}
			}
			else
			{
				Serialize(0);
			}
		}

		public void Serialize(int value)
		{
			EnsureCapacity(4);
			_buffer[_position] = (byte)value;
			_buffer[_position + 1] = (byte)(value >> 8);
			_buffer[_position + 2] = (byte)(value >> 16);
			_buffer[_position + 3] = (byte)(value >> 24);
			_position += 4;
		}

		public void Serialize(int[] value)
		{
			if (value != null)
			{
				int length = value.Length;
				Serialize(length);
				for (int i = 0; i < length; i++)
				{
					Serialize(value[i]);
				}
			}
			else
			{
				Serialize(0);
			}
		}

		public void Serialize(uint value)
		{
			Serialize((int)value);
		}

		public void Serialize(long value)
		{
			EnsureCapacity(8);
			unsafe
			{
				byte* pointerSource = (byte*)&value;
				fixed (byte* pointerTarget = _buffer)
				{
					for (int i = 0; i < 8; i++)
					{
						pointerTarget[_position + i] = pointerSource[i];
					}
				}
			}
			_position += 8;
		}

		public void Serialize(long[] value)
		{
			if (value != null)
			{
				int length = value.Length;
				Serialize(length);
				for (int i = 0; i < length; i++)
				{
					Serialize(value[i]);
				}
			}
			else
			{
				Serialize(0);
			}
		}

		public void Serialize(ulong value)
		{
			Serialize((long)value);
		}

		public void Serialize(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				Serialize(0);
			}
			else
			{
				int maxByteCount = Encoding.GetMaxByteCount(value.Length);
				if (_stringBuffer == null || _stringBuffer.Length < maxByteCount)
				{
					_stringBuffer = new byte[GetNextPowerOfTwo(maxByteCount)];
				}
				int bytesLength = Encoding.GetBytes(value, 0, value.Length, _stringBuffer, 0);
				Serialize(bytesLength);
				EnsureCapacity(bytesLength);
				Array.Copy(_stringBuffer, 0, _buffer, _position, bytesLength);
				_position += bytesLength;
			}
		}

		public void Serialize(string[] value)
		{
			if (value != null)
			{
				int length = value.Length;
				Serialize(length);
				for (int i = 0; i < length; i++)
				{
					Serialize(value[i]);
				}
			}
			else
			{
				Serialize(0);
			}
		}

		public void Serialize(Fix64 value)
		{
			long rawValue = value.RawValue;
			Serialize(rawValue);
		}
	}
}