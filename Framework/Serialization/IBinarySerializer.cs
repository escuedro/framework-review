using Framework.FixedMath;

namespace Framework.Serialization
{
	public interface IBinarySerializer
	{
		void Serialize(bool value);
		void Serialize(bool[] value);

		void Serialize(byte value);
		void Serialize(byte[] value);

		//void Serialize(short value);

		//void Serialize(ushort value);

		void Serialize(int value);
		void Serialize(int[] value);

		void Serialize(uint value);

		void Serialize(long value);
		void Serialize(long[] value);

		void Serialize(ulong value);

		void Serialize(string value);
		void Serialize(string[] value);

		void Serialize(Fix64 value);
	}
}