
using System;

namespace Framework.Serialization
{
	[AttributeUsage(AttributeTargets.Class)]
	// ReSharper disable once InconsistentNaming
	public sealed class RPCAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class GenerateSerializeAttribute : Attribute
	{
	}
}