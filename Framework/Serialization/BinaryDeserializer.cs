using System;
using System.Text;
using Framework.FixedMath;

namespace Framework.Serialization
{
	public sealed class BinaryDeserializer : IBinaryDeserializer
	{
		private static readonly Encoding Encoding = Encoding.UTF8;

		private readonly byte[] _readBuffer = new byte[8];
		private byte[] _array;
		private int _length;
		private int _position;

		public BinaryDeserializer(byte[] array, int start = 0)
		{
			Init(array, start);
		}

		public bool IsReadToEnd()
		{
			return _position == _length;
		}

		public void Init(byte[] array, int start = 0)
		{
			_array = array;
			_length = _array.Length;
			_position = start;
		}

		public void Deserialize(out bool value)
		{
			FillBuffer(1);
			value = _readBuffer[0] != 0;
		}

		public void Deserialize(out bool[] value)
		{
			Deserialize(out int length);
			if (length > 0)
			{
				value = new bool[length];
				for (int i = 0; i < length; i++)
				{
					Deserialize(out bool element);
					value[i] = element;
				}
			}
			else
			{
				value = null;
			}
		}

		public void Deserialize(out byte value)
		{
			FillBuffer(1);
			value = _readBuffer[0];
		}

		public void Deserialize(out byte[] value)
		{
			Deserialize(out int length);
			if (length > 0)
			{
				value = new byte[length];
				for (int i = 0; i < length; i++)
				{
					Deserialize(out byte element);
					value[i] = element;
				}
			}
			else
			{
				value = null;
			}
		}

		public void Deserialize(out int value)
		{
			FillBuffer(4);
			value = _readBuffer[0] | _readBuffer[1] << 8 | _readBuffer[2] << 16 | _readBuffer[3] << 24;
		}

		public void Deserialize(out int[] value)
		{
			Deserialize(out int length);
			if (length > 0)
			{
				value = new int[length];
				for (int i = 0; i < length; i++)
				{
					Deserialize(out int element);
					value[i] = element;
				}
			}
			else
			{
				value = null;
			}
		}

		public void Deserialize(out uint value)
		{
			Deserialize(out int valueInt);
			value = (uint)valueInt;
		}

		public void Deserialize(out long value)
		{
			FillBuffer(8);
			long copyValue = 0;
			unsafe
			{
				fixed (byte* pointerSource = _readBuffer)
				{
					byte* pointerTarget = (byte*)&copyValue;
					for (int i = 0; i < 8; i++)
					{
						pointerTarget[i] = pointerSource[i];
					}
				}
			}
			value = copyValue;
		}

		public void Deserialize(out long[] value)
		{
			Deserialize(out int length);
			if (length > 0)
			{
				value = new long[length];
				for (int i = 0; i < length; i++)
				{
					Deserialize(out long element);
					value[i] = element;
				}
			}
			else
			{
				value = null;
			}
		}

		public void Deserialize(out ulong value)
		{
			Deserialize(out long valueLong);
			value = (ulong)valueLong;
		}


		public void Deserialize(out string value)
		{
			Deserialize(out int length);
			if (length == 0)
			{
				value = "";
			}
			else
			{
				value = Encoding.GetString(_array, _position, length);
				_position += length;
			}
		}

		public void Deserialize(out Fix64 value)
		{
			Deserialize(out long rawValue);
			value = Fix64.CreateByRawValue(rawValue);
		}

		public void Deserialize(out string[] value)
		{
			Deserialize(out int length);
			if (length > 0)
			{
				value = new string[length];
				for (int i = 0; i < length; i++)
				{
					Deserialize(out string element);
					value[i] = element;
				}
			}
			else
			{
				value = null;
			}
		}

		private void FillBuffer(int count)
		{
			int next = _position + count;
			if (next > _length)
			{
				throw new Exception("source array is ended");
			}
			for (int i = 0; i < count; i++)
			{
				_readBuffer[i] = _array[_position + i];
			}
			_position = next;
		}
	}
}