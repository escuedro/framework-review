using Framework.FixedMath;

namespace Framework.Serialization
{
	public interface IBinaryDeserializer
	{
		void Deserialize(out bool value);
		void Deserialize(out bool[] value);

		void Deserialize(out byte value);
		void Deserialize(out byte[] value);

		//void Deserialize(out short value);

		//void Deserialize(out ushort value);

		void Deserialize(out int value);

		void Deserialize(out int[] value);

		void Deserialize(out uint value);

		void Deserialize(out long value);
		void Deserialize(out long[] value);

		void Deserialize(out ulong value);

		void Deserialize(out string value);

		void Deserialize(out Fix64 value);

		void Deserialize(out string[] value);

		bool IsReadToEnd();
	}
}