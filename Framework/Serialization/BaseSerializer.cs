using System;

namespace Framework.Serialization
{
	public abstract class BaseSerializer
	{
		public virtual void Deserialize<T>(IBinaryDeserializer deserializer, out T data)
		{
			throw new Exception($"Please run protocol generator");
		}

		public virtual void Serialize<T>(IBinarySerializer serializer, T data)
		{
			throw new Exception($"Please run protocol generator");
		}
	}
}