#if UNITY_EDITOR
using System;
using System.IO;
using System.Text;
using Framework;
using Framework.Codegen;
using Framework.Codegen.TranslationCodegen;
using UnityEditor;

namespace GenerateDI
{
	[InitializeOnLoad]
	public class CodeGeneratorClient
	{
		private static readonly string GenerateDiKey = "GenerateDi";
		private static readonly string GeneratedDiPath = Path.Combine(UnityEngine.Application.dataPath,
				"Framework", "Global", "Generated", "GeneratedInjectors.cs");
		private static readonly string ProjectRelativeDIPath =
				Path.Combine("Assets", "Framework", "Global", "Generated", "GeneratedInjectors.cs");
		private static readonly string GenerateProtocolKey = "GenerateProtocol";
		private static readonly string GeneratedSerializerPath = Path.Combine(UnityEngine.Application.dataPath,
				"Framework", "Global", "Generated", "GeneratedSerializer.cs");
		private static readonly string ProjectRelativeSerializerPath =
				Path.Combine("Assets", "Framework", "Global", "Generated", "GeneratedSerializer.cs");
		private static readonly string GenerateResourcesPath = Path.Combine(UnityEngine.Application.dataPath,
				"Game", "Resource.cs");
		private static readonly string ProjectRelativeResourcesPath = Path.Combine("Assets", "Game", "Resource.cs");
		private static readonly string ReimportDiPath =
				Path.Combine("Assets", "Framework", "Global", "Service", "GeneratedBinder.cs");
		private static readonly string ReimportSerializerPath =
				Path.Combine("Assets", "Framework", "Global", "Service", "GeneratedSerializer.cs");
		public static readonly string SoundsTypePath =
				Path.Combine(UnityEngine.Application.dataPath, "Game", "Sounds", "SoundType.cs");
		private static readonly string ProjectRelativeSoundTypePath =
				Path.Combine("Assets", "Game", "Sounds", "SoundType.cs");
		private static readonly string TranslationKeysPath =
				Path.Combine(UnityEngine.Application.dataPath, "Game", "Translations", "TranslationKeys.cs");
		private static readonly string ProjectRelativeTranslationKeysPath =
				Path.Combine("Assets", "Game", "Translations", "TranslationKeys.cs");

		static CodeGeneratorClient()
		{
			if (EditorPrefs.GetBool(GenerateDiKey, false))
			{
				EditorPrefs.SetBool(GenerateDiKey, false);
				UpdateDi();
				AssetDatabase.ImportAsset(ProjectRelativeDIPath, ImportAssetOptions.ImportRecursive);
				//todo recompile only Assembly-CSharp
			}
			if (EditorPrefs.GetBool(GenerateProtocolKey, false))
			{
				EditorPrefs.SetBool(GenerateProtocolKey, false);
				UpdateProtocol();
				AssetDatabase.ImportAsset(ProjectRelativeSerializerPath, ImportAssetOptions.ImportRecursive);
				//todo recompile only Assembly-CSharp
			}
		}

		public static void PrepareGeneratedCode()
		{
			UpdateDi();
			UpdateProtocol();
			ProgramParameters programParameters = new ProgramParameters(Environment.GetCommandLineArgs());
			string customDefine = programParameters.GetPathByName("-custom_script_define");
			PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL, customDefine ?? string.Empty);
		}

		private static void UpdateDi()
		{
			UpdateGeneratedCode(GeneratedDiPath, "FrameworkAssembly", "GameAssembly", "Assembly-CSharp",
					"GameSharedAssembly");
		}

		private static void UpdateProtocol()
		{
			UpdateGeneratedSerializer(GeneratedSerializerPath, "FrameworkAssembly", "GameSharedAssembly",
					"GameAssembly");
		}

		[MenuItem("Codegen/Update DI")]
		public static void GenerateDi()
		{
			File.WriteAllText(GeneratedDiPath, "");
			EditorPrefs.SetBool(GenerateDiKey, true);
			AssetDatabase.ImportAsset(ReimportDiPath,
					ImportAssetOptions.ImportRecursive); // todo skip if no compile errors
		}

		[MenuItem("Codegen/Protocol")]
		public static void GenerateProtocol()
		{
			File.WriteAllText(GeneratedSerializerPath, "");
			EditorPrefs.SetBool(GenerateProtocolKey, true);
			AssetDatabase.ImportAsset(ReimportSerializerPath,
					ImportAssetOptions.ImportRecursive); // todo skip if no compile errors
		}

		[MenuItem("Codegen/Resource Paths")]
		public static void GenerateResourcePaths()
		{
			File.WriteAllText(GenerateResourcesPath, "");
			UpdateGeneratedResourcesPaths(GenerateResourcesPath);
			AssetDatabase.ImportAsset(ProjectRelativeResourcesPath, ImportAssetOptions.ImportRecursive);
		}

		[MenuItem("Codegen/Sounds Type")]
		public static void GenerateSoundsType()
		{
			File.WriteAllText(SoundsTypePath, "");
			UpdateSoundsType(SoundsTypePath);
			AssetDatabase.ImportAsset(ProjectRelativeSoundTypePath, ImportAssetOptions.ImportRecursive);
		}

		public static void GenerateTranslationKeys()
		{
			File.WriteAllText(TranslationKeysPath, "");
			UpdateTranslationKeys(TranslationKeysPath);
			AssetDatabase.ImportAsset(ProjectRelativeTranslationKeysPath, ImportAssetOptions.ImportRecursive);
		}

		private static void UpdateTranslationKeys(string translationKeysPath)
		{
			Log.Info("Start analyze translate keys");
			var translationKeysCodegen = new TranslationsCodegen();
			CodegenFile keysFile = translationKeysCodegen.GenerateTranslationKeys();
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			keysFile.Write(stringBuilder, 0);
			File.WriteAllText(translationKeysPath, stringBuilder.ToString());
		}

		private static void UpdateSoundsType(string resultPath)
		{
			Log.Info("Start analyze sounds path");
			var soundsAnalyzer = new SoundsAnalyzer();
			var soundsTypeCodegen = new SoundsTypeCodegen();
			EnumFile resourcesFile = soundsTypeCodegen.GenerateSoundsType(soundsAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			resourcesFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}

		private static void UpdateGeneratedCode(string resultPath, params string[] assemblyNames)
		{
			Log.Info("Start analyze DI");
			DiProjectAnalyzer diProjectAnalyzer = new DiProjectAnalyzer(assemblyNames);
			DiProjectCodegen projectCodegen = new DiProjectCodegen();
			CodegenFile injectorFile = projectCodegen.GenerateInjectors(diProjectAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			injectorFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}

		private static void UpdateGeneratedSerializer(string resultPath, params string[] assemblyNames)
		{
			Log.Info("Start analyze Protocol");
			var projectAnalyzer = new SerializationProjectAnalyzer(true, assemblyNames);
			var projectCodegen = new SerializerCodegen();
			CodegenFile injectorFile = projectCodegen.GenerateSerializer(projectAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			injectorFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}

		private static void UpdateGeneratedResourcesPaths(string resultPath)
		{
			Log.Info("Start analyze resources");
			var resourcesAnalyzer = new ResourcesAnalyzer();
			var projectCodegen = new ResourcesCodegen();
			CodegenFile resourcesFile = projectCodegen.GenerateResourcesPaths(resourcesAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			resourcesFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}
	}
}
#endif