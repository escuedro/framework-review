﻿#if !NOT_UNITY3D

using Framework.Client.Translations;

namespace Framework.Codegen.TranslationCodegen
{
    public class TranslationsCodegen
    {
        public CodegenFile GenerateTranslationKeys()
        {
            CodegenFile codegenFile = new CodegenFile()
            {
                Usings = new string[] {"Framework"},
                Namespace = "Game.Translations",
                Classes = GetClasses()
            };
            return codegenFile;
        }

        private CodegenClass[] GetClasses()
        {
            CodegenClass[] classes = new CodegenClass[1];
            TranslationKey[] translationKeys = Translation.GetTranslationKeys();

            CodegenClass translationKeyClass = new CodegenClass()
            {
                Name = "TranslationKeys",
                IsAbstract = true,
                Fields = GetFields(translationKeys)
            };
            classes[0] = translationKeyClass;
            return classes;
        }

        private CodegenField[] GetFields(TranslationKey[] translationKeys)
        {
            CodegenField[] fields = new CodegenField[translationKeys.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                TranslationKey translationKey = translationKeys[i];
                fields[i] = new CodegenField()
                {
                    IsReadonly = true,
                    IsStatic = true,
                    Value = $"new TranslationKey(\"{translationKeys[i].Value}\")",
                    Variable = new CodegenVariable()
                    {
                        Type = typeof(TranslationKey),
                        Name = translationKey.Value,
                    }
                };
            }

            return fields;
        }
    }
}
#endif