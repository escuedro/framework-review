#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Hasher;

namespace Framework.Codegen
{
	public class HashProjectAnalyzer
	{
		public readonly List<Type> HashTypes = new List<Type>();

		public HashProjectAnalyzer(params string[] assembliesName)
		{
			Assembly[] assemblies = new Assembly[assembliesName.Length];
			for (int i = 0; i < assembliesName.Length; i++)
			{
				assemblies[i] = AppDomain.CurrentDomain.Load(assembliesName[i]);
			}
			FindTypes(assemblies);
		}

		private void FindTypes(Assembly[] assemblies)
		{
			Type generateAttribute = typeof(GenerateHashAttribute);

			foreach (Assembly assembly in assemblies)
			{
				foreach (TypeInfo typeInfo in assembly.DefinedTypes)
				{
					if (Attribute.IsDefined(typeInfo, generateAttribute))
					{
						HashTypes.Add(typeInfo);
					}
				}
			}
		}
	}
}
#endif