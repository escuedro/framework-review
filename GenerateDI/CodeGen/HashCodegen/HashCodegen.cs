#if NOT_UNITY3D
using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Collections;
using Framework.FixedMath;

namespace Framework.Codegen
{
	public class HashCodegen
	{
		private static List<Type> _simpleTypes = new List<Type>
		{
				typeof(bool),
				typeof(byte),
				typeof(int),
				typeof(uint),
				typeof(long),
				typeof(ulong),
				typeof(string),
				typeof(Fix64)
		};


		public CodegenFile GetHashFile(HashProjectAnalyzer projectAnalyzer)
		{
			List<CodegenMethod> codegenMethods = new List<CodegenMethod>();
			HashSet<string> usings = new HashSet<string>();
			usings.Add("System");

			codegenMethods.Add(GetHashPublic());
			codegenMethods.Add(GetHashPrivate(projectAnalyzer));
			GenerateHash(projectAnalyzer, codegenMethods, usings);
			CodegenClass serializerClass = new CodegenClass
			{
					IsPartial = true,
					Name = "GeneratedHasher",
					Methods = codegenMethods.ToArray()
			};
			CodegenFile codegenFile = new CodegenFile
			{
					Define = "NOT_UNITY3D",
					Namespace = "Framework.Generated",
					Classes = new[] { serializerClass },
					Usings = new List<string>(usings).ToArray()
			};
			return codegenFile;
		}

		private CodegenMethod GetHashPrivate(HashProjectAnalyzer projectAnalyzer)
		{
			List<string> methodBody = new List<string>();
			foreach (Type hashType in projectAnalyzer.HashTypes)
			{
				string typeWritable = CodegenUtils.GetTypeWritable(hashType);
				methodBody.Add($"if (dataType == typeof({typeWritable}))");
				methodBody.Add("{");
				methodBody.Add($"	return GetHash(({typeWritable})data);");
				methodBody.Add("}");
			}

			methodBody.Add(
					"throw new Exception($\"Not found hasher for {dataType.FullName}. Try rebuild hash or add attribute [GenerateHash]\");");
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Private,
					Name = "GetHash",
					Parameters = new[]
					{
							new CodegenVariable(typeof(Type), "dataType"), new CodegenVariable(typeof(object), "data")
					},
					Body = methodBody.ToArray(),
					ReturnType = "int"
			};
		}

		private CodegenMethod GetHashPublic()
		{
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Public,
					Override = true,
					Name = "GetHash",
					GenericArgs = "T",
					Parameters = new[] { new CodegenVariable { StringType = "T", Name = "data" } },
					Body = new[] { "return GetHash(typeof(T), data);" },
					ReturnType = "int"
			};
		}


		private void GenerateHash(HashProjectAnalyzer projectAnalyzer,
				List<CodegenMethod> codegenMethods,
				HashSet<string> usings)
		{
			foreach (Type type in projectAnalyzer.HashTypes)
			{
				List<string> methodBody = new List<string>();
				string typeWritable = CodegenUtils.GetTypeWritable(type);
				FieldInfo[] fields = type.GetFields(
						BindingFlags.Public |
						BindingFlags.NonPublic |
						BindingFlags.Instance |
						BindingFlags.DeclaredOnly);

				methodBody.Add("HashCode hash = new HashCode();");
				foreach (FieldInfo fieldInfo in fields)
				{
					if (fieldInfo.FieldType.IsGenericType &&
							fieldInfo.FieldType.GetGenericTypeDefinition() == typeof(ConstArray<>))
					{
						Type arrayType = fieldInfo.FieldType.GenericTypeArguments[0];
						usings.Add(arrayType.Namespace);
						methodBody.Add($"hash.Add(data.{fieldInfo.Name}.Count);");
						methodBody.Add($"foreach(var element in  data.{fieldInfo.Name})");
						methodBody.Add("{");
						if (_simpleTypes.Contains(arrayType))
						{
							methodBody.Add($"	hash.Add(element.GetHashCode());");
						}
						else if (arrayType.IsEnum)
						{
							methodBody.Add($"	hash.Add((int)element);");
						}
						else
						{
							methodBody.Add($"	hash.Add(GetHash(element));");
						}
						methodBody.Add("}");
						continue;
					}
					if (_simpleTypes.Contains(fieldInfo.FieldType))
					{
						methodBody.Add($"hash.Add(data.{fieldInfo.Name});");
					}
					else
					{
						usings.Add(fieldInfo.FieldType.Namespace);
						if (fieldInfo.FieldType.IsEnum)
						{
							methodBody.Add($"hash.Add((int)data.{fieldInfo.Name});");
						}
						else
						{
							methodBody.Add($"hash.Add(GetHash(data.{fieldInfo.Name}));");
						}
					}
				}
				methodBody.Add("return hash.ToHashCode();");

				CodegenMethod deserializeMethod = new CodegenMethod
				{
						Visibility = CodegenVisibility.Private,
						Name = "GetHash",
						ReturnType = "int",

						Parameters = new[]
						{
								new CodegenVariable { Name = "data", Type = type }
						},
						Body = methodBody.ToArray()
				};

				Log.Info($"Create Hasher for {typeWritable}");
				codegenMethods.Add(deserializeMethod);
				usings.Add(type.Namespace);
			}
		}
	}
}
#endif