using System;
using Framework.Injection;

namespace Framework.Codegen
{
	public class AnalyzerRegistation
	{
		public Type ClassType;
		public Type BindType;
		public Registration Registration;

		public AnalyzerRegistation(Type classType, Type bindType, Registration registration)
		{
			ClassType = classType;
			BindType = bindType;
			Registration = registration;
		}
	}
}