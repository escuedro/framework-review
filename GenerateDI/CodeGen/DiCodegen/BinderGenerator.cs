using System;
using System.Collections.Generic;
using Framework.Injection;

namespace Framework.Codegen
{
	public class BinderGenerator
	{
		public static CodegenClass GetBinderCodegen(DiProjectAnalyzer projectAnalyzer)
		{
			CodegenVariable containerParameter = new CodegenVariable(typeof(Container), "container");

			List<string> bodyLines = new List<string>();
			foreach (InjectInfo injectInfo in projectAnalyzer.InjectDirectly)
			{
				Type injectType = injectInfo.Type;
				Type bindType = projectAnalyzer.GetBindingType(injectType);
				if (!projectAnalyzer.HasBinding(injectInfo.Type) && !injectInfo.HasDirectInjectReason())
				{
					continue;
				}
				if (injectInfo.Type.IsGenericTypeDefinition)
				{
					if (injectInfo.HasDirectInjectReason())
					{
						throw new UsageException($"\n\nDo not inject directly to generic class {injectInfo.Type.Name}, create base non generic class\n");
					}
					continue;
				}

				if (bindType != injectType)
				{
					bodyLines.Add(
							$"container.AddInjector(typeof({injectType.Name}), new {injectType.Name}Injector());");
				}
				bodyLines.Add($"container.AddInjector(typeof({bindType.Name}), new {injectType.Name}Injector());");
			}
			foreach (AnalyzerRegistation registration in projectAnalyzer.Registrations)
			{
				string isTransient = registration.Registration.IsTransient ? "true" : "false";
				bodyLines.Add(
						$"container.AddRegistration(typeof({registration.BindType.Name}), {isTransient});");
			}

			CodegenClass codegenClass = new CodegenClass
			{
					Visibility = CodegenVisibility.Public,
					IsPartial = true,
					Name = "GeneratedBinder",
					Methods = new[]
					{
							new CodegenMethod
							{
									Visibility = CodegenVisibility.Public,
									Name = "Bind",
									Override = true,
									Parameters = new[] { containerParameter },
									ReturnType = "void",
									Body = bodyLines.ToArray()
							}
					}
			};
			return codegenClass;
		}
	}
}