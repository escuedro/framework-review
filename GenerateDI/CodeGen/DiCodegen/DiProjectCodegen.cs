using System.Collections.Generic;
using Framework.Injection;

namespace Framework.Codegen
{
	public class DiProjectCodegen
	{
		public CodegenFile GenerateInjectors(DiProjectAnalyzer projectAnalyzer)
		{
			List<CodegenClass> codegenClasses = new List<CodegenClass>(projectAnalyzer.InjectDirectly.Count + 1);

			HashSet<string> usings = new HashSet<string>();
			codegenClasses.Add(BinderGenerator.GetBinderCodegen(projectAnalyzer));
			foreach (InjectInfo injectInfo in projectAnalyzer.InjectDirectly)
			{
				if (!projectAnalyzer.HasBinding(injectInfo.Type) && !injectInfo.HasDirectInjectReason())
				{
					continue;
				}

				if (injectInfo.Type.IsGenericTypeDefinition)
				{
					if (injectInfo.HasDirectInjectReason())
					{
						throw new UsageException(
								$"\n\nDo not inject directly to generic class {injectInfo.Type.Name}, create base non generic class\n");
					}
					continue;
				}

				Log.Info($"Create Injector for {injectInfo.Type.Name}");
				CodegenClass injectorCodegen = InjectorGenerator.GetInjectorCodegen(injectInfo, usings);
				codegenClasses.Add(injectorCodegen);
				usings.Add(injectInfo.Type.Namespace);
			}
			foreach (AnalyzerRegistation registration in projectAnalyzer.Registrations)
			{
				usings.Add(registration.BindType.Namespace);
			}
			usings.Add("System");
			usings.Add("Framework.Injection");
			usings.Add("Framework.Serialization");
			usings.Remove("Framework");


			CodegenFile codegenFile = new CodegenFile
			{
					Namespace = "Framework.Generated",
					Classes = codegenClasses.ToArray(),
					Usings = new List<string>(usings).ToArray()
			};
			return codegenFile;
		}
	}
}