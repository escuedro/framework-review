using System;
using System.Collections.Generic;
using System.Text;
using Framework.Injection;

namespace Framework.Codegen
{
	public class InjectorGenerator
	{
		public static CodegenClass GetInjectorCodegen(InjectInfo injectInfo, HashSet<string> usings)
		{
			CodegenVariable containerVariable = new CodegenVariable(typeof(IContainer), "_container");
			string injectorName = injectInfo.Type.Name + "Injector";
			CodegenField containerField = new CodegenField
			{
					Visibility = CodegenVisibility.Private,
					Variable = containerVariable
			};

			List<CodegenMethod> codegenMethods = new List<CodegenMethod>();

			if (TryGetInjectorInstantiate(injectInfo, containerField, usings, out CodegenMethod injectorInstantiate))
			{
				codegenMethods.Add(injectorInstantiate);
			}
			if(TryGetInjectorInject(injectInfo, containerField, usings, out injectorInstantiate))
			{
				codegenMethods.Add(injectorInstantiate);
			}

			CodegenClass codegenClass = new CodegenClass
			{
					Visibility = CodegenVisibility.Public,
					Name = injectorName,
					Interface = typeof(BaseInjector),
					IsSealed = true,
					Methods = codegenMethods.ToArray()
			};
			return codegenClass;
		}

		private static bool TryGetInjectorInstantiate(InjectInfo injectInfo,
				CodegenField containerField,
				HashSet<string> usings,
				out CodegenMethod result)
		{
			if (injectInfo.Constructor != null)
			{
				StringBuilder sb = new StringBuilder();
				var parameters = injectInfo.Constructor.Parameters;
				if (parameters != null)
				{
					for (int i = 0; i < parameters.Count; i++)
					{
						var constructorParameter = parameters[i];
						sb.Append(
								$"{containerField.Variable.Name}.Resolve<{constructorParameter.Info.ParameterType.Name}>()");
						usings.Add(constructorParameter.Info.ParameterType.Namespace);
						if (i + 1 < parameters.Count)
						{
							sb.Append(", ");
						}
					}
				}
				result = new CodegenMethod
				{
						Visibility = CodegenVisibility.Public,
						ReturnType = "object",
						Override = true,
						Name = "Instantiate",
						Body =  new[]
						{
								$"return new {injectInfo.Type.Name}({sb});"
						}
				};
				return true;
			}
			result = default;
			return false;
		}

		private static bool TryGetInjectorInject(InjectInfo injectInfo,
				CodegenField containerField,
				HashSet<string> usings,
				out CodegenMethod result)
		{
			List<string> bodyLines = new List<string>();
			bodyLines.Add($"var obj = ({injectInfo.Type.Name})instance;");
			var fields = injectInfo.Fields;
			if (fields != null)
			{
				foreach (InjectFieldInfo fieldInfo in fields)
				{
					bodyLines.Add(
							$"obj.{fieldInfo.Info.Name} = {containerField.Variable.Name}.Resolve<{fieldInfo.Info.FieldType.Name}>();");
					usings.Add(fieldInfo.Info.FieldType.Namespace);
				}
			}

			var methodsfields = injectInfo.Methods;
			if (methodsfields != null)
			{
				methodsfields.Sort((a, b) => string.Compare(a.Info.Name, b.Info.Name, StringComparison.Ordinal));

				foreach (var methodInfo in methodsfields)
				{
					var parameters = methodInfo.Parameters;
					StringBuilder sb = new StringBuilder();
					if (parameters != null)
					{
						for (int i = 0; i < parameters.Count; i++)
						{
							var methodParameter = parameters[i];
							sb.Append($"{containerField.Variable.Name}.Resolve<{methodParameter.Info.ParameterType.Name}>()");
							if (i + 1 < parameters.Count)
							{
								sb.Append(", ");
							}
							usings.Add(methodParameter.Info.ParameterType.Namespace);
						}
					}

					bodyLines.Add($"obj.{methodInfo.Info.Name}({sb});");
				}
			}

			if (bodyLines.Count <= 1)
			{
				result = default;
				return false;
			}
			result = new CodegenMethod
			{
					Visibility = CodegenVisibility.Public,
					Override = true,
					ReturnType = "void",
					Name = "Inject",
					Parameters = new[] { new CodegenVariable(typeof(object), "instance") },
					Body = bodyLines.ToArray()
			};
			return true;
		}
	}
}