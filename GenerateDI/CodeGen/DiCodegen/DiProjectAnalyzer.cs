using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Injection;

namespace Framework.Codegen
{
	public class DiProjectAnalyzer
	{
		public readonly HashSet<InjectInfo> InjectDirectly = new HashSet<InjectInfo>();
		public readonly List<AnalyzerRegistation> Registrations = new List<AnalyzerRegistation>();
		private readonly InjectAnalyzer _injectAnalyzer;

		public DiProjectAnalyzer(params string[] assembliesName)
		{
			_injectAnalyzer = new InjectAnalyzer();
			Assembly[] assemblies = new Assembly[assembliesName.Length];
			for (int i = 0; i < assembliesName.Length; i++)
			{
				assemblies[i] = AppDomain.CurrentDomain.Load(assembliesName[i]);
			}
			FindRegistrations(assemblies);
			FindInjectedDirectly(assemblies);
		}

		private void FindRegistrations(Assembly[] assemblies)
		{
			Type BindType = typeof(BindAttribute);

			foreach (Assembly assembly in assemblies)
			{
				foreach (TypeInfo typeInfo in assembly.DefinedTypes)
				{
					if (typeInfo.IsClass)
					{
						if (Attribute.IsDefined(typeInfo, BindType))
						{
							BindAttribute bindAttribute =
									(BindAttribute)Attribute.GetCustomAttribute(typeInfo, BindType);
							Type bindType = bindAttribute.BindTo ?? typeInfo;

							if (!bindType.IsAssignableFrom(typeInfo))
							{
								throw new UsageException($"Wrong bind type in {typeInfo.FullName}");
							}
							Registrations.Add(new AnalyzerRegistation(typeInfo, bindType,
									new Registration(bindAttribute.AsTransient)));
						}
					}
				}
			}
		}

		private void FindInjectedDirectly(Assembly[] assemblies)
		{
			foreach (Assembly assembly in assemblies)
			{
				foreach (TypeInfo typeInfo in assembly.DefinedTypes)
				{
					InjectInfo injectInfo = GetInjectInfo(typeInfo);
					if (injectInfo == null)
					{
						continue;
					}

					bool hasInjectReason = injectInfo.HasInjectReason(this);
					if (hasInjectReason || HasBinding(typeInfo))
					{
						InjectDirectly.Add(injectInfo);
					}
				}
			}
		}

		public bool HasBinding(Type type)
		{
			foreach (AnalyzerRegistation registration in Registrations)
			{
				if (registration.ClassType == type)
				{
					return true;
				}
			}
			return false;
		}

		public Type GetBindingType(Type type)
		{
			foreach (AnalyzerRegistation registration in Registrations)
			{
				if (registration.ClassType == type)
				{
					return registration.BindType;
				}
			}
			return type;
		}

		[CanBeNull]
		public InjectInfo GetInjectInfo(TypeInfo typeInfo)
		{
			InjectInfo injectInfo = default;
			try
			{
				injectInfo = _injectAnalyzer.GetInfo(typeInfo, InjectFlag.Construct | InjectFlag.Inject);
			}
			catch (Exception exception)
			{
				if (exception is UsageException)
				{
					throw;
				}
				try
				{
					injectInfo = _injectAnalyzer.GetInfo(typeInfo, InjectFlag.Inject);
				}
				catch (Exception exceptionInner)
				{
					if (exceptionInner is UsageException)
					{
						throw;
					}
				}
			}
			return injectInfo;
		}
	}
}