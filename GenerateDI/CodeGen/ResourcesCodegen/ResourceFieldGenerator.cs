﻿#if !NOT_UNITY3D

using System;
using System.Text;

namespace Framework.Codegen
{
	public class ResourceFieldGenerator
	{
		public CodegenField GetField(ResourceFile resourceFile)
		{
			CodegenField field = new CodegenField()
			{
					Visibility = CodegenVisibility.Public,
					IsStatic = true,
					IsReadonly = true,
					Variable = new CodegenVariable()
					{
							Type = typeof(ResourcePath),
							Name = GetResourceFileName(resourceFile)
					},
					Value = GetValue(resourceFile)
			};
			return field;
		}

		private string GetValue(ResourceFile resourceFile)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("\"");
			string correctPath = resourceFile.ResourcePath.Value.Replace(@"\", "/");
			stringBuilder.Append(correctPath);
			stringBuilder.Append("\"");
			return stringBuilder.ToString();
		}

		private string GetResourceFileName(ResourceFile resourceFile)
		{
			StringBuilder sb = new StringBuilder();
			string correctFileName = GetCorrectFileName(resourceFile.FileName);
			sb.Append(correctFileName);
			string correctExtension = GetCorrectExtension(resourceFile.FileExtension);
			sb.Append(correctExtension);
			return sb.ToString();
		}

		private string GetCorrectFileName(string fileName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (char letter in fileName)
			{
				if (char.IsLetterOrDigit(letter))
				{
					stringBuilder.Append(letter);
				}
			}
			if (stringBuilder.Length == 0)
			{
				throw new Exception($"File with name {fileName} is empty or consists of special characters");
			}
			return FirstLetterToUpper(stringBuilder.ToString());
		}

		private string GetCorrectExtension(string inputString)
		{
			string correctExtension = inputString.Remove(0, 1);
			return FirstLetterToUpper(correctExtension);
		}

		private string FirstLetterToUpper(string inputString)
		{
			if (inputString.Length > 1)
			{
				return char.ToUpper(inputString[0]) + inputString.Substring(1);
			}

			return inputString.ToUpper();
		}
	}
}

#endif