﻿#if !NOT_UNITY3D

namespace Framework.Codegen
{
	public class ResourcesAnalyzer
	{
		public readonly ResourceFolder ResourceFolder;

		private static readonly string RootDataPath = UnityEngine.Application.dataPath;

		private readonly IResourceSearcher _resourceSearcher;

		public ResourcesAnalyzer()
		{
			_resourceSearcher = new ResourceSearcher();
			ResourceFolder = _resourceSearcher.GetResourcesFolder(RootDataPath);
		}
	}
}

#endif