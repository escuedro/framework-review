﻿#if !NOT_UNITY3D

namespace Framework.Codegen
{
	public readonly struct ResourceFile
	{
		public readonly string FileName;
		public readonly string FileExtension;
		public readonly ResourcePath ResourcePath;

		public ResourceFile(string fileName, string fileExtension, ResourcePath resourcePath)
		{
			FileName = fileName;
			FileExtension = fileExtension;
			ResourcePath = resourcePath;
		}
	}
}

#endif