﻿#if !NOT_UNITY3D

using System.Collections.Generic;

using System.Text;

namespace Framework.Codegen
{
	public class ResourcesCodegen
	{
		private ResourceFieldGenerator _resourceFieldGenerator = new ResourceFieldGenerator();
		public CodegenFile GenerateResourcesPaths(ResourcesAnalyzer resourcesAnalyzer)
		{
			CodegenFile codegenFile = new CodegenFile()
			{
					Usings = new string[] {"Framework"},
					Namespace = "Game",
					Classes = GetClasses(resourcesAnalyzer)
			};
			return codegenFile;
		}

		private CodegenClass[] GetClasses(ResourcesAnalyzer resourcesAnalyzer)
		{
			List<CodegenClass> codegenClasses = new List<CodegenClass>();
			codegenClasses.AddRange(GetNestedClasses(resourcesAnalyzer.ResourceFolder.Folders));
			return codegenClasses.ToArray();
		}

		private CodegenClass[] GetNestedClasses(List<Folder> resourceFolders)
		{
			CodegenClass coreClass = new CodegenClass();
			Folder coreFolder = UniteCoreFolders(resourceFolders);
			WriteCodegenClass(coreFolder, coreClass);
			return new [] {coreClass};
		}

		private Folder UniteCoreFolders(List<Folder> resourceFolders)
		{
			Folder unitedFolder = new Folder();
			unitedFolder.Name = "Resource";
			foreach (Folder resourceFolder in resourceFolders)
			{
				unitedFolder.Files.AddRange(resourceFolder.Files);
				unitedFolder.Folders.AddRange(resourceFolder.Folders);
			}
			return unitedFolder;
		}

		private void WriteCodegenClass(Folder folder, CodegenClass codegenClass)
		{
			codegenClass.Visibility = CodegenVisibility.Public;
			codegenClass.IsAbstract = true;
			codegenClass.Name = folder.Name;
			codegenClass.Fields = GetFieldsByFolder(folder);
			codegenClass.Classes = new CodegenClass[folder.Folders.Count];
			for (int i = 0; i < folder.Folders.Count; i++)
			{
				Folder subfolder = folder.Folders[i];
				CodegenClass nestedClass = new CodegenClass();
				WriteCodegenClass(subfolder, nestedClass);
				codegenClass.Classes[i] = nestedClass;
			}
		}

		private CodegenField[] GetFieldsByFolder(Folder folder)
		{
			List<CodegenField> codegenFields = new List<CodegenField>();
			WriteAllFolderFiles(folder, codegenFields);
			return codegenFields.ToArray();
		}

		private void WriteAllFolderFiles(Folder folder, List<CodegenField> codegenFields)
		{
			foreach (ResourceFile resourceFile in folder.Files)
			{
				CodegenField codegenField = _resourceFieldGenerator.GetField(resourceFile);
				codegenFields.Add(codegenField);
			}
		}
	}
}
#endif