﻿#if !NOT_UNITY3D

namespace Framework.Codegen
{
	public interface IResourceSearcher
	{
		public ResourceFolder GetResourcesFolder(string rootDirectoryPath);
	}
}
#endif