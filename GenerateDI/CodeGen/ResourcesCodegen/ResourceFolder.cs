﻿#if !NOT_UNITY3D

using System.Collections.Generic;

namespace Framework.Codegen
{
	public class ResourceFolder : BaseFolder
	{
		public List<string> CoreFolderPaths = new List<string>();
	}

	public abstract class BaseFolder
	{
		public List<Folder> Folders = new List<Folder>();
	}

	public class Folder : BaseFolder
	{
		public List<ResourceFile> Files = new List<ResourceFile>();
		public string Name;
	}
}
#endif