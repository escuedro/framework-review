﻿#if !NOT_UNITY3D

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Framework.Codegen
{
	public class ResourceSearcher : IResourceSearcher
	{
		private const string ResourcesFolderName = "\\Resources";
		private const string MetaExtension = ".meta";
		private const string CoreFolderName = "Resource";

		public ResourceFolder GetResourcesFolder(string directoryPath)
		{
			List<string> resourceCoreFolders = GetResourceCoreFolders(directoryPath);
			ResourceFolder resourceFolder = new ResourceFolder();
			foreach (string resourceCoreFolder in resourceCoreFolders)
			{
				resourceFolder.Folders.Add(new Folder() { Name = CoreFolderName });
				resourceFolder.CoreFolderPaths.Add(resourceCoreFolder);
			}
			WriteResourceFolders(resourceFolder);
			return resourceFolder;
		}

		private void WriteResourceFolders(ResourceFolder resourceFolder)
		{
			for (int i = 0; i < resourceFolder.Folders.Count; i++)
			{
				WriteFolderResources(resourceFolder.Folders[i], resourceFolder.CoreFolderPaths[i]);
			}
		}

		private void WriteFolderResources(Folder folder, string folderPath)
		{
			string[] folderPaths = Directory.GetDirectories(folderPath);
			folder.Files = GetFilesInFolder(folderPath);
			foreach (string path in folderPaths)
			{
				Folder folderFromPath = new Folder { Name = GetFolderName(path) };
				folder.Folders.Add(folderFromPath);
				WriteFolderResources(folderFromPath, path);
			}
		}

		private string GetFolderName(string folderPath)
		{
			string folderName = folderPath.Remove(0, folderPath.LastIndexOf(@"\") + 1);
			StringBuilder sb = new StringBuilder();
			foreach (char character in folderName)
			{
				if (char.IsLetter(character))
				{
					sb.Append(character);
				}
			}
			return sb.ToString();
		}

		private List<string> GetResourceCoreFolders(string directoryPath)
		{
			List<string> resourceCoreFolders = new List<string>();
			WriteCoreResourceFolder(directoryPath, resourceCoreFolders);
			return resourceCoreFolders;
		}

		private void WriteCoreResourceFolder(string directoryPath, List<string> folders)
		{
			string[] allFolders = Directory.GetDirectories(directoryPath);
			string[] resourceFolders = allFolders.Where(IsResourceName).ToArray();
			folders.AddRange(resourceFolders);
			foreach (string folder in allFolders)
			{
				WriteCoreResourceFolder(folder, folders);
			}
		}

		private bool IsResourceName(string folderName)
		{
			return folderName.Remove(0, folderName.LastIndexOf(ResourcesFolderName) + ResourcesFolderName.Length).
					Length == 0 && folderName.EndsWith(ResourcesFolderName);
		}

		private List<ResourceFile> GetFilesInFolder(string folderPath)
		{
			List<ResourceFile> resourceFiles = new List<ResourceFile>();
			string[] files = Directory.GetFiles(folderPath);
			foreach (string file in files)
			{
				if (!Path.GetExtension(file).Equals(MetaExtension))
				{
					string fileName = Path.GetFileNameWithoutExtension(file);
					string fiileExtension = Path.GetExtension(file);
					ResourceFile resourceFile =
							new ResourceFile(fileName, fiileExtension,
									GetResourcePathByFolder(fileName, folderPath));
					resourceFiles.Add(resourceFile);
				}
			}
			return resourceFiles;
		}

		private ResourcePath GetResourcePathByFolder(string fileName, string folderPath)
		{
			StringBuilder sb = new StringBuilder();
			int indexOfResourcesWord = folderPath.LastIndexOf(ResourcesFolderName);
			int positionOfResourcesWord = indexOfResourcesWord + ResourcesFolderName.Length;
			bool needToAddOffset = folderPath.Length > positionOfResourcesWord;
			int offset = needToAddOffset ? 1 : 0;
			sb.Append(folderPath.Remove(0, positionOfResourcesWord + offset));
			if (needToAddOffset)
			{
				sb.Append(@"\");
			}
			sb.Append(fileName);
			return new ResourcePath(sb.ToString());
		}
	}
}

#endif