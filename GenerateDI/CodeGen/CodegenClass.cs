using System;
using System.Text;

namespace Framework.Codegen
{
	public enum CodegenVisibility
	{
		Public,
		Internal,
		Private
	}

	public static class CodegenUtils
	{
		public static void WriteTab(this StringBuilder sb, int tabLevel)
		{
			for (int i = 0; i < tabLevel; i++)
			{
				sb.Append('	');
			}
		}

		public static void AppendMultiple(this StringBuilder sb, params string[] words)
		{
			for (int i = 0; i < words.Length; i++)
			{
				string word = words[i];
				sb.Append(word);
				if (i + 1 < words.Length)
				{
					sb.Append(' ');
				}
			}
		}

		public static string ToStringCorrect(this CodegenVisibility visibility)
		{
			return visibility.ToString().ToLower();
		}

		public static void WriteType(this StringBuilder sb, Type type)
		{
			sb.Append(GetTypeWritable(type));
		}

		public static string GetTypeWritable(Type type)
		{
			if (type.DeclaringType != null)
			{
				return $"{type.DeclaringType.Name}.{type.Name}";
			}
			return type.Name;
		}

		public static string ConvertToLocalName(string name)
		{
			string first = name[0].ToString().ToLower();
			return first + name.Substring(1);
		}
	}

	public class CodegenFile
	{
		[CanBeNull]
		public string Define;
		public string[] Usings;
		public string Namespace;
		public CodegenClass[] Classes;

		public void Write(StringBuilder sb, int tabLevel)
		{
			WriteUsingDeclaration(sb, tabLevel);
			sb.AppendLine();
			sb.WriteTab(tabLevel);
			sb.AppendLine($"namespace {Namespace}");
			sb.WriteTab(tabLevel);
			sb.AppendLine("{");
			for (int i = 0; i < Classes.Length; i++)
			{
				CodegenClass codegenClass = Classes[i];
				codegenClass.Write(sb, tabLevel + 1);
				if (i + 1 < Classes.Length)
				{
					sb.AppendLine();
				}
			}

			sb.WriteTab(tabLevel);
			sb.AppendLine("}");
			if (Define != null)
			{
				sb.AppendLine($"#endif");
			}
		}

		void WriteUsingDeclaration(StringBuilder sb, int tabLevel)
		{
			if (Define != null)
			{
				sb.AppendLine($"#if {Define}");
			}
			if (Usings != null)
			{
				foreach (string usingName in Usings)
				{
					if (!string.IsNullOrEmpty(usingName))
					{
						sb.WriteTab(tabLevel);
						sb.AppendLine($"using {usingName};");
					}
				}
			}
		}
	}

	public class CodegenClass
	{
		public CodegenVisibility Visibility;
		public bool IsSealed;
		public bool IsPartial;
		public bool IsAbstract;
		public string Name;
		[CanBeNull]
		public Type Interface;
		public CodegenField[] Fields;
		public CodegenMethod[] Methods;
		public CodegenClass[] Classes;

		public void Write(StringBuilder sb, int tabLevel)
		{
			WriteClassDeclaration(sb, tabLevel);
			sb.WriteTab(tabLevel);
			sb.AppendLine("{");
			WriteFieldsDeclaration(sb, tabLevel + 1);
			WriteMethodsDeclaration(sb, tabLevel + 1);
			if (Classes != null)
			{
				foreach (CodegenClass codegenClass in Classes)
				{
					codegenClass.Write(sb, tabLevel + 1);
				}
			}

			sb.WriteTab(tabLevel);
			sb.AppendLine("}");
		}

		private void WriteMethodsDeclaration(StringBuilder sb, int tabLevel)
		{
			if (Methods != null)
			{
				for (int i = 0; i < Methods.Length; i++)
				{
					CodegenMethod codegenMethod = Methods[i];
					codegenMethod.Write(sb, tabLevel);
					if (i + 1 < Methods.Length)
					{
						sb.AppendLine();
					}
				}
			}
		}


		private void WriteFieldsDeclaration(StringBuilder sb, int tabLevel)
		{
			if (Fields != null)
			{
				foreach (CodegenField codegenField in Fields)
				{
					sb.WriteTab(tabLevel);
					codegenField.Write(sb);
				}
				sb.AppendLine();
			}
		}

		private void WriteClassDeclaration(StringBuilder sb, int tabLevel)
		{
			sb.WriteTab(tabLevel);
			sb.Append(Visibility.ToStringCorrect());
			if (IsSealed)
			{
				sb.Append(" sealed");
			}
			if (IsPartial)
			{
				sb.Append(" partial");
			}
			if (IsAbstract)
			{
				sb.Append(" abstract");
			}
			sb.Append($" class {Name}");
			if (Interface != null)
			{
				sb.AppendLine($" : {Interface.Name}");
			}
			else
			{
				sb.AppendLine();
			}
		}
	}

	public struct CodegenVariable
	{
		public Type Type;
		public string StringType;
		public string Name;
		public string Parameter;

		public void Write(StringBuilder sb)
		{
			if (!string.IsNullOrEmpty(Parameter))
			{
				sb.Append(Parameter);
				sb.Append(' ');
			}
			if (StringType != null)
			{
				sb.Append(StringType);
			}
			else
			{
				sb.WriteType(Type);
			}
			sb.Append(' ');
			sb.Append(Name);
		}

		public CodegenVariable(Type type)
		{
			Type = type;
			Name = "_" + type.Name;
			Parameter = null;
			StringType = null;
		}

		public CodegenVariable(Type type, string name)
		{
			Type = type;
			Name = name;
			Parameter = null;
			StringType = null;
		}
	}

	public struct CodegenField
	{
		public CodegenVisibility Visibility;
		public bool IsStatic;
		public bool IsReadonly;
		public CodegenVariable Variable;
		public string Value;

		public void Write(StringBuilder sb)
		{
			sb.Append(Visibility.ToStringCorrect());
			if (IsStatic)
			{
				sb.Append(" static");
			}
			if (IsReadonly)
			{
				sb.Append(" readonly");
			}
			sb.Append(' ');
			Variable.Write(sb);
			if (!String.IsNullOrEmpty(Value))
			{
				sb.Append(' ');
				sb.Append("= ");
				sb.Append(Value);
			}
			sb.AppendLine(";");
		}
	}

	public struct CodegenMethod
	{
		public CodegenVisibility Visibility;
		public string Name;
		public string GenericArgs;
		public bool Override;
		public string ReturnType;
		public CodegenVariable[] Parameters;
		public string[] Body;

		public void Write(StringBuilder sb, int tabLevel)
		{
			sb.WriteTab(tabLevel);
			sb.Append(Visibility.ToStringCorrect());
			if (Override)
			{
				sb.Append(" override");
			}
			sb.Append($" {ReturnType}");
			if (!string.IsNullOrEmpty(Name))
			{
				sb.Append($" {Name}");
			}
			if (!string.IsNullOrEmpty(GenericArgs))
			{
				sb.Append($"<{GenericArgs}>");
			}
			sb.Append("(");
			if (Parameters != null)
			{
				for (int i = 0; i < Parameters.Length; i++)
				{
					CodegenVariable parameter = Parameters[i];
					parameter.Write(sb);
					if (i + 1 < Parameters.Length)
					{
						sb.Append(", ");
					}
				}
			}
			sb.AppendLine(")");
			sb.WriteTab(tabLevel);
			sb.AppendLine("{");
			if (Body != null)
			{
				foreach (string bodyLine in Body)
				{
					sb.WriteTab(tabLevel + 1);
					sb.AppendLine(bodyLine);
				}
			}

			sb.WriteTab(tabLevel);
			sb.AppendLine("}");
		}
	}
}