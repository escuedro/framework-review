using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Collections;
using Framework.FixedMath;
using Framework.Serialization;

namespace Framework.Codegen
{
	public class SerializerCodegen
	{
		private static List<Type> _simpleTypes = new List<Type>
		{
				typeof(bool),
				typeof(byte),
				typeof(int),
				typeof(uint),
				typeof(string),
				typeof(long),
				typeof(ulong),
				typeof(Fix64)
		};

		private Dictionary<Type, string> _friendlyNamesByType = new Dictionary<Type, string>
		{
				{ typeof(bool), "bool" },
				{ typeof(byte), "byte" },
				{ typeof(int), "int" },
				{ typeof(uint), "uint" },
				{ typeof(long), "long" },
				{ typeof(ulong), "ulong" },
				{ typeof(Fix64), "Fix64" }
		};

		public CodegenFile GenerateSerializer(SerializationProjectAnalyzer projectAnalyzer)
		{
			List<CodegenMethod> codegenMethods = new List<CodegenMethod>();


			HashSet<string> usings = new HashSet<string>();
			codegenMethods.Add(DeserializerBinderGenerator.GetBindingMethod(projectAnalyzer));
			codegenMethods.Add(DeserializerBinderGenerator.GetBindingMethodInternal(projectAnalyzer));
			codegenMethods.Add(SerializerBinderGenerator.GetBindingMethod());
			codegenMethods.Add(SerializerBinderGenerator.GetBindingMethodInternal(projectAnalyzer));
			GenerateDeserialize(projectAnalyzer, codegenMethods, usings);
			GenerateSerialize(projectAnalyzer, codegenMethods, usings);

			usings.Add("System");
			usings.Add("Framework.Serialization");
			usings.Add("Framework.Collections");
			usings.Remove("Framework");

			CodegenClass serializerClass = new CodegenClass
			{
					IsPartial = true,
					Name = "GeneratedSerializer",
					Methods = codegenMethods.ToArray()
			};
			CodegenFile codegenFile = new CodegenFile
			{
					Namespace = "Framework.Generated",
					Classes = new[] { serializerClass },
					Usings = new List<string>(usings).ToArray()
			};
			return codegenFile;
		}


		private void GenerateDeserialize(SerializationProjectAnalyzer projectAnalyzer,
				List<CodegenMethod> codegenMethods,
				HashSet<string> usings)
		{
			foreach (Type type in projectAnalyzer.GenerateDeserialize)
			{
				List<string> methodBody = new List<string>();
				string typeWritable = CodegenUtils.GetTypeWritable(type);
				methodBody.Add($"data = new {typeWritable}();");
				FieldInfo[] fields = type.GetFields(
						BindingFlags.Public |
						BindingFlags.NonPublic |
						BindingFlags.Instance |
						BindingFlags.DeclaredOnly);

				foreach (FieldInfo fieldInfo in fields)
				{
					usings.Add(fieldInfo.FieldType.Namespace);
					if (fieldInfo.FieldType.IsGenericType &&
							fieldInfo.FieldType.GetGenericTypeDefinition() == typeof(ConstArray<>))
					{
						Type elementType = fieldInfo.FieldType.GenericTypeArguments[0];
						usings.Add(elementType.Namespace);
						if (_simpleTypes.Contains(elementType))
						{
							string typeName = _friendlyNamesByType[elementType];
							string localArrayName = CodegenUtils.ConvertToLocalName($"{fieldInfo.Name}Array");
							methodBody.Add($"deserializer.Deserialize(out {typeName}[] {localArrayName});");
							methodBody.Add($"data.{fieldInfo.Name} = new ConstArray<{typeName}>({localArrayName});");
						}
						else
						{
							string typeName = CodegenUtils.GetTypeWritable(elementType);
							string localLengthName = CodegenUtils.ConvertToLocalName($"{fieldInfo.Name}Lenght");
							string localArrayName = CodegenUtils.ConvertToLocalName($"{fieldInfo.Name}Array");
							methodBody.Add(
									$"deserializer.Deserialize(out int {localLengthName}); // data.{fieldInfo.Name}");
							methodBody.Add($"if ({localLengthName} > 0)");
							methodBody.Add("{");
							methodBody.Add($"	var {localArrayName} = new {typeName}[{localLengthName}];");
							methodBody.Add($"	for (int i = 0; i < {localLengthName}; i++) ");
							methodBody.Add("	{");
							if (elementType.IsEnum)
							{
								methodBody.Add($"		DeserializeInternal(deserializer, out int element_int);");
								methodBody.Add($"		element = ({typeName}) element_int");
							}
							else
							{
								methodBody.Add($"		DeserializeInternal(deserializer, out {typeName} element);");
							}

							methodBody.Add($"		{localArrayName}[i] = element;");
							methodBody.Add("	}");
							methodBody.Add($"	data.{fieldInfo.Name} = new ConstArray<{typeName}>({localArrayName});");
							methodBody.Add("}");
						}
						continue;
					}
					if (_simpleTypes.Contains(fieldInfo.FieldType))
					{
						methodBody.Add($"deserializer.Deserialize(out data.{fieldInfo.Name});");
					}
					else
					{
						if (fieldInfo.FieldType.IsEnum)
						{
							string typeName = CodegenUtils.GetTypeWritable(fieldInfo.FieldType);
							methodBody.Add($"deserializer.Deserialize(out int {fieldInfo.Name}_int);");
							methodBody.Add($"data.{fieldInfo.Name} = ({typeName})  {fieldInfo.Name}_int;");
						}
						else
						{
							methodBody.Add($"DeserializeInternal(deserializer, out data.{fieldInfo.Name});");
						}
					}
				}

				CodegenMethod deserializeMethod = new CodegenMethod
				{
						Visibility = CodegenVisibility.Private,
						Name = "DeserializeInternal",
						ReturnType = "void",

						Parameters = new[]
						{
								new CodegenVariable { Name = "deserializer", Type = typeof(IBinaryDeserializer) },
								new CodegenVariable { Name = "data", Type = type, Parameter = "out" }
						},
						Body = methodBody.ToArray()
				};

				Log.Info($"Create Deserialize for {typeWritable}");
				codegenMethods.Add(deserializeMethod);
				usings.Add(type.Namespace);
			}
		}


		private void GenerateSerialize(SerializationProjectAnalyzer projectAnalyzer,
				List<CodegenMethod> codegenMethods,
				HashSet<string> usings)
		{
			foreach (Type type in projectAnalyzer.GenerateSerialize)
			{
				List<string> methodBody = new List<string>();
				string typeWritable = CodegenUtils.GetTypeWritable(type);
				FieldInfo[] fields = type.GetFields(
						BindingFlags.Public |
						BindingFlags.NonPublic |
						BindingFlags.Instance |
						BindingFlags.DeclaredOnly);

				foreach (FieldInfo fieldInfo in fields)
				{
					if (fieldInfo.FieldType.IsGenericType &&
							fieldInfo.FieldType.GetGenericTypeDefinition() == typeof(ConstArray<>))
					{
						Type arrayType = fieldInfo.FieldType.GenericTypeArguments[0];
						if (_simpleTypes.Contains(arrayType))
						{
							methodBody.Add($"serializer.Serialize(data.{fieldInfo.Name}.ToArray());");
						}
						else
						{
							methodBody.Add(
									$"serializer.Serialize(data.{fieldInfo.Name}.Count);");
							methodBody.Add($"foreach(var element in  data.{fieldInfo.Name})");
							methodBody.Add("{");
							if (arrayType.IsEnum)
							{
								methodBody.Add("	serializer.Serialize((int)element);");
							}
							else
							{
								methodBody.Add("	SerializeInternal(serializer, element);");
							}
							methodBody.Add("}");
						}
						continue;
					}
					if (_simpleTypes.Contains(fieldInfo.FieldType))
					{
						methodBody.Add($"serializer.Serialize(data.{fieldInfo.Name});");
					}
					else
					{
						if (fieldInfo.FieldType.IsEnum)
						{
							methodBody.Add($"serializer.Serialize((int)data.{fieldInfo.Name});");
						}
						else
						{
							methodBody.Add($"SerializeInternal(serializer, data.{fieldInfo.Name});");
						}
					}
				}

				CodegenMethod deserializeMethod = new CodegenMethod
				{
						Visibility = CodegenVisibility.Private,
						Name = "SerializeInternal",
						ReturnType = "void",

						Parameters = new[]
						{
								new CodegenVariable { Name = "serializer", Type = typeof(IBinarySerializer) },
								new CodegenVariable { Name = "data", Type = type }
						},
						Body = methodBody.ToArray()
				};

				Log.Info($"Create Serialize for {typeWritable}");
				codegenMethods.Add(deserializeMethod);
				usings.Add(type.Namespace);
			}
		}
	}
}