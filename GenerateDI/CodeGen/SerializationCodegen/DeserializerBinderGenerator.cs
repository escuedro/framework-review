using System;
using System.Collections.Generic;
using Framework.Serialization;

namespace Framework.Codegen
{
	public class DeserializerBinderGenerator
	{
		public static CodegenMethod GetBindingMethod(SerializationProjectAnalyzer projectAnalyzer)
		{
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Public,
					Override = true,
					Name = "Deserialize",
					GenericArgs = "T",
					ReturnType = "void",
					Parameters = new[]
					{
							new CodegenVariable { Name = "deserializer", Type = typeof(IBinaryDeserializer) },
							new CodegenVariable { Name = "data", StringType = "T", Parameter = "out" },
					},
					Body = new[]
					{
							"DeserializeInternal(deserializer, typeof(T), out var deserialized);",
							"data = (T)deserialized;"
					}
			};
		}

		public static CodegenMethod GetBindingMethodInternal(SerializationProjectAnalyzer projectAnalyzer)
		{
			List<string> bodyLines = new List<string>();
			foreach (Type type in projectAnalyzer.Bind)
			{
				if (projectAnalyzer.GenerateDeserialize.Contains(type))
				{
					string typeWritable = CodegenUtils.GetTypeWritable(type);
					bodyLines.Add($"if (dataType == typeof({typeWritable}))");
					bodyLines.Add("{");
					bodyLines.Add($"	DeserializeInternal(deserializer, out {typeWritable} typedData);");
					bodyLines.Add("	data = typedData;");
					bodyLines.Add("} else");
				}
			}
			bodyLines.Add(
					"throw new Exception($\"Not found deserializer for {dataType.FullName}. Try rebuild protocol\");");
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Private,
					Name = "DeserializeInternal",
					ReturnType = "void",
					Parameters = new[]
					{
							new CodegenVariable { Name = "deserializer", Type = typeof(IBinaryDeserializer) },
							new CodegenVariable { Name = "dataType", Type = typeof(Type) },
							new CodegenVariable { Name = "data", Type = typeof(object), Parameter = "out" },
					},
					Body = bodyLines.ToArray()
			};
		}
	}
}