using System;
using System.Collections.Generic;
using Framework.Serialization;

namespace Framework.Codegen
{
	public class SerializerBinderGenerator
	{
		public static CodegenMethod GetBindingMethod()
		{
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Public,
					Override = true,
					Name = "Serialize",
					GenericArgs = "T",
					ReturnType = "void",
					Parameters = new[]
					{
							new CodegenVariable { Name = "serializer", Type = typeof(IBinarySerializer) },
							new CodegenVariable { Name = "data", StringType = "T" }
					},
					Body = new[]
					{
							"SerializeInternal(serializer, data);"
					}
			};
		}

		public static CodegenMethod GetBindingMethodInternal(SerializationProjectAnalyzer projectAnalyzer)
		{
			List<string> bodyLines = new List<string>();
			bodyLines.Add("switch (data)");
			bodyLines.Add("{");
			foreach (Type type in projectAnalyzer.Bind)
			{
				if (projectAnalyzer.GenerateSerialize.Contains(type))
				{
					string typeWritable = CodegenUtils.GetTypeWritable(type);
					bodyLines.Add($"	case {typeWritable} typedData:");
					bodyLines.Add($"		SerializeInternal(serializer, typedData);");
					bodyLines.Add($"		return;");
				}
			}
			bodyLines.Add("}");
			bodyLines.Add(
					"throw new Exception($\"Not found deserializer for {data.GetType().FullName}. Try rebuild protocol\");");
			return new CodegenMethod
			{
					Visibility = CodegenVisibility.Private,
					Name = "SerializeInternal",
					ReturnType = "void",
					Parameters = new[]
					{
							new CodegenVariable { Name = "serializer", Type = typeof(IBinarySerializer) },
							new CodegenVariable { Name = "data", Type = typeof(object) }
					},
					Body = bodyLines.ToArray()
			};
		}
	}
}