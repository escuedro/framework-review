using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Serialization;

namespace Framework.Codegen
{
	public class SerializationProjectAnalyzer
	{
		public List<Type> GenerateSerialize = new List<Type>();
		public List<Type> GenerateDeserialize = new List<Type>();
		public List<Type> Bind = new List<Type>();

		public SerializationProjectAnalyzer(bool excludeServerCode, params string[] assembliesName)
		{
			Assembly[] assemblies = new Assembly[assembliesName.Length];
			for (int i = 0; i < assembliesName.Length; i++)
			{
				assemblies[i] = AppDomain.CurrentDomain.Load(assembliesName[i]);
			}
			FindTypes(excludeServerCode, assemblies);
		}

		private void FindTypes(bool excludeServerCode, Assembly[] assemblies)
		{
			Type generateAttribute = typeof(GenerateSerializeAttribute);
			Type rpcAttribute = typeof(RPCAttribute);

			foreach (Assembly assembly in assemblies)
			{
				foreach (TypeInfo typeInfo in assembly.DefinedTypes)
				{
					if (Attribute.IsDefined(typeInfo, generateAttribute))
					{
						GenerateSerialize.Add(typeInfo);
						GenerateDeserialize.Add(typeInfo);
					}
					if (Attribute.IsDefined(typeInfo, rpcAttribute))
					{
						foreach (var nestedType in typeInfo.DeclaredNestedTypes)
						{
							GenerateSerialize.Add(nestedType);
							GenerateDeserialize.Add(nestedType);
							Bind.Add(nestedType);
							if (nestedType.Name == "Request" && excludeServerCode)
							{
								GenerateDeserialize.Remove(nestedType);
							}
							else if (nestedType.Name == "Response" && excludeServerCode)
							{
								GenerateSerialize.Remove(nestedType);
							}
						}
					}
				}
			}
		}
	}
}