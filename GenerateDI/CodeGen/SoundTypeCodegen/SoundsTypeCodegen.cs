﻿#if !NOT_UNITY3D

namespace Framework.Codegen
{
	public class SoundsTypeCodegen
	{
		public EnumFile GenerateSoundsType(SoundsAnalyzer soundsAnalyzer)
		{
			EnumFile enumFile = new EnumFile()
			{
					Name = "SoundType",
					Namespace = "Game.Audio",
					EnumElements = soundsAnalyzer.SoundsNames,
					Define = "!NOT_UNITY3D"
			};
			return enumFile;
		}
	}
}

#endif