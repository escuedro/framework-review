﻿#if !NOT_UNITY3D

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Framework.Codegen
{
	public class SoundsAnalyzer
	{
		public readonly string[] SoundsNames;

		private static readonly string SoundFolderPath = Path.Combine(Application.dataPath, "Resources", "Sounds");
		private const string AssetExtension = ".asset";

		public SoundsAnalyzer()
		{
			SoundsNames = GetSoundsNames();
		}

		private string[] GetSoundsNames()
		{
			List<string> soundsNames = new List<string>();
			GetSoundsInFolder(SoundFolderPath, soundsNames);
			return soundsNames.ToArray();
		}

		private void GetSoundsInFolder(string soundFolderPath, List<string> result)
		{
			string[] files = Directory.GetFiles(soundFolderPath);
			foreach (string file in files)
			{
				string fileExtension = Path.GetExtension(file);
				if (fileExtension == AssetExtension)
				{
					string correctName = Path.GetFileNameWithoutExtension(file);
					result.Add(GetCorrectFileName(correctName));
				}
			}
			string[] folders = Directory.GetDirectories(soundFolderPath);
			foreach (string folder in folders)
			{
				GetSoundsInFolder(folder, result);
			}
		}

		private string GetCorrectFileName(string fileName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (char letter in fileName)
			{
				if (char.IsLetterOrDigit(letter))
				{
					stringBuilder.Append(letter);
				}
			}
			if (stringBuilder.Length == 0)
			{
				throw new Exception($"File with name {fileName} is empty or consists of special characters");
			}
			return FirstLetterToUpper(stringBuilder.ToString());
		}

		private string FirstLetterToUpper(string inputString)
		{
			if (inputString.Length > 1)
			{
				return char.ToUpper(inputString[0]) + inputString.Substring(1);
			}

			return inputString.ToUpper();
		}
	}
}

#endif