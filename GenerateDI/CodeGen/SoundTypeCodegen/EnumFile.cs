﻿using System.Text;

namespace Framework.Codegen
{
	public class EnumFile
	{
		[CanBeNull]
		public string Define;
		public string Namespace;
		public string Name;
		public string[] EnumElements;

		public void Write(StringBuilder sb, int tabLevel)
		{
			if (Define != null)
			{
				sb.AppendLine($"#if {Define}");
				sb.AppendLine();
			}
			WriteNamespace(sb);
			sb.WriteTab(tabLevel);
			sb.AppendLine("{");
			sb.WriteTab(tabLevel + 1);
			sb.AppendLine($"public enum {Name}");
			sb.WriteTab(tabLevel + 1);
			sb.AppendLine("{");
			WriteEnumMembers(sb, tabLevel + 1);
			sb.WriteTab(tabLevel + 1);
			sb.AppendLine("}");
			sb.AppendLine("}");
			if (Define != null)
			{
				sb.Append("#endif");
			}
		}

		private void WriteNamespace(StringBuilder stringBuilder)
		{
			stringBuilder.AppendLine($"namespace {Namespace}");
		}

		private void WriteEnumMembers(StringBuilder sb, int tabLevel)
		{
			foreach (string enumElement in EnumElements)
			{
				sb.WriteTab(tabLevel + 1);
				sb.AppendLine($"{enumElement},");
			}
		}
	}
}