using System;
using System.Collections.Generic;
using System.Reflection;

namespace Framework.Injection
{
	[Flags]
	public enum InjectFlag
	{
		Construct = 1,
		Inject = 2
	}

	public sealed class InjectAnalyzer
	{
#if !NOT_UNITY3D
		private static readonly Type UnityComponentType = typeof(UnityEngine.Component);
#endif
		private static readonly Type InjectAttributeType = typeof(InjectAttribute);

		private readonly Dictionary<Type, AnalyzeResult> _resultByType = new Dictionary<Type, AnalyzeResult>();

		public InjectInfo GetInfo(Type type, InjectFlag flags)
		{
			if (_resultByType.TryGetValue(type, out AnalyzeResult result))
			{
				if ((result.Flags & flags) == flags)
				{
					return result.Info;
				}

				InjectFlag missingFlags = (result.Flags ^ flags) & flags;
				Analyze(result.Info, missingFlags);
				_resultByType[type] = new AnalyzeResult(result.Info, result.Flags | missingFlags);
				return result.Info;
			}

			InjectInfo info = new InjectInfo(type);
			Analyze(info, flags);
			_resultByType.Add(type, new AnalyzeResult(info, flags));
			return info;
		}

		private static void Analyze(InjectInfo info, InjectFlag flags)
		{
			Type type = info.Type;
			if (type.IsEnum)
			{
				throw new Exception("Enum types not supported.");
			}
			if (type.IsArray)
			{
				throw new Exception("Array types not supported.");
			}
			if ((flags & InjectFlag.Construct) == InjectFlag.Construct)
			{
#if !NOT_UNITY3D
				if (UnityComponentType.IsAssignableFrom(type))
				{
					throw new Exception($"Can't construct UnityEngine component \"{type.Name}\".");
				}
#endif
				if (type.IsAbstract)
				{
					throw new Exception($"Can't construct abstract class \"{type.Name}\".");
				}
				info.Constructor = GetConstructorInfo(type);
			}
			if ((flags & InjectFlag.Inject) == InjectFlag.Inject)
			{
				info.Methods = GetMethodsInfo(type);
				info.Properties = GetPropertiesInfo(type);
				info.Fields = GetFieldsInfo(type);
			}
		}

		private static InjectConstructorInfo GetConstructorInfo(Type type)
		{
			ConstructorInfo constructorInfo = FindConstructor(type);
			return new InjectConstructorInfo(constructorInfo, GetParametersInfo(type, constructorInfo));
		}

		private static ConstructorInfo FindConstructor(Type type)
		{
			ConstructorInfo[] constructors = type.GetConstructors(
					BindingFlags.Public |
					BindingFlags.NonPublic |
					BindingFlags.Instance);
			int constructorsLength = constructors.Length;
			if (constructorsLength == 0)
			{
				throw new Exception($"Class \"{type.Name}\" has no constructors.");
			}

			ConstructorInfo applicableConstructor = null;
			int explicitConstructorsCount = 0;
			foreach (ConstructorInfo constructor in constructors)
			{
				if (Attribute.IsDefined(constructor, InjectAttributeType))
				{
					if (!constructor.IsPublic)
					{
						throw new UsageException(
								$"\n\nCannot inject to non public constructor '{type.FullName}' \n");
					}
					applicableConstructor = constructor;
					explicitConstructorsCount++;
				}
			}

			if (applicableConstructor == null)
			{
				foreach (ConstructorInfo constructor in constructors)
				{
					if (constructor.GetParameters().Length == 0)
					{
						return constructor;
					}
				}

				throw new Exception($"Class \"{type.Name}\" has no applicable constructors.");
			}
			if (explicitConstructorsCount > 1)
			{
				throw new Exception($"Class \"{type.Name}\" has multiple constructors with \"Inject\" attribute.");
			}
			if (explicitConstructorsCount == 1)
			{
				return applicableConstructor;
			}

			throw new Exception(
					$"Class \"{type.Name}\" No constuctor with \"Inject\". Specify one with \"Inject\" attribute.");
		}

		[CanBeNull]
		private static List<InjectMethodInfo> GetMethodsInfo(Type type)
		{
			MethodInfo[] methods = type.GetMethods(
					BindingFlags.Public |
					BindingFlags.NonPublic |
					BindingFlags.Instance |
					BindingFlags.DeclaredOnly);
			if (methods.Length > 0)
			{
				List<InjectMethodInfo> methodsInfo = null;
				foreach (MethodInfo method in methods)
				{
					if (Attribute.IsDefined(method, InjectAttributeType))
					{
						if (!method.IsPublic)
						{
							throw new UsageException(
									$"\n\nCannot inject to non public members '{type.FullName}.{method.Name}' \n");
						}
						if (methodsInfo == null)
						{
							methodsInfo = new List<InjectMethodInfo>();
						}
						methodsInfo.Add(new InjectMethodInfo(method, GetParametersInfo(type, method)));
					}
				}
				return methodsInfo;
			}
			return null;
		}

		[CanBeNull]
		private static List<InjectPropertyInfo> GetPropertiesInfo(Type type)
		{
			PropertyInfo[] properties = type.GetProperties(
					BindingFlags.Public |
					BindingFlags.NonPublic |
					BindingFlags.Instance |
					BindingFlags.DeclaredOnly);
			if (properties.Length > 0)
			{
				List<InjectPropertyInfo> propertiesInfo = null;
				foreach (PropertyInfo property in properties)
				{
					if (Attribute.IsDefined(property, InjectAttributeType))
					{
						if (!property.CanWrite)
						{
							throw new Exception($"Property \"{property.Name}\" at class \"{type.Name}\" is readonly.");
						}
						if (propertiesInfo == null)
						{
							propertiesInfo = new List<InjectPropertyInfo>();
						}
						var injectAttribute = Attribute.GetCustomAttribute(property, InjectAttributeType);
						propertiesInfo.Add(new InjectPropertyInfo(property, GetAttributeInfo(injectAttribute)));
					}
				}
				return propertiesInfo;
			}
			return null;
		}

		[CanBeNull]
		private static List<InjectFieldInfo> GetFieldsInfo(Type type)
		{
			FieldInfo[] fields = type.GetFields(
					BindingFlags.Public |
					BindingFlags.NonPublic |
					BindingFlags.Instance |
					BindingFlags.DeclaredOnly);
			if (fields.Length > 0)
			{
				List<InjectFieldInfo> fieldsInfo = null;
				foreach (FieldInfo field in fields)
				{
					if (Attribute.IsDefined(field, InjectAttributeType))
					{
						if (!field.IsPublic)
						{
							throw new UsageException(
									$"\n\nCannot inject to non public members '{type.FullName}.{field.Name}' \n");
						}

						if (fieldsInfo == null)
						{
							fieldsInfo = new List<InjectFieldInfo>();
						}
						var injectAttribute = Attribute.GetCustomAttribute(field, InjectAttributeType);
						fieldsInfo.Add(new InjectFieldInfo(field, GetAttributeInfo(injectAttribute)));
					}
				}
				return fieldsInfo;
			}
			return null;
		}

		[CanBeNull]
		private static List<InjectParameterInfo> GetParametersInfo(Type type, MethodBase method)
		{
			ParameterInfo[] parameters = method.GetParameters();
			if (parameters.Length > 0)
			{
				List<InjectParameterInfo> parametersInfo = new List<InjectParameterInfo>();
				foreach (ParameterInfo parameter in parameters)
				{
					parametersInfo.Add(new InjectParameterInfo(parameter,
							GetParameterAttributeInfo(type, method, parameter)));
				}
				return parametersInfo;
			}
			return null;
		}

		[CanBeNull]
		private static InjectAttributeInfo GetParameterAttributeInfo(Type type,
				MethodBase method,
				ParameterInfo parameter)
		{
			Attribute[] attributes = Attribute.GetCustomAttributes(parameter, InjectAttributeType, false);
			if (attributes.Length == 1)
			{
				return GetAttributeInfo(attributes[0]);
			}
			if (attributes.Length > 1)
			{
				throw new Exception(
						$"Parameter \"{parameter.Name}\" in method \"{method.Name}\" at class \"{type.Name}\" has multiple \"Inject\" attributes.");
			}
			return null;
		}

		private static InjectAttributeInfo GetAttributeInfo(Attribute attribute)
		{
			if (attribute is InjectAttribute injectAttribute)
			{
				return new InjectAttributeInfo(injectAttribute.Id, injectAttribute.Optional);
			}
			throw new Exception($"Attribute \"{attribute.GetType()}\" has invalid type.");
		}

		private readonly struct AnalyzeResult
		{
			public readonly InjectInfo Info;
			public readonly InjectFlag Flags;

			public AnalyzeResult(InjectInfo info, InjectFlag flags)
			{
				Info = info;
				Flags = flags;
			}
		}
	}
}