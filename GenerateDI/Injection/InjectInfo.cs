using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Codegen;

namespace Framework.Injection
{
	public class InjectInfo
	{
		public readonly Type Type;

		[CanBeNull]
		public InjectConstructorInfo Constructor;
		[CanBeNull]
		public List<InjectMethodInfo> Methods;
		[CanBeNull]
		public List<InjectPropertyInfo> Properties;
		[CanBeNull]
		public List<InjectFieldInfo> Fields;

		public bool HasInjectReason(DiProjectAnalyzer projectAnalyzer)
		{
			if (HasDirectInjectReason())
			{
				return true;
			}
			if (HasInjectInParent(projectAnalyzer))
			{
				if (!Type.IsAbstract && Constructor == null)
				{
#if !NOT_UNITY3D
					if (typeof(UnityEngine.Object).IsAssignableFrom(Type))
					{
						return true;
					}
#endif
					throw new UsageException(
							$"\n\nInheritor {Type.FullName}  has not [Inject] constructor. Add [Inject]\n");
				}
				return true;
			}
			return false;
		}

		public bool HasDirectInjectReason()
		{
			int methods = Methods?.Count ?? 0;
			int fields = Fields?.Count ?? 0;
			if (methods == 0 && fields == 0)
			{
				if (Constructor != null)
				{
					if (Constructor.Parameters != null && Constructor.Parameters.Count > 0)
					{
						return true;
					}
				}
				return false;
			}
			return true;
		}

		private bool HasInjectInParent(DiProjectAnalyzer projectAnalyzer)
		{
			Type parent = Type.BaseType;
			while (parent != null)
			{
				InjectInfo injectInfo = projectAnalyzer.GetInjectInfo(parent.GetTypeInfo());
				if (injectInfo?.HasDirectInjectReason() ?? false)
				{
					return true;
				}


				parent = parent.BaseType;
			}
			return false;
		}

		public InjectInfo(Type type)
		{
			Type = type;
		}

		public override string ToString()
		{
			int methods = Methods?.Count ?? 0;
			int fields = Fields?.Count ?? 0;
			string constructor = Constructor != null ? "has" : "no";
			return $"{Type.Name} {constructor} constructor, {methods} methods, {fields} fields";
		}
	}

	public class InjectConstructorInfo
	{
		public readonly ConstructorInfo Info;
		[CanBeNull]
		public readonly List<InjectParameterInfo> Parameters;

		public InjectConstructorInfo(ConstructorInfo info, List<InjectParameterInfo> parameters = null)
		{
			Info = info;
			Parameters = parameters;
		}
	}

	public class InjectMethodInfo
	{
		public readonly MethodInfo Info;
		[CanBeNull]
		public readonly List<InjectParameterInfo> Parameters;

		public InjectMethodInfo(MethodInfo info, List<InjectParameterInfo> parameters = null)
		{
			Info = info;
			Parameters = parameters;
		}
	}

	public class InjectPropertyInfo
	{
		public readonly PropertyInfo Info;
		public readonly InjectAttributeInfo Attribute;

		public InjectPropertyInfo(PropertyInfo info, InjectAttributeInfo attribute)
		{
			Info = info;
			Attribute = attribute;
		}
	}

	public class InjectFieldInfo
	{
		public readonly FieldInfo Info;
		public readonly InjectAttributeInfo Attribute;

		public InjectFieldInfo(FieldInfo info, InjectAttributeInfo attribute)
		{
			Info = info;
			Attribute = attribute;
		}
	}

	public class InjectParameterInfo
	{
		public readonly ParameterInfo Info;
		[CanBeNull]
		public readonly InjectAttributeInfo Attribute;

		public InjectParameterInfo(ParameterInfo info, InjectAttributeInfo attribute = null)
		{
			Info = info;
			Attribute = attribute;
		}
	}

	public class InjectAttributeInfo
	{
		[CanBeNull]
		public readonly object Id;
		public readonly bool Optional;

		public InjectAttributeInfo(object id, bool optional)
		{
			Id = id;
			Optional = optional;
		}
	}
}