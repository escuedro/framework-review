﻿#if NOT_UNITY3D
using System;
using System.IO;
using System.Text;
using Framework;
using Framework.Codegen;

namespace GenerateDI
{
	public class DiGenerator
	{
		static void Main(string[] args)
		{
			// ReSharper disable once UnusedVariable
			ServerLogger serverLogger = new ServerLogger(ServerConfig.Default);
			var directory = new DirectoryInfo(Environment.CurrentDirectory);
			// ReSharper disable PossibleNullReferenceException
			var root = directory.Parent.Parent.Parent.Parent.Parent;
			string generatedDir = Path.Combine(root.FullName, "framework", "Global", "Generated");
			foreach (string arg in args)
			{
				if (arg == "ci")
				{
					generatedDir = Path.Combine(root.FullName, "Server", "Global", "Global", "Generated");
				}
			}
			// ReSharper restore PossibleNullReferenceException
			foreach (string argument in args)
			{
				if (argument == "di")
				{
					UpdateGeneratedDi($"{generatedDir}/GeneratedInjectors.cs",
							"Framework", "Global", "GameShared", "ServerGame");
				}
				else if (argument == "protocol")
				{
					UpdateGeneratedSerializer($"{generatedDir}/GeneratedSerializer.cs", "Framework",
							"GameShared");
				}
				else if (argument == "hash")
				{
					UpdateGeneratedHasher($"{generatedDir}/GeneratedHasher.cs", "GameShared", "Framework");
				}
			}
		}

		public static void UpdateGeneratedDi(string resultPath, params string[] assemblyNames)
		{
			Log.Info("Start analyze");
			DiProjectAnalyzer projectAnalyzer = new DiProjectAnalyzer(assemblyNames);
			DiProjectCodegen projectCodegen = new DiProjectCodegen();
			CodegenFile injectorFile = projectCodegen.GenerateInjectors(projectAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			injectorFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}

		public static void UpdateGeneratedSerializer(string resultPath, params string[] assemblyNames)
		{
			Log.Info("Start analyze");
			var projectAnalyzer = new SerializationProjectAnalyzer(false, assemblyNames);
			var projectCodegen = new SerializerCodegen();
			CodegenFile injectorFile = projectCodegen.GenerateSerializer(projectAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			injectorFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}

		public static void UpdateGeneratedHasher(string resultPath, params string[] assemblyNames)
		{
			Log.Info("Start analyze");
			var projectAnalyzer = new HashProjectAnalyzer(assemblyNames);
			var projectCodegen = new HashCodegen();
			CodegenFile injectorFile = projectCodegen.GetHashFile(projectAnalyzer);
			Log.Info("End analyze\n");
			StringBuilder stringBuilder = new StringBuilder();
			injectorFile.Write(stringBuilder, 0);
			File.WriteAllText(resultPath, stringBuilder.ToString());
		}
	}
}
#endif